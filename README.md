# 🪩 Boogie

*Boogie* is a terminal based Tango (https://www.tango-controls.org/) control system browser that takes inspiration from the standard tool "Jive".

![Screenshot](screenshot.png)

Built with Python (~3.9 or later), PyTango and Textual.

Development status: *Alpha* - not recommended for use in an operational control system. *There will be bugs!*


## Installation

The usual ways should work, e.g. cloning this repo and then:

    $ pip install

The Textual library is still under heavy development, and while Boogie requires a minimum version, it's not unlikely that later Textual versions will break things. If things seem broken, please downgrade `textual` to the required version (see `setup.cfg`).


## Usage

Start with

    $ boogie

or, to talk to another Tango host than the default:

    $ TANGO_HOST=some.tango.host:10000 boogie

Press `Ctrl-C` to quit.

Further instructions can be found in the built in "help" browser, opened by pressing `F1`.


## Development

Install in local mode so you can edit the files in place:

    $ pip install -e .

To run in "development" mode, start a `textual console` in another terminal, and then:

    $ textual run -c boogie --dev

Now logging and all `print` statements arrive on the console, very useful for debugging. Recommend using `textual console -x SYSTEM -x EVENT` to exclude the most verbose logging of Textual internals.

For documentation on Textual, see https://textual.textualize.io/

It is often convenient to control the intial path. This can be done using a command line argument, like so:

    $ boogie server:TangoTest:test:TangoTest:sys/tg_test/1


### Testing

There are some unit tests for the basic application, that don't require a Tango database.

These tests can be run like this:

    $ TEXTUAL=devtools pytest tests/test_app.py

The `TEXTUAL` env var enables connecting to the `textual console` to get debug logging during tests.


## Features (at various stages of "working")
- Browsing by server, class, device, alias, host
- Showing device state, status, info
- Listing device attributes, properties, commands
- Reading attributes, properties, various info
- Running commands
- Creating, deleting, editing device properties
- Deleting devices, servers
- Live device state, status
- Attribute "live" monitoring
- Writing attributes
- Custom Sardana browser
- Persistent navigation history
- Custom panels for device classes
- Bookmarks
- Search

## Planned features
- Attribute configuration
- Creating things (servers, devices...)
- Polling configurator

## Maybe features
- Saving settings
- More custom browsers, e.g. starter, HDB++...


## Under the hood

### UI

```
   | Foo |  Bar  Baz
  +--------+----------------------+
  |            Path               |
  +--------+----------------------+
  |        |       Banner         |
  |        +----------------------+
  |        |                      |
  |        |       Listing        |
  |  Tree  |                      |
  |        +----------------------|
  |        |                      |
  |        |       Details        |
  |        |                      |
  +--------+----------------------+
```

The UI layout is as above. At the top there's a tab bar, showing all the available "plugins". Below that is the "path" bar, that tells you the location of the thing currently being inspected. The Tree on the left shows a hierarchical view of the entries available.

The above parts are always present. The three stacked containers on the right side however are all optional, and may or may not be shown depending on the current path.

- The "Banner" is intended to be a compact widget, that may be implemented for any given entry type. If there is a banner implementation for an entry in the path it will be shown. If there are several, the last one in the path is shown.

- The "Listing" is useful for entries with multiple children, but which does not fit well in the tree. For example there might be lots of children, or more info to show per child. Then a list view is more appropriate. Implementing a `DetailsList` widget for an entry type accomplishes this. It has some built in functionality, such as filtering.

- Finally the `Details` widget is probably the main thing to implement for most entry types. If the currently selected entry (i.e. what is last in the path) has a Details widget implemented, it will be shown here. It can be completely custom.

### Entries

Boogie works a little bit like a web browser, in the sense that it always keeps track of the current location, a.k.a the "Path" (seen in the widget at the top). It can be e.g. `"Server" (the current tab) -> "TangoTest" (server) -> "test" (instance) -> "TangoTest" (class) -> "sys/tg_test/1" (device)`. It tells you where in the hierarchy you currently are. The main part of this hierarchy is visible in the tree widget on the left. Selecting something there (e.g. by pressing enter och clicking with the mouse) causes the Path to be updated.

Each entry is an object that represents a "level" in the hierarchy, for example a Tango device. It can also be more of an abstract thing like "the properties of a device". This is a convenience thing, as it's used by the UI. Each Entry can have any number of other Entries as children, forming a tree structure. Entries contain all the code for communicating with the actual control system.

The Path is in fact just the current Entry, with its chain of "ancestor" entries; parent, grandparent and so on up until the root entry e.g. "Server".

The entries can be seen as the "data model" of the application.

There is a history kept of the Path as it changes, so it's possible to go back to previous paths visited.

There's also an event called `NavigateTo` that can be sent by widgets, in order to navigate the application somewhere else. Useful for links, etc.

### Widgets

Apart from the entry tree on the left, a selected entry may also be represented by widgets that get shown on the right, when selected. Most entries are shown in the tree, but then there may be "leaf" entries which don't have subnodes in the tree. These are just Textual widgets (inheriting a special baseclass, see below) and can display any information, as well as allow interactions.

A widget generally represents an Entry. There is a widget base class called `EntryWidget` which should be used for all such widgets. It is a "container" (generic) class that contains an Entry subclass. The application will use this information to automatically match up Entry and widget, based on the current Path. In the basic case you just need to implement the Textual `compose` method to build your widget, and the `_update` method to hook it up with data whenever its Entry changes.

There are two main variants of `EntryWidget`; `Details` and `DetailsList`. These base classes are respectively used for entries that represent a single "thing" (like `TangoDevice`), and entries that represent collections (like `TangoDeviceAttributes`). These widgets provide some default functionality that should probably be used, to keep the interface consistent. But anything could of course be overridden and customized if really necessary.

It is also possible to use the EntryWidget class without setting the generic subscript, and setting the entry manually, using `set_entry()`. This is often useful for re-using child widgets.

### Plugins

Boogie consists of the basic "app" which doesn't do much by itself, and a set of "plugins" that contain all the "business logic". So far, "plugins" are mostly a way to compartmentalize the code in separate subpackages.

A "plugin" consists of one or more Entry hierarchies, together with a set of related widgets. Each hierarchy gets displayed as a "tab" in the main UI. Note that the basic Tango functionality is not really considered a plugin - even though it's implemented like one - because it is referenced by other plugins.

Plugins could be used for creating custom "views" of the control system, such as the sardana plugin. It would also be possible to create plugins that talk to other things than Tango, as long as they stick to the basic structure. However at this point there is no way to install custom plugins, though this could become possible in the future.
