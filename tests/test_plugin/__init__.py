"""
A dummy plugin for tests
"""

from . import widgets  # noqa
from .entry import Test1RootEntry, Test2RootEntry


async def get_title():
    return "abc123:10000"


BROWSERS = [
    {
        "root": Test1RootEntry,
        "get_title": get_title,
    },
    {
        "root": Test2RootEntry,
        "get_title": get_title,
    },
]
