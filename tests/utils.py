import asyncio

import pytest
from textual.css.query import NoMatches


async def retry(times: int = 10, sleep: float = 0.2):
    """
    Simple way to do a set number of retries in a test. Handles failed
    CSS queries. Fails the test if all retries are used up.
    """

    i = 0
    while i < times:
        try:
            yield i
        except NoMatches:
            pass
        await asyncio.sleep(sleep)
        i += 1

    pytest.fail("Ran out of retries.")
