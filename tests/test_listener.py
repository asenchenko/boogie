import asyncio
from random import random
from time import time

import pytest
import tango

from boogie.plugins.tango.listener import DeviceTracker


class MockDeviceProxy:
    def __init__(self, devicename, allow_subscription=False):
        self.name = devicename
        self.allow_subscription = allow_subscription

    async def read_attributes(self, attributes, extract_as=None):
        results = []
        for attribute in attributes:
            da = tango.DeviceAttribute()
            da.name = attribute
            da.value = random()
            da.timestamp = tango.TimeVal(time())
            da.quality = tango.AttrQuality.ATTR_VALID
            results.append(da)
        return results

    async def subscribe_event(self, attribute, event_type, callback, extract_as=None):
        if self.allow_subscription:
            pass  # TODO
        else:
            raise tango.DevFailed()


async def get_device_proxy_no_subs(devname):
    return MockDeviceProxy(devname)


def get_sleep():
    poll_event = asyncio.Event()

    async def poll_sleep(_):
        await poll_event.wait()
        await asyncio.sleep(0)

    async def toggle_event():
        poll_event.set()
        await asyncio.sleep(0)
        poll_event.clear()

    return poll_sleep, toggle_event


async def expect_one_each(queue, attributes):
    counter = 0
    seen_attrs = set()
    while counter < len(attributes):
        full_name, dev_attr = await queue.get()
        dev, attr = full_name.rsplit("/", 1)
        seen_attrs.add(attr)
        counter += 1
    assert queue.empty()
    assert seen_attrs == set(attributes)


@pytest.mark.asyncio
async def test_device_tracker_polled_immediate_read():
    devname = "tango://my.tango.host:10000/my/test/device"
    queue = asyncio.Queue()
    poll_sleep, _ = get_sleep()
    tracker = DeviceTracker(
        devname,
        queue,
        poll_period=0.1,
        get_device_proxy=get_device_proxy_no_subs,
        poll_sleep=poll_sleep,
    )
    await tracker.start()

    attributes = ["attr_a", "attr_b", "attrc"]
    await tracker.add_attributes(attributes)

    # Now the tracker poll loop has not yet had opportunity to run
    # We should still get data from all attributes at once
    seen_attrs = set()
    for _ in range(3):
        full_name, dev_attr = await queue.get()
        assert full_name.startswith("tango://my.tango.host:10000/my/test/device/")
        dev, attr = full_name.rsplit("/", 1)
        seen_attrs.add(attr)
    assert seen_attrs == set(attributes)

    # No more data
    assert queue.empty()

    await tracker.stop()


@pytest.mark.asyncio
async def test_device_tracker_polled_reads_all():
    devname = "tango://my.tango.host:10000/my/test/device"
    queue = asyncio.Queue()
    poll_sleep, toggle_sleep = get_sleep()
    tracker = DeviceTracker(
        devname,
        queue,
        poll_period=0.1,
        get_device_proxy=get_device_proxy_no_subs,
        poll_sleep=poll_sleep,
    )
    await tracker.start()

    attributes = ["attr_a", "attr_b", "attrc"]
    await tracker.add_attributes(attributes)

    # Initial read
    await expect_one_each(queue, attributes)

    # Each poll loop all attributes are read
    for _ in range(3):
        await toggle_sleep()
        await expect_one_each(queue, attributes)

    await tracker.stop()


@pytest.mark.asyncio
async def test_device_tracker_polled_add_attributes():
    devname = "tango://my.tango.host:10000/my/test/device"
    queue = asyncio.Queue()
    poll_sleep, toggle_sleep = get_sleep()
    tracker = DeviceTracker(
        devname,
        queue,
        poll_period=0.1,
        get_device_proxy=get_device_proxy_no_subs,
        poll_sleep=poll_sleep,
    )
    await tracker.start()
    attributes = ["attr_a", "attr_b", "attrc"]
    await tracker.add_attributes(attributes)

    # Initial read
    await expect_one_each(queue, attributes)

    # Some polling
    for _ in range(3):
        await toggle_sleep()
        await expect_one_each(queue, attributes)

    # Add some new attributes (note overlap)
    new_attributes = ["attr_a", "attr_d", "attre"]
    await tracker.add_attributes(new_attributes)

    # Expect an immediate read of the new attributes (but not)
    # attributes already added).
    await expect_one_each(queue, set(new_attributes) - set(attributes))

    # Each poll loop all attributes are read
    expected_attrs = set(attributes) | set(new_attributes)
    for _ in range(3):
        await toggle_sleep()
        await expect_one_each(queue, expected_attrs)

    await tracker.stop()


@pytest.mark.asyncio
async def test_device_tracker_polled_remove_attributes():
    devname = "tango://my.tango.host:10000/my/test/device"
    attributes = ["attr_a", "attr_b", "attrc"]
    queue = asyncio.Queue()
    poll_sleep, toggle_sleep = get_sleep()
    tracker = DeviceTracker(
        devname,
        queue,
        poll_period=0.1,
        get_device_proxy=get_device_proxy_no_subs,
        poll_sleep=poll_sleep,
    )
    await tracker.start()
    await tracker.add_attributes(attributes)

    # initial read
    await expect_one_each(queue, attributes)

    # Some polling
    for _ in range(3):
        await toggle_sleep()
        await expect_one_each(queue, attributes)

    # Remove some attributes
    old_attributes = ["attr_a", "attrc"]
    for attr in old_attributes:
        await tracker.remove_attribute(attr)

    assert queue.empty()

    # Check that we no longer get any of the removed attrs
    expected_attrs = set(attributes) - set(old_attributes)
    for _ in range(3):
        await toggle_sleep()
        await expect_one_each(queue, expected_attrs)

    await tracker.stop()
