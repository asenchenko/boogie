"""
These test run the behavior of the app, with mock plugins that don't
require any external communication.
"""

from copy import deepcopy

import pytest
from boogie.app import Boogie
from . import test_plugin

pytestmark = pytest.mark.asyncio


def get_app(path=None):
    """
    A simple wrapper returning an app with default arguments.
    May provide an initial path for the app to start on.
    """

    # The :memory: argument ensures we don't write to the default
    # sqlite history db when running tests. It will instead use
    # RAM to store the db.
    return Boogie(
        path,
        history_db_path=":memory:",
        bookmarks_db_path=":memory:",
        plugins=[test_plugin],
    )


async def test_app_basic():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        assert tree.has_focus
        tree.select_node(None)
        node = tree.cursor_node
        assert "Apa1" in node.label
        app.save_screenshot("/tmp/app.svg")


async def test_app_navigation():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        tree.select_node(None)
        await pilot.press("down")
        node = tree.cursor_node
        assert "Bepa1" in node.label
        await pilot.press("right")
        await pilot.press("right")
        node = tree.cursor_node
        assert "foo1" in node.label


async def test_app_tree_goes_to_node():
    app = get_app(["Root1", "Cepa1", "bar1"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        node = tree.cursor_node
        assert "bar1" in node.label


async def test_app_tree_focused_on_tab_change():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("2")
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root2").query_one("CustomTree")
        assert tree.has_focus


async def test_app_tree_initial_path():
    app = get_app(["root1", "Bepa1", "baz1"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        assert tree.has_focus
        node = tree.cursor_node
        assert "baz1" in node.label


async def test_app_details_widget():
    app = get_app(["root1", "Bepa1", "foo1"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        w = app.query_one("#test1-level2-details")
        assert "foo1" in w.render()
        app.save_screenshot("/tmp/app.svg")


async def test_app_details_list_widget():
    app = get_app(["root2", "Bepa2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )
        assert table.get_row_at(1) == [
            "bar2",
        ]


async def test_app_details_list_widget_selected():
    app = get_app(["root2", "Bepa2", "bar2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )
        app.save_screenshot("/tmp/app.svg")
        assert table.cursor_row == 1


async def test_app_details_list_widget_navigation():
    app = get_app(["root2", "Bepa2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("enter")
        await pilot.wait_for_scheduled_animations()
        app.save_screenshot("/tmp/app.svg")
        w = app.query_one("#test2-level2-details")
        assert "foo2" in w.render()


async def test_app_details_list_widget_filter():
    app = get_app(["root2", "Bepa2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("%")
        await pilot.press("b", "a", "enter")
        await pilot.wait_for_scheduled_animations()
        app.save_screenshot("/tmp/app.svg")
        table = (
            app.query_one("TabPane#root2")
            .query_one("#details-list")
            .query_one("DataTable")
        )

        # "foo2" should be filtered out here
        assert table.get_row_at(0)[0] == "bar2"
        assert table.get_row_at(1)[0] == "baz2"


async def test_app_details_list_widget_opens_details():
    app = get_app(["root2", "Bepa2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("down")
        await pilot.press("enter")
        await pilot.wait_for_scheduled_animations()
        app.save_screenshot("/tmp/app.svg")
        pane = app.query_one("TabPane#root2")
        w = pane.query_one("#details").query_one("#test2-level2-details")
        assert "bar2" in w.render()


async def test_app_history_stored():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("down", "right", "down", "right", "enter")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "foo1"
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 1


async def test_app_history_back():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        tree.select_node(None)
        await pilot.press("down", "right", "enter")
        await pilot.press("right", "down", "enter")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "bar1"
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 2

        # Go back
        await pilot.press("ctrl+left")
        assert pane.entry.name == "Bepa1"
        history = list(app.history.items("abc123:10000"))
        # Going back does not add a history entry
        assert len(history) == 2


async def test_app_history_forward():
    app = get_app()
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        await pilot.press("down", "right", "enter")
        await pilot.press("right", "down", "enter")
        await pilot.press("ctrl+left")

        # Go forward
        await pilot.press("ctrl+right")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "bar1"
        history = list(app.history.items("abc123:10000"))
        # Going back does not add a history entry
        assert len(history) == 2


async def test_app_history_up():
    app = get_app(["root1", "Bepa1", "baz1"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()
        tree = app.query_one("TabPane#root1").query_one("CustomTree")
        assert tree.has_focus
        node = tree.cursor_node
        assert "baz1" in node.label
        history = list(app.history.items("abc123:10000"))
        assert len(history) == 1

        # Go up (to the parent entry)
        await pilot.press("ctrl+up")
        pane = app.query_one("TabPane#root1 Browser")
        assert pane.entry.name == "Bepa1"
        history = list(app.history.items("abc123:10000"))
        # Going up does not add a history entry
        assert len(history) == 1


async def test_app_reload(monkeypatch):
    app = get_app(["root2", "Bepa2", "bar2"])
    async with app.run_test() as pilot:
        await pilot.wait_for_scheduled_animations()

        # Remove part of the data so that the end of the path is not valid
        new_data = deepcopy(test_plugin.entry.data)
        del new_data["Root2"]["Bepa2"]["bar2"]
        monkeypatch.setattr(test_plugin.entry, "data", new_data)

        # Trigger a reload
        await pilot.press("f5")
        await pilot.wait_for_scheduled_animations()

        # Check that we went up the path
        pane = app.query_one("TabPane#root2 Browser")
        assert pane.entry.name == "Bepa2"

        # Check that we updated the widgets
        details = pane.query_one("#details")
        assert details.display is False
        # app.save_screenshot("/tmp/app.svg")
