import asyncio
from typing import Any

from textual import work
from textual.message import Message
from textual.reactive import var
from textual.widgets import Tree, Input
from textual.widgets._tree import TreeNode
from textual.binding import Binding
from textual.geometry import Region

from .entry import Entry, EntryType
from .widgets.entry import EntryWidget
from .widgets.modal import ConfirmModal
from .messages import Reload


class CustomTree(Tree):
    "Tree widget that behaves somewhat like the Jive tree"

    BINDINGS = [
        Binding("enter", "select_cursor", "Select", show=False),
        Binding("up", "cursor_up", "Cursor Up", show=False),
        Binding("down", "cursor_down", "Cursor Down", show=False),
        Binding("left", "cursor_back", "Cursor Back", show=False),
        Binding("right", "cursor_forward", "Cursor Forward", show=False),
    ]

    def action_cursor_back(self) -> None:
        line = self._tree_lines[self.cursor_line]
        node = line.path[-1]
        if node.is_expanded:
            node.collapse()
            self.post_message(self.NodeCollapsed(node))
        elif len(line.path) > 1:
            parent = line.path[-2]
            self.cursor_line = parent.line
            self.scroll_to_line(self.cursor_line)

    async def action_cursor_forward(self) -> None:
        line = self._tree_lines[self.cursor_line]
        node = line.path[-1]
        if self.auto_expand:
            if not node.allow_expand:
                return
            if node.is_expanded:
                self.cursor_line += 1
            else:
                node.expand()
                self.post_message(self.NodeExpanded(node))
            self.scroll_to_line(self.cursor_line)

    def action_select_cursor(self) -> None:
        line = self._tree_lines[self.cursor_line]
        node = line.path[-1]
        self.post_message(self.NodeSelected(node))

    def _get_label_region(self, line: int) -> Region | None:
        """Returns the region occupied by the label of the node at line `line`."""

        # Override that ensures that the little arrow also gets scrolled into
        # view when a tree node is highlighted. I think it's more intuitive.

        try:
            tree_line = self._tree_lines[line]
        except IndexError:
            return None
        region_x = tree_line._get_guide_width(self.guide_depth, self.show_root)
        region_width = self.get_label_width(tree_line.node)
        return Region(region_x - 2, line, region_width + 2, 1)


class BoogieTree(EntryWidget):
    DEFAULT_CSS = """

    BoogieTree {
        scrollbar-size-vertical: 1;
        display: none;
        #error {
            display: none;
            color: $error;
        }
        Input.pattern {
            dock: top;
            display: none;
            border: solid gray;
        }
    }
    """

    pattern = var(None, init=False)

    BINDINGS = [
        Binding("plus", "grow", "Grow", show=False),
        Binding("minus", "shrink", "Shrink", show=False),
        Binding("equals_sign", "reset", "Reset", show=False),
        # Binding("delete", "delete", "Delete", show=True),
        # Binding("a", "add", "Add", show=True),
    ]

    entry_type: type[Entry]

    class Selected(Message):
        def __init__(self, entry: Any) -> None:
            self.entry = entry
            super().__init__()

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self._node_states: dict[Entry, bool] = {}
        self._load_lock = asyncio.Lock()
        self.rooted = asyncio.Event()
        self._highlighted_node = None

    def on_tree_node_highlighted(self, message) -> None:
        self._highlighted_node = message.node

    @property
    def current_node(self) -> TreeNode | None:
        tree = self.query_one(CustomTree)
        node = tree.cursor_node
        return node

    @property
    def current_entry(self) -> Entry | None:
        node = self.current_node
        if node:
            return node.data
        return None

    async def action_delete(self) -> None:
        tree = self.query_one(CustomTree)
        node = self.current_node
        assert node
        entry = node.data

        async def maybe_delete(delete):
            if delete:
                await entry.delete()
                tree.select_node(node.parent)
                self.post_message(Reload())
                self.notify(f"Deleted '{entry.name}'")

        if entry and hasattr(entry, "delete"):
            question = f"Really delete {entry.name}?"
            self.app.push_screen(ConfirmModal(question), maybe_delete)

    async def action_add(self):
        tree = self.query_one(CustomTree)
        node = self.current_node
        entry = node.data

        async def maybe_delete(delete):
            if delete:
                await entry.delete()
                tree.select_node(node.parent)
                self.post_message(Reload())
                self.notify(f"Deleted '{entry.name}'")

        if hasattr(entry, "delete"):
            question = f"Really delete {entry.name}?"
            self.app.push_screen(ConfirmModal(question), maybe_delete)

    def on_input_submitted(self, msg):
        self.pattern = msg.value
        pattern_input = self.query_one(Input)
        if not msg.value:
            pattern_input.display = False
        pattern_input.can_focus = False
        self.query_one(CustomTree).focus()

    async def _update(self, entry: EntryType) -> None:
        """
        TODO this is a little confusing: the tree is created once, when
        the "entry" is set. That becomes the tree's "root".
        Later, to navigate the tree to some sub-entry, use the
        "select_entry" method.
        This should perhaps be refactored.
        """
        self.loading = True
        self.query(CustomTree).remove()
        tree = CustomTree(entry.name, data=entry)
        tree.show_root = False
        tree.guide_depth = 2
        await self.mount(tree)
        tree.root.expand()
        tree.focus()
        self.display = True
        self.loading = False

    async def on_tree_node_expanded(self, message):
        node = message.node
        await self._load_children(node)
        # Keeping track of opened nodes, for restore after reloading
        # Might be that this can be avoided.
        self._node_states[node.data] = True

    def on_tree_node_collapsed(self, message):
        self._node_states.pop(message.node.data, None)

    async def _load_children(self, node, force=False) -> None:
        "Called when a tree node is expanded, to add its children"
        # self.log("_load_children", node=node)
        async with self._load_lock:  # Prevent simultaneous loading
            if node._children and not force:
                # Already populated!
                return
            node.remove_children()
            with self.app.batch_update():
                try:
                    for child in await node.data.get_children():
                        print("found child", child)
                        if child.leaf_node:
                            node.add_leaf(await child.get_label(), child)
                        else:
                            node.add(await child.get_label(), child)
                except Exception as e:
                    self.app.notify(f"Failed to load children for entry {node}: {e}")
            tree = self.query_one(Tree)
            if node == tree.root:
                self.rooted.set()

    async def on_tree_node_selected(self, message: Tree.NodeSelected):
        self.post_message(self.Selected(message.node.data))

    async def select_entry(self, entry):
        """
        Select the node corresponding to the given entry.
        The path may be deeper than the tree (e.g if something has been
        removed), in which case we stop at the last node available.
        """
        await self.rooted.wait()

        tangotree = self.query_one(CustomTree)
        try:
            node = await self.find_node(tangotree.root, entry.path[1:])
            # self.log("Select", node=node)
            # Works better if we don't call this immediately
            # TODO figure out why?
            self.call_after_refresh(self.go_to, node)
        except ValueError:
            self.log("Could not find tree node for", entry=entry, root=tangotree.root)

    def go_to(self, node):
        tree = self.query_one(CustomTree)
        # TODO can we avoid this if the node is already visible?
        tree.scroll_to_node(node)
        tree.select_node(node)

    def action_grow(self):
        width = self.styles.width
        self.styles.width = f"{width.value + 5}{width.symbol}"

    def action_shrink(self):
        width = self.styles.width
        self.styles.width = f"{max(0, width.value - 5)}{width.symbol}"

    def action_reset(self):
        self.styles.width = None

    async def find_node(self, root, path):
        # self.log("find_node", root=root, path=path)
        try:
            entry, *rest = path
        except ValueError:
            return root
        # Make sure the root node is expanded
        await self.rooted.wait()
        # self.log("Looking through children of", root=root)
        for node in root.children:
            # self.log("Node", node=node)
            if node.data.path_str().lower() == entry.path_str().lower():
                if entry.leaf_node:
                    # self.log("Done", node=node)
                    return node
                if rest:
                    # A bit complicated; if the node is not expanded
                    # the node may not yet exist. In that case, we load the
                    # children manually (otherwise this would happen
                    # asynchronously, meaning hard to handle here)
                    with self.prevent(Tree.NodeExpanded):
                        node.expand()
                    try:
                        return await self.find_node(node, rest)
                    except ValueError:
                        await self._load_children(node, force=True)
                        try:
                            return await self.find_node(node, rest)
                        except ValueError:
                            pass
                return node
        raise ValueError("path could not be found")

    @work(exclusive=True)
    async def reload(self) -> None:
        self.log("Reload tree...")
        self.rooted.clear()
        if self._highlighted_node:
            path = self._highlighted_node.data.path
        else:
            path = None
        with self.app.batch_update():
            tree = self.query_one(CustomTree)
            await self._load_children(tree.root, force=True)
            for entry in list(self._node_states.keys()):
                try:
                    node = await self.find_node(tree.root, entry.path[1:])
                    node.expand()
                except ValueError:
                    pass

        # TODO find a better way to ensure that things are loaded so that
        # we can revisit the current node. Currently this happens after
        # the batch, potentially leading to flicker.
        if path:
            self.call_after_refresh(self._finish_reload, path)

    async def _finish_reload(self, path):
        await self.rooted.wait()
        tree = self.query_one(CustomTree)
        try:
            node = await self.find_node(tree.root, path[1:])
            self.go_to(node)
        except ValueError:
            pass
