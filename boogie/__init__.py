import importlib
import pkgutil

from . import plugins


def iter_namespace(ns_pkg):
    return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")


# discovered_plugins = sorted(
#     (
#         importlib.import_module(name)
#         for finder, name, ispkg in iter_namespace(plugins)
#         if ispkg
#     ),
#     key=lambda m: getattr(m, "INDEX", 100),
# )


def get_plugins():
    return sorted(
        (
            importlib.import_module(name)
            for finder, name, ispkg in iter_namespace(plugins)
            if ispkg
        ),
        key=lambda m: getattr(m, "INDEX", 100),
    )
