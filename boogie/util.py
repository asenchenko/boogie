from contextlib import contextmanager
import hashlib
import sys
import os


@contextmanager
def swallow_stderr():
    """Context manager that redirects stderr and stdout somewhere else"""
    # This is a hack to get rid of some unwanted output

    stderr_fileno = sys.stderr.fileno()
    stderr_save = os.dup(stderr_fileno)
    stderr_pipe = os.pipe()
    os.dup2(stderr_pipe[1], stderr_fileno)
    os.close(stderr_pipe[1])

    yield

    os.close(stderr_pipe[0])
    os.dup2(stderr_save, stderr_fileno)
    os.close(stderr_save)


def freeze_data(raw_data: dict[str, str | list]):
    "Ensure that the given dict consists of hashable values"
    # TODO this assumes there are no other weird things in there,
    # and that the dict is flat. Hope it holds!
    return {
        key: tuple(value) if isinstance(value, list) else value
        for key, value in raw_data.items()
        if key.lower() not in {"parent", "path"}  # Collision
    }


def persistent_hash(s: str):
    return int(hashlib.sha512(s.encode("utf-8")).hexdigest(), 16)
