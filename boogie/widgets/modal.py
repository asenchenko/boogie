from importlib.resources import files
import webbrowser

from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.containers import Grid
from textual.widgets import Label, Button, Input, MarkdownViewer, Markdown, Footer
from textual.events import Key


class ConfirmModal(ModalScreen[bool]):
    """
    Screen with a yes/no dialog.
    """

    DEFAULT_CSS = """
    ConfirmModal {
        align: center middle;
    }
    ConfirmModal #dialog {
        grid-size: 2;
        grid-gutter: 1 2;
        grid-rows: 1fr 3;
        padding: 0 1;
        width: 60;
        height: 11;
        border: thick $error;
        background: $surface;
    }
    ConfirmModal #question {
        column-span: 2;
        height: 1fr;
        width: 1fr;
        content-align: center middle;
    }
    ConfirmModal Button {
        width: 100%;
    }
    """

    def __init__(self, question, *args, **kwargs):
        self.question = question
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.question, id="question"),
            Button("Cancel", variant="error", id="no"),
            Button("Proceed", variant="primary", id="yes"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "yes":
            self.dismiss(True)
        else:
            self.dismiss(False)

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(False)


class GetStringModal(ModalScreen[str]):

    DEFAULT_CSS = """
    GetStringModal {
        align: center middle;
    }
    GetStringModal #dialog {
        grid-size: 2;
        grid-gutter: 1 2;
        grid-rows: 1fr 3 3;
        padding: 0 1;
        width: 60;
        height: 12;
        background: $surface;
        border: thick $primary;
    }
    GetStringModal #question {
        column-span: 2;
        height: 1fr;
        width: 1fr;
        content-align: center middle;
    }
    GetStringModal Input {
        column-span: 2;
        height: 1fr;
        width: 1fr;
    }
    GetStringModal Button {
        width: 100%;
    }
    """

    def __init__(self, message, placeholder=None, initial=None, *args, **kwargs):
        self.message = message
        self.placeholder = placeholder
        self.initial = initial
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.message, id="question"),
            Input(self.initial, placeholder=self.placeholder),
            Button("Cancel", variant="error", id="no"),
            Button("Proceed", variant="primary", id="yes"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "yes":
            name = self.query_one(Input).value
            self.dismiss(name)
        else:
            self.dismiss()

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss()


class MyMarkdownViewer(MarkdownViewer):
    """
    Markdown viewer customized to open HTTP links in a web browser (if possible).
    """

    async def _on_markdown_link_clicked(self, message: Markdown.LinkClicked) -> None:
        self.log(href=message.href)
        if message.href.startswith(("http://", "https://")):
            webbrowser.open(message.href, new=2)
            self.app.notify("Opened HTTP link in your web browser!")
            message.prevent_default()


class HelpBrowser(ModalScreen):

    BINDINGS = [
        ("escape", "close", "Close"),
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.text = files("boogie").joinpath("HELP.md").read_text()

    def compose(self) -> ComposeResult:
        yield MyMarkdownViewer(self.text, show_table_of_contents=False)
        yield Footer()

    def action_close(self):
        self.dismiss()
