import asyncio
from textwrap import dedent

from textual.widgets import TabPane, Static, Label, Collapsible
from textual.widgets._collapsible import CollapsibleTitle
from textual.messages import Message
from rich.pretty import Pretty

from ..entry import Entry
from .entry import EntryWidget


class LazyTabPane(TabPane):
    """
    A Tab that defers mounting its content widget until first shown.
    """

    def __init__(
        self, title: str, child_class: EntryWidget, entry: Entry | None = None, **kwargs
    ):
        self.__title = title
        self._child_class = child_class
        self._entry = entry
        self._loaded = asyncio.Event()
        super().__init__(title=title, **kwargs)
        # self.loading = True

    def on_show(self):
        if not self.is_loaded:
            self._load_child()

    def _load_child(self):
        print("Loading lazy pane...")
        widget = self._child_class()
        self.mount(widget)
        widget.set_entry(self._entry)
        print("Done")
        self._loaded.set()
        # self.loading = False

    @property
    def is_loaded(self):
        return self._loaded.is_set()

    @property
    def loaded(self):
        return self._loaded.wait()


class InfoTable(Static, can_focus=False):
    """
    Table widget that shows static info from a dict.
    FIELDS can contain all fields that are interesting, but
    only the ones that exist in the given dict are displayed.
    """

    DEFAULT_CSS = """
    InfoTable {
        width: 1fr;
        height: auto;
        layout: grid;
        grid-gutter: 0 1;
        grid-size: 2;
    }
    InfoTable Label {
        text-align: right;
        color: $text-muted;
    }
    InfoTable Static {
    }
    InfoTable Input {
        border: none;
        padding: 0;
    }
    InfoTable Input:focus {
        border: none;
        background: $background-lighten-1;
    }
    """

    FIELDS: list[tuple[str, str]] = []

    class Changed(Message):
        def __init__(self, name, value):
            self.name = name
            self.value = value
            super().__init__()

    # def compose(self):
    #     yield Vertical()

    def set_config(self, values):
        used_labels = set()
        with self.app.batch_update():
            self.query("*").remove()
            for title, name, *rest in self.FIELDS:
                if name in values:
                    value = values.get(name)
                    self.mount(Label(f"{title}:", expand=True, classes=name))
                    if rest:
                        (widget_class,) = rest
                        value_fmt = value
                    else:
                        widget_class = Static
                        value_fmt = (
                            dedent(value).strip()
                            if isinstance(value, str)
                            else Pretty(value)
                        )
                    self.mount(widget_class(value_fmt, id=name, classes=name))
                    used_labels.add(title)
            width = max(len(label) for label in used_labels)
            self.styles.grid_columns = (width + 2, "1fr")


class CollapsibleTitleStyled(CollapsibleTitle):
    def __init__(
        self,
        *,
        label: str,
        collapsed_symbol: str,
        expanded_symbol: str,
        collapsed: bool,
    ) -> None:
        Static.__init__(self, markup=True)
        self.collapsed_symbol = collapsed_symbol
        self.expanded_symbol = expanded_symbol
        self.label = label
        self.collapsed = collapsed
        self._collapsed_label = f"{collapsed_symbol} {label}"
        self._expanded_label = f"{expanded_symbol} {label}"


class StyledCollapsible(Collapsible):
    def __init__(
        self,
        *children,
        title: str = "Toggle",
        collapsed: bool = True,
        collapsed_symbol: str = "▶",
        expanded_symbol: str = "▼",
        name: str | None = None,
        id: str | None = None,
        classes: str | None = None,
        disabled: bool = False,
    ) -> None:
        super().__init__(name=name, id=id, classes=classes, disabled=disabled)
        self._title = CollapsibleTitleStyled(
            label=title,
            collapsed_symbol=collapsed_symbol,
            expanded_symbol=expanded_symbol,
            collapsed=collapsed,
        )
        self.title = title
        self._contents_list = list(children)
        self.collapsed = collapsed
