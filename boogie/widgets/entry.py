from __future__ import annotations
import asyncio
from functools import partial
from inspect import signature
import logging
from typing import Sequence, TypeVar, Generic, get_args
from typing import TYPE_CHECKING

from rich.pretty import Pretty
from textual import on
from textual.app import ComposeResult
from textual.events import Mount
from textual.widget import Widget
from textual.reactive import reactive, var
from textual.widgets import Static
from textual.containers import VerticalScroll
from textual.css.query import NoMatches

if TYPE_CHECKING:
    from ..app import Boogie

from ..entry import Entry, EntryType


logger = logging.getLogger(__name__)


class EntryWidget(Widget, Generic[EntryType]):
    """
    A very basic widget class that has an Entry (None at init).
    The entry can be changed, and the widget should update
    accordingly.

    Use Details/DetailsList for the main details widgets,
    but subwidgets to those can be EntryWidgets if they need
    an entry.

    The intended way of using this class is to subclass it and set
    the entry_class to whatever entry it's supposed to handle.
    The widget may implement _update() to do whatever needs to
    happen when "entry" is set. E.g. update entry on child widgets,
    etc.
    """

    # Subpath is the part of the current path beginning with the entry.
    # It can be useful e.g. when displaying a list to also be able to
    # highlight the current selected child entry.
    # subpath: var[Sequence[Entry]] = var([], init=False, always_update=True)

    app: Boogie
    entry: EntryType | None
    _subsubpath: Sequence[Entry]

    def __init__(self, *args, **kwargs) -> None:
        self._mounted = asyncio.Event()
        self.entry = None
        self._subsubpath = []
        super().__init__(*args, **kwargs)

    # "entry" is not a reactive var, it's just reflecting "subpath". But for
    # convenience we make it sort of work a bit like one anyway.
    # But typically setting subpath is better, as it allows widgets to reflect
    # selected child too.
    # TODO This seems overcomplicated, there should be a better solution.

    _error: var[Exception | None] = var(None, init=False, always_update=False)

    def set_entry(self, entry: EntryType, *subsubpath: Entry, reload=False) -> None:
        """
        Set the entry for this widget.
        """
        # OK this is a bit messy, naming needs work
        # - subpath = [entry, *subsubpath] :P
        logger.debug("%r.set_entry(%r) start", self, entry)
        old_subpath = self.subpath
        sig = signature(self._update)
        nargs = len(sig.parameters)
        subpath: list[Entry] = [entry, *subsubpath]
        if reload or subpath[:nargs] != old_subpath[:nargs]:
            self._subsubpath = subsubpath
            self.entry = entry
            self.run_worker(
                partial(self._update_wrapper, entry, *subsubpath[: nargs - 1]),
                exclusive=True,  # Abort any previous run for the same widget
                group=str(id(self)),
                description=f"_update_wrapper for {type(self).__name__} {subpath}",
            )
        logger.debug("%r.set_entry(%r) end", self, entry)

    def set_error(self, e: Exception) -> None:
        self._error = e

    def reload(self):
        """
        Just redraw the widget.
        """
        # TODO Any cached data is not reloaded, can this be handled somehow?
        self.set_entry(self.entry, reload=True)

    @property
    def subpath(self) -> Sequence[Entry]:
        if self.entry:
            return [self.entry, *self._subsubpath]
        return []

    @on(Mount)
    def __on_mount(self) -> None:
        self._mounted.set()

    @property
    def mounted(self):
        return self._mounted.wait()

    async def _update_wrapper(self, entry: EntryType, *subsubpath: Entry):
        """
        Wrapper that runs the _update method as soon as the widget is mounted
        Doing it earlier causes problems since the update method probably needs
        to access child widgets, that won't exist.
        """
        await self._mounted.wait()
        # try:
        logger.debug("%r._update(%r) start", self, entry)
        await self._update(entry, *subsubpath)
        logger.debug("%r._update(%r) end", self, entry)
        # except Exception as e:
        #     self.log("+++++++++++++++++++++", widget=self, error=e)
        #     # TODO not sure this is a good idea... maybe better to crash?
        #     # These exceptions could be bugs as well as "real" problems.
        #     self._error = e

    def watch__error(self, _, error: Exception) -> None:
        logger.error("%r.error: %r", self, error);
        if error:
            self.add_class("error")
        else:
            self.remove_class("error")
        try:
            self.query_one(Error).error = error
        except NoMatches:
            pass

    async def _update(self, entry: EntryType) -> None:
        """
        Override this to do whatever is needed when entry changes.  You
        could override watch_entry() instead, but it's probably better
        to use _update() as it will be run as a worker, and also
        exclusively.  That means it's fine even for _update to never
        exit, e.g. for updating stuff in a loop. The worker will be
        cancelled automatically, next time entry changes.
        """

    # Helpers

    # TODO there should be a way to type the return type so that
    # it also works for subclasses... only matters for type checking.
    @classmethod
    def get_widget_class(cls, entry_class: type[EntryType]) -> type[EntryWidget] | None:
        """
        Find the proper EntryWidget subclass to display for the given Entry
        class. Does this by inspecting the class hierarchy and matching
        on the type parameter. It will return the most "specific" matching
        widget class it finds. That means that a subclass trumps a class higher
        up in the hierarchy.
        """
        subclasses = all_subclasses(cls)
        matches = []
        for subclass in subclasses:
            if hasattr(subclass, "__orig_bases__"):
                args = get_args(subclass.__orig_bases__[0])
                if args:
                    widget_entry_class = args[0]  # Generic type parameter
                    if issubclass(entry_class, widget_entry_class):
                        matches.append(subclass)
        if matches:
            return matches[0]  # TODO or -1?
        return None


class Error(Static):
    """
    This widget is useful for displaying any errors that occur.
    Normally not visible, it will appear if the error property is
    set on a Details widget.
    """

    DEFAULT_CSS = """
    Error {
        overflow-x: hidden;
        dock: bottom;
        display: none;
    }
    """

    BINDINGS = [
        ("x", "dismiss", "Dismiss error"),
    ]

    error: reactive[Exception | None] = reactive(None, always_update=False)

    def compose(self) -> ComposeResult:
        self.can_focus = False
        with VerticalScroll():
            yield Static()

    def watch_error(self, _, error):
        error = self.query_one(Static)
        if isinstance(error, Exception):
            self.border_title = "Exception"
            content = Pretty(error)
            error.update(content)
        else:
            error.update(str(error))

    def action_dismiss(self):
        self.parent._error = None


class MonitorWidget(EntryWidget, Generic[EntryType]):
    pass


class MonitorWidgetTrend(Static):
    pass


T = TypeVar("T")


def all_subclasses(cls: type[T]) -> Sequence[type[T]]:
    """
    Recursively find all the subclasses of a given class. Sorted in reverse
    order because we want to prioritize more specialized subclasses.

    This only works as intended if all the subclasses available are imported
    already, so we have to make sure all widgets that are to be used are
    imported at startup. Plugins must take care to do that.
    """
    # TODO pretty ugly...
    return list(
        reversed(
            sum(
                [
                    list(cls.__subclasses__()),
                    [s for c in cls.__subclasses__() for s in all_subclasses(c)],
                ],
                [],
            )
        )
    )
