from __future__ import annotations
from typing import Sequence, Generic

import tango
from textual.app import ComposeResult
from textual.binding import Binding
from textual.reactive import var
from textual.widgets import DataTable, Input

from ..messages import NavigateTo
from .entry import EntryWidget, Error, EntryType


class Details(EntryWidget, Generic[EntryType]):
    """
    Base widget for a "details" widget.
    Intended to represent an entry, whatever that means.
    """

    DEFAULT_CSS = """
    Details {
        height: 1fr;
        &.error Error {
            display: block;
            dock: bottom;
        }
        .actions {
            height: auto;
            dock: bottom;
        }
    }
    """

    BINDINGS = [
        Binding("plus", "grow", "Grow", show=False),
        Binding("minus", "shrink", "Shrink", show=False),
        Binding("equals_sign", "reset", "Reset", show=False),
    ]

    def action_grow(self) -> None:
        if self.parent:
            height = self.parent.styles.height
            if height is not None:
                self.parent.styles.height = f"{height.value * 2}{height.symbol}"

    def action_shrink(self) -> None:
        if self.parent:
            height = self.parent.styles.height
            if height:
                self.parent.styles.height = f"{height.value / 2}{height.symbol}"

    def action_reset(self) -> None:
        "Reset the relative sizes of panes."
        pane = self.parent
        assert pane
        listing = pane.query_one("#details-list")
        assert listing
        listing.styles.height = None
        details = pane.query_one("#details")
        assert details
        details.styles.height = None


class DetailsList(EntryWidget, Generic[EntryType]):
    """
    Base widget for a "details-list" widget, showing an entry's children
    in a DataTable.

    Typically, a subclass only needs to implement `get_row`.
    """

    DEFAULT_CSS = """
    DetailsList {
        height: 1fr;
        width: 1fr;
        Input {
            dock: top;
            display: none;
        }
        DataTable {
            height: 1fr;
        }
        &.error Error {
            display: block;
            dock: bottom;
        }
    }
    """

    # "entries" refers to child entries, but "children" is taken by
    # Textual. Perhaps "child_entries" would be more helpful?
    entries: var[list[EntryType]] = var([], init=False)

    # Child filter pattern
    pattern: var[str] = var("", init=False)

    error_widget = Error

    BINDINGS = [
        Binding("plus", "grow", "Grow", show=False),
        Binding("minus", "shrink", "Shrink", show=False),
        Binding("equals_sign", "reset", "Reset", show=False),
        ("%", "filter", "Filter"),
    ]

    COLUMNS: Sequence[str] = ()  # Titles for columns in the table

    def compose(self) -> ComposeResult:
        yield Input(classes="pattern", placeholder="Filter list items on substring")
        data_table: DataTable[tuple[str, ...]] = DataTable()
        data_table.add_columns(*self.COLUMNS)
        data_table.cursor_type = "row"
        data_table.zebra_stripes = True
        yield data_table
        yield self.error_widget()

    # TODO this stuff seems over-complicated...

    def set_entry(self, entry, *subsubpath, reload=False):
        if entry:
            if not self.entry or entry != self.entry or reload:
                self.loading = True

                async def load_children():
                    self.log("_load_children", entry)
                    children = await self._load_children(entry)
                    self.loading = False
                    return children

                self.run_worker(
                    load_children,
                    exclusive=True,
                    group=type(self).__name__,
                    description=f"_load_children for {type(self).__name__} {type(entry)}.{entry.name}",
                )
            elif subsubpath != self._subsubpath:
                # Subpath changed, select the appropriate child
                # TODO not sure why this requires hashing, Entry has an __eq__ method
                filtered = self.get_filtered_children(self.entries, self.pattern)
                if subsubpath and filtered:
                    child = subsubpath[0]
                    try:
                        index = filtered.index(child)
                    except ValueError:
                        # Guess it's not in the list any more, might happen on reload?
                        self.log(
                            "Selected entry is not in list?",
                            entry=child,
                            entries=filtered,
                        )
                        index = 0
                    table = self.query_one(DataTable)
                    self.call_later(table.move_cursor, row=index)

        self.entry = entry
        self._subsubpath = subsubpath

    async def watch_entries(self, _, children) -> None:
        await self._update_children(children, self.pattern)

    async def watch_pattern(self, _, pattern: str) -> None:
        await self._update_children(self.entries, pattern)

    async def _load_children(self, entry) -> None:
        "Load child entries"
        try:
            self.entries = await entry.get_children()
            self._error = None
        except Exception as e:
            self._error = e
            self.entries = []
            self.loading = False

    def _get_selected_index(self, children):
        if len(self.subpath) > 1:
            child = self.subpath[1]
            try:
                index = tango.utils.CaselessList(c.name for c in children).index(
                    child.name
                )
            except IndexError:
                index = 0
        else:
            index = 0
        return index

    async def _update_children(
        self,
        entries: list,
        pattern: str,
    ):
        with self.app.batch_update():
            filtered = self.get_filtered_children(entries, pattern)
            index = self._get_selected_index(filtered)
            table = self.query_one(DataTable)
            table.fixed_columns = 1
            table.clear()
            for entry in filtered:
                row, kwargs = await self.get_row(entry)
                table.add_row(*row, **kwargs)
            self.call_later(table.move_cursor, row=index)
            table.focus()

    async def get_row(self, entry) -> tuple[Sequence, dict]:
        """
        Return a sequence of renderables to represent an entry in the table,
        and a dict of keyword arguments to table.add_row
        The sequence of renderables must match the COLUMNS class var.
        TODO clunky, can it be made more friendly?
        """
        raise NotImplementedError("Listing widget must implement 'get_row'.")

    def get_filtered_children(self, children, pattern: str) -> Sequence:
        "This can be overridden if more advanced matching is needed"
        # TODO maybe filtering children should be part of the entry instead?
        if pattern:
            pattern = pattern.casefold()
            return [a for a in children if pattern in a.name.casefold()]
        return children

    def on_data_table_row_selected(self, message: DataTable.RowSelected) -> None:
        entries = self.get_filtered_children(self.entries, self.pattern)
        entry = entries[message.cursor_row]
        self.post_message(NavigateTo(entry))

    @property
    def highlighted_entry(self):
        table = self.query_one(DataTable)
        entries = self.get_filtered_children(self.entries, self.pattern)
        entry = entries[table.cursor_row]
        return entry

    async def on_input_submitted(self, msg) -> None:
        # TODO live filter the list
        self.pattern = msg.value
        pattern_input = self.query_one(Input)
        if not msg.value:
            pattern_input.display = False
        pattern_input.can_focus = False
        self.query_one(DataTable).focus()

    def action_filter(self) -> None:
        pattern_input = self.query_one(".pattern")
        pattern_input.display = True
        pattern_input.can_focus = True
        pattern_input.focus()

    def action_grow(self) -> None:
        if self.parent:
            height = self.parent.styles.height
            if height:
                self.parent.styles.height = f"{height.value * 2}{height.symbol}"

    def action_shrink(self) -> None:
        if self.parent:
            height = self.parent.styles.height
            if height:
                self.parent.styles.height = f"{height.value / 2}{height.symbol}"

    async def action_reset(self):
        pane = await self.app.screen.active_browser
        pane.query_one("#details-list").styles.height = None
        pane.query_one("#details").styles.height = None


class Banner(EntryWidget, Generic[EntryType]):
    """
    Base class for "banner" widgets which are intended for compact
    representation of something (e.g. device state) at the top right
    side.
    """
