"""
The "search" feature. Each plugin root can have its own search
implementation. The search interface opens as a modal overlay.
"""

import asyncio
from inspect import signature

from textual import work
from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.widgets import Input, OptionList, Label
from textual.containers import Vertical, Horizontal
from textual.widgets.option_list import Option
from textual.binding import Binding


class SearchResult(Option):

    def __init__(self, path, *args, **kwargs):
        self.path = path
        super().__init__(*args, **kwargs)


class SearchScreen(ModalScreen):

    DEFAULT_CSS = """
    SearchScreen {
        height: auto;
        &>Vertical {
            border: hkey $primary;
            background: $background-lighten-1;
            height: auto;
            max-height: 1fr;
            margin: 3 6;
            Horizontal {
                width: 1fr;
                height: auto;
                padding: 1;
            }
            Input {
                height: 1;
                background: transparent;
                border: none;
                width: 1fr;
                &:focus {
                    border: none;
                }
            }
            OptionList {
                border: none;
                width: 100%;
                height: 1fr;
                display: none;
                background: transparent;
                margin-top: 1;
                &:focus {
                    border: none;
                }
            }
        }
    }
    """

    BINDINGS = [
        ("escape", "close", "Close"),
        Binding("down", "down", "Down", show=False),
    ]

    def __init__(self, provider, *args, **kwargs):
        self.provider = provider
        self.sig = signature(provider)
        self.rows = []
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        with Vertical():
            with Horizontal():
                for name, arg in self.sig.parameters.items():
                    yield Label("🔎")
                    yield Input(placeholder=name, classes="argument")
            yield OptionList()

    def action_close(self) -> None:
        search_input = self.query(Input)
        if any(s.has_focus for s in search_input):
            self.dismiss()
        search_input.focus()

    def action_down(self):
        optionlist = self.query_one(OptionList)
        optionlist.focus()

    async def on_input_changed(self, message: Input.Submitted) -> None:
        arg_inputs = self.query(Input)
        args = [i.value for i in arg_inputs]
        self._search(args)

    async def on_input_submitted(self, message: Input.Submitted) -> None:
        arg_inputs = self.query(Input)
        args = [i.value for i in arg_inputs]
        self._search(args, sleep=0, focus=True)

    @work(exclusive=True)
    async def _search(self, args, sleep=0.5, focus=False):
        # This is for "throttling"; we will not search while the user is
        # typing at more than one character per 0.5 s.
        if sleep:
            await asyncio.sleep(0.5)
        # Do the actual searching
        provider = self.provider
        results = await provider(*args)
        optionlist = self.query_one(OptionList)
        optionlist.clear_options()
        if results:
            optionlist.add_options(
                SearchResult(path, name)
                for name, path in results
            )
            optionlist.display = True
            optionlist.action_first()
            if focus:
                optionlist.focus()

    async def on_option_list_option_selected(self, message):
        result = message.option
        path = result.path
        self.dismiss(path)

    def on_list_view_highlighted(self, message):
        self.query_one("#results").scroll_to_widget(message.control)
