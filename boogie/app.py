from __future__ import annotations
import logging
from logging.handlers import RotatingFileHandler
import os
import time
from typing import Callable, Sequence

from platformdirs import user_data_dir, user_log_dir
from textual import on, work
from textual.app import App, ComposeResult
from textual.screen import Screen
from textual.widgets import Header, TabbedContent, TabPane, Tabs, Footer
from textual.logging import TextualHandler
from textual.binding import Binding
from textual.color import Color
from textual.events import Key

from . import get_plugins
from .entry import Entry, get_entry
from .messages import NavigateTo, Reload
from .browser import Browser
from .history import History, HistoryBrowser
from .bookmarks import Bookmarks, BookmarksBrowser
from .widgets.modal import HelpBrowser
from .widgets.entry import MonitorWidget
from .monitor import MonitorScreen
from .path import Path
from .cache import clear_cache
from .command import BoogieCommands


# Log to file, remove this later
log_dir = user_log_dir('Boogie')
import tango
tango_host, _ = tango.ApiUtil.get_env_var("TANGO_HOST").split(":")
os.makedirs(log_dir, exist_ok=True)
logging.basicConfig(
    level="NOTSET",
    format="%(asctime)s %(levelname)s %(name)s %(message)s",
    handlers=[
        TextualHandler(),
        RotatingFileHandler(
            filename=f"{log_dir}/boogie_{tango_host}.log",
            maxBytes=10_000_000,
        ),
    ],
)

logger = logging.getLogger(__name__)


class Boogie(App):
    """
    The top level application object. Just loads some screens.
    """

    TITLE = "🪩Boogie"

    BINDINGS = [
        # Binding("ctrl+c", "", None, show=False),
        Binding("f1", "help", "Help"),
        Binding("f10", "app.quit", "Exit", show=False),
        # Binding("space", "command_palette", show=False, priority=True),
    ]

    COMMANDS = set()  # Disable default commands, they aren't useful

    def __init__(
        self,
        path: Sequence[str] | None = None,
        history_db_path: str | None = None,
        bookmarks_db_path: str | None = None,
        plugins=None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

        self.initial_path = path
        data_dir = user_data_dir("Boogie")
        os.makedirs(data_dir, exist_ok=True)
        history_db_path = history_db_path or os.path.join(data_dir, "history.db")
        bookmarks_db_path = history_db_path or os.path.join(data_dir, "bookmarks.db")
        self.history = History(history_db_path)
        # TODO this might better live in the History class
        self._current_history_id: int | None = None
        self.bookmarks = Bookmarks(bookmarks_db_path)

        if plugins:
            self._plugins = plugins
        else:
            self._plugins = get_plugins()
        self.log("Found plugins", plugins=[plugin.__name__ for plugin in self._plugins])

        self._plugin_roots = {
            info["root"].name.lower(): info
            for plugin in self._plugins
            for info in getattr(plugin, "BROWSERS", [])
        }

    def on_mount(self) -> None:
        # Plugins may specify custom CSS.
        # TODO this means that plugin CSS could collide, unless carefully name-spaced.
        # For now there's no way to load CSS more dynamically.

        MainScreen.CSS_PATH = [
            *sum([getattr(plugin, "EXTRA_CSS", []) for plugin in self._plugins], []),
            "main.tcss",
        ]

        MainScreen.COMMANDS = set.union(
            MainScreen.COMMANDS, *(
                plugin.COMMAND_PROVIDERS
                for plugin in self._plugins
                if hasattr(plugin, "COMMAND_PROVIDERS")
            )
        )

        # TODO create this on demand?
        self.monitor_screen = MonitorScreen()
        self.install_screen(self.monitor_screen, "monitor")
        self.push_screen("monitor")

        main_screen = MainScreen(self.initial_path)
        self.install_screen(main_screen, "main")
        self.push_screen("main")

    async def get_entry(self, path_str: Sequence[str]) -> Entry:
        "Return the Entry corresponding to a path"
        root_name = path_str[0].lower()
        root = self._plugin_roots[root_name]["root"]
        entry, error = await get_entry(root, path_str)
        if error:
            self.log(f"Error finding entry {path_str}", error=error)
            # self.notify(error)
        return entry

    async def on_navigate_to(self, message: NavigateTo) -> None:
        await self.navigate_to(message.entry)

    @property
    def entry(self):
        screen = self.get_screen("main")
        tabbed = screen.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.get_pane(tabbed.active)
        browser = pane.query_one(Browser)
        return browser.entry

    async def navigate_to(self, entry: Entry, store=True) -> None:
        "Direct the application to the given location (entry)"
        self.log("Navigating to", entry=entry, store=store)
        root = entry.path[0]
        screen = self.get_screen("main")
        self.switch_screen("main")
        tabbed = screen.query_one("#root-tabs", expect_type=TabbedContent)
        tab_id = root.name.lower()
        tabbed.active = tab_id
        pane = tabbed.get_pane(tab_id)
        await screen._load_pane(pane)  # type: ignore  # TODO
        browser = pane.query_one(Browser)
        await browser.mounted
        browser.set_entry(entry)
        if store:
            assert screen.sub_title
            self.update_history(entry, screen.sub_title)

    async def action_go_to(self, path_str: str) -> None:
        "Handle clicked links"
        # Links can occur in any widget text, and are specified like this:
        # [@click=screen.go_to('server:dserver:TangoTest')]Linky link[/]
        path = path_str.split(":")
        entry = await self.get_entry(path)
        await self.navigate_to(entry)

    @work
    async def update_history(self, entry: Entry, key: str) -> None:
        "Store a path in the history database"
        path_str = [str(entry) for entry in entry.path]
        id_ = self.history.append(key, path_str, "path")
        self._current_history_id = id_

    async def create_bookmark(self, entry) -> None:
        print("cteate_bookmark", entry)
        path_str = [str(entry) for entry in entry.path]
        key = self.screen.sub_title
        if key:
            self.bookmarks.append(key=key, path=path_str)
            self.notify("Created bookmark")

    async def run_entry_method(self, entry: Entry, method: Callable, **kwargs):
        """
        Run a method on an entry.
        Doing it this way causes it to leave a record in the
        history, so it's probably a good idea for methods that modify
        things. Otherwise it should be the same as running it directly.

        Note that the cmd is assumed to be async.
        TODO maybe better to come up with a more automatic way of handling
        this, like a decorator on methods.
        """
        name = method.__name__
        assert name != "path", f"Sorry, '{name}' is not an allowed method name"
        self.log("Running entry method", path=entry.path, name=name, kwargs=kwargs)
        path_str = [str(entry) for entry in entry.path]
        t0 = time.time()
        result = await method(**kwargs)
        dt = time.time() - t0
        kwargs["duration"] = round(dt, 6)
        assert self.screen.sub_title
        self.history.append(
            key=self.screen.sub_title, path=path_str, action=name, data=kwargs
        )
        return result

    def copy_to_clipboard(
        self, text: str, message: str = "Copied to clipboard"
    ) -> None:
        super().copy_to_clipboard(text)
        self.notify(message)

    def action_help(self) -> None:
        self.push_screen(HelpBrowser())

    def action_show_history(self) -> None:
        "Display the history browser widget"

        async def go_to_path(result=None) -> None:
            "Handle result from the history browser"
            if result:
                history_id, path = result
                entry = await self.get_entry(path)
                await self.navigate_to(entry, store=False)
                self._current_history_id = history_id

        if self.sub_title is not None:
            self.push_screen(
                HistoryBrowser(self.history, self.sub_title, self._current_history_id),
                go_to_path,
            )
        else:
            self.notify("Sorry, not connected...", severity="error")

    def action_show_bookmarks(self) -> None:
        "Display the history browser widget"

        async def go_to_path(result: tuple | None = None) -> None:
            "Handle result from the history browser"
            if result:
                id_, path = result
                entry = await self.get_entry(path)
                await self.navigate_to(entry, store=False)

        if self.sub_title is not None:
            self.push_screen(
                BookmarksBrowser(self.bookmarks, self.sub_title), go_to_path
            )
        else:
            self.notify("Sorry, not connected...", severity="error")

    async def add_to_monitor(self, entry: Entry, show: bool = False) -> None:
        "Add an entry to the monitor screen."
        monitor_class = MonitorWidget.get_widget_class(type(entry))
        if monitor_class:
            monitor = monitor_class()
            await self.monitor_screen.add(monitor)
            monitor.set_entry(entry)
            if show:
                self.switch_screen("monitor")
            else:
                self.notify(
                    f"Monitoring {entry}.\nPress F4 to show the monitor screen."
                )
        else:
            self.notify(
                f"Oops, no monitor widget available for {type(entry)}!",
                severity="warning",
            )

    def action_show_monitor(self):
        self.switch_screen("monitor")


class MainScreen(Screen):
    """
    "Main" screen, housing the plugin tabs, handling global key bindings,
    and generally wiring things together.
    """

    BINDINGS = [
        Binding("f2", "show_history", "History"),
        Binding("f3", "show_bookmarks", "Bookmarks"),
        Binding("f4", "show_monitor", "Monitor"),
        Binding("f5", "reload", "Reload"),
        Binding("ctrl+left", "go_back", "Go back", show=False),
        Binding("ctrl+right", "go_forward", "Go forward", show=False),
        Binding("ctrl+up", "go_up", "Go up", show=False),
    ]

    COMMANDS = {BoogieCommands}

    app: Boogie

    def __init__(
        self, path: Sequence[str] | None = None, plugins=None, *args, **kwargs
    ):

        self._path = path

        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield Header()
        with TabbedContent(id="root-tabs"):
            index = 1
            for name, info in self.app._plugin_roots.items():
                get_title = info.get("get_title")
                search = info.get("search")
                root = info["root"]
                yield TabPane(
                    f"[dim]{index}:[/]{root.name}",
                    Browser(get_title, search, id=name.lower()),
                    id=name.lower(),
                )
                index += 1
        yield Footer()

    async def on_mount(self) -> None:

        # Prevent from focusing the main tab bar
        self.query_one(Tabs).can_focus = False

        if self._path:
            self.log("Going to path", path=self._path)
            entry = await self.app.get_entry(self._path)
            await self.app.navigate_to(entry)

    @property
    async def active_browser(self) -> Browser | None:
        tabbed: TabbedContent = self.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.active_pane
        if pane:
            browser = pane.query_one(Browser)
            return browser
        return None

    @on(TabbedContent.TabActivated, "#root-tabs")
    async def tab_changed(self, message: Tabs.TabActivated) -> None:
        tabbed: TabbedContent = self.query_one("#root-tabs", expect_type=TabbedContent)
        pane = tabbed.active_pane
        await self._load_pane(pane)
        self._focus_pane(pane)

    async def _load_pane(self, pane) -> None:
        "Make sure the given pane is 'loaded', i.e. provided with an entry"
        browser = pane.query_one(Browser)
        if not browser.entry:
            root_class = self.app._plugin_roots[browser.id]["root"]
            browser.set_entry(root_class())
            # TODO this is a way to avoid a "race condition" at the initial
            # entry setting... Find a better way!
            await browser.rooted.wait()

    def _focus_pane(self, pane) -> None:
        # TODO this is a hack, to ensure that we have focus somewhere after
        # switching tabs. Would be better to find a way to just restore the
        # previous focus.
        browser = pane.query_one(Browser)
        browser.focus()
        self.focus_next()

    def set_sub_title(self, title) -> None:
        # # Try to find a "unique" color for the title, to make it more obvious
        # # which control system you are connected to.
        # h = persistent_hash(title[: len(title) // 2]) % 100
        # s = persistent_hash(title[len(title) // 2:]) % 100
        # color = Color.from_hsl(h / 100, 0.5 + s / 200, 0.5)
        # self.query_one("HeaderTitle").styles.color = "#77aaff"
        self.sub_title = title

    async def action_reload(self) -> None:
        await self.reload()

    async def on_reload(self, message: Reload) -> None:
        await self.reload()

    async def reload(self, quiet=False) -> None:
        "Clear cache and refresh all widgets"

        clear_cache()
        # TODO reload all tabs?
        # TODO do we need to clean up the old listener?
        browser = await self.active_browser
        if browser:
            await browser.reload()
            if not quiet:
                self.app.notify("Reloaded")

    @on(Path.NavigateBack)
    async def action_go_back(self) -> None:
        "Step back in history"
        self.log("Action go back", history_id=self.app._current_history_id)
        try:
            assert self.sub_title
            history_id, path = self.app.history.get_previous(
                self.sub_title, self.app._current_history_id
            )
        except (IndexError, AssertionError) as e:
            self.notify(f"Sorry, failed to navigate: {e}", severity="warning")
            return
        entry = await self.app.get_entry(path)
        self.log("Going back to", entry=entry)
        if entry:
            await self.app.navigate_to(entry, store=False)
            self.app._current_history_id = history_id
        else:
            self.notify(f"No entry found for {path}..?")

    @on(Path.NavigateForward)
    async def action_go_forward(self) -> None:
        "Step forward in history"
        try:
            history_id, path = self.app.history.get_next(
                self.sub_title, self.app._current_history_id
            )
        except IndexError:
            return
        entry = await self.app.get_entry(path)
        if entry:
            await self.app.navigate_to(entry, store=False)
            self.app._current_history_id = history_id

    @on(Path.NavigateUp)
    async def action_go_up(self) -> None:
        "Go to parent"
        browser = await self.active_browser
        if browser and browser.entry and browser.entry.parent:
            await self.app.navigate_to(browser.entry.parent, store=False)

    async def on_key(self, event: Key) -> None:
        if event.key.isnumeric():
            # Switch to top level tab using number keys
            tab_index = int(event.key) - 1
            try:
                tab_id = list(self.app._plugin_roots.keys())[tab_index]
            except IndexError:
                return
            tabbed: TabbedContent = self.query_one(
                "#root-tabs", expect_type=TabbedContent
            )
            tabbed.active = tab_id
            # Switching tabs "programmatically" not yet trigger an event
            # See https://github.com/Textualize/textual/issues/3869
            await self._load_pane(tabbed.active_pane)
            self._focus_pane(tabbed.active_pane)


def get_app() -> Boogie:
    from argparse import ArgumentParser

    parser = ArgumentParser()

    def colon_separated(s):
        return s.split(":")

    parser.add_argument(
        "path",
        type=colon_separated,
        nargs="?",
        help="Starting path. Colon separated, e.g. 'Device:sys:tg_test:1'",
    )
    parser.add_argument(
        "-n",
        "--no-swallow",
        action="store_true",
        help="Don't eat stderr output. Only useful for debugging",
    )

    args = parser.parse_args()

    return Boogie(path=args.path)


def main() -> None:
    get_app().run()
