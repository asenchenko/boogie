from datetime import datetime, timezone
import json
import logging
import sqlite3
from typing import Sequence, Any

from textual.app import ComposeResult
from textual.screen import ModalScreen
from textual.widgets import DataTable, Header, Footer, Input
from textual.containers import Vertical, Grid
from textual.reactive import var

logger = logging.getLogger(__name__)


class Bookmarks:
    """
    Stores bookmarks in a sqlite database.
    """

    def __init__(self, db_file):
        self.conn = sqlite3.connect(db_file)
        cursor = self.conn.cursor()
        cursor.execute(
            """
        CREATE TABLE IF NOT EXISTS bookmarks (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp TEXT DEFAULT CURRENT_TIMESTAMP,
            key TEXT NOT NULL,
            path TEXT,
            data TEXT
        )
        """
        )
        try:
            cursor.execute(
                """
            CREATE UNIQUE INDEX bookmarks_id_key_path
                ON bookmarks(id, key, path) ORDER BY id DESC
            CREATE UNIQUE INDEX bookmarks_id_time_key
                ON bookmarks(id, timestamp, key) ORDER BY id DESC
            """
            )
        except sqlite3.OperationalError:
            # This happens if the indices already exist, which is OK.
            # TODO what else can go wrong here?
            pass
        self.conn.commit()

    def append(self, key: str, path: Sequence[str], data=None):
        "Add a bookmark"
        cursor = self.conn.cursor()
        if data is not None:
            try:
                data = json.dumps(data)
            except TypeError as e:
                print(f"Failed to JSON encode arguments: {e}")
                data = "{}"
        cursor.execute(
            "INSERT INTO bookmarks (key, path, data) VALUES (?, ?, ?)",
            (key, ":".join(path), data),
        )
        self.conn.commit()
        logger.info(f"Added bookmark {key} {path}")

    def items(
        self,
        key: str | None = None,
        path: str = "%",
        offset: int = 0,
        limit: int = 100,
    ):
        "Get some path entries"
        logger.debug(f"Getting bookmark items {offset} to {offset + limit}")
        cursor = self.conn.cursor()
        query: tuple[str] | tuple[str, Sequence[Any]]
        path = "%" if not path else f"%{path.replace('', '%')}%"
        if key:
            query = (
                f"""
            SELECT id, timestamp, path, data
            FROM bookmarks
            WHERE key = ? AND path LIKE ?
            ORDER BY id DESC
            LIMIT {limit}
            OFFSET {offset}
            """,
                (key, path),
            )
        else:
            query = (
                f"""
            SELECT id, timestamp, path, data
            FROM bookmarks
            WHERE path LIKE ?
            ORDER BY id DESC
            LIMIT {limit}
            OFFSET {offset}
            """,
                (path,),
            )
        for row in cursor.execute(*query):
            yield row

    def get_item(self, id_):
        "Get a particular entry"
        cursor = self.conn.cursor()
        return list(
            cursor.execute(
                """
            SELECT *
            FROM bookmarks
            WHERE id = ?
            ORDER BY id DESC
            LIMIT 1
            """,
                (id_,),
            )
        )[0]


class BookmarksBrowser(ModalScreen):
    DEFAULT_CSS = """
    BookmarksBrowser {

    }
    BookmarksBrowser Grid {
        grid-size: 2;
        height: auto;
    }
    BookmarksBrowser Vertical {
    }
    BookmarksBrowser DataTable {
        height: 1fr;
    }
    """

    BINDINGS = [
        ("escape", "close", "Close"),
        ("m", "load_more", "Load more"),
    ]

    path_filter: var[str | None] = var(None, init=False)

    def __init__(self, bookmarks: Bookmarks, key: str, *args, **kwargs):
        self.bookmarks = bookmarks
        self.key = key
        self._offset = 0
        self._limit = 100
        super().__init__(*args, **kwargs)

    def compose(self) -> ComposeResult:
        yield Header()
        filters = Grid(id="filters")
        with filters:
            yield Input(id="path-filter", placeholder="Path")
        vertical = Vertical()
        with vertical:
            dt: DataTable[tuple[str, str, str]] = DataTable()
            dt.add_columns("Timestamp", "Path", "Comment")
            dt.cursor_type = "row"
            dt.zebra_stripes = True
            yield dt
        yield Footer()

    def on_mount(self):
        # TODO be more clever about this. Maybe some kind of
        # lazy loading?
        self.title = "Bookmarks"
        self.sub_title = self.key
        self.load()

    def watch_path_filter(self, _, __):
        self.load(restart=True)

    def watch_action_filter(self, _, __):
        self.load(restart=True)

    def load(self, restart=False):
        data_table = self.query_one(DataTable)
        if restart:
            self._offset = 0
            data_table.clear()

        paths = self.bookmarks.items(
            self.key, path=self.path_filter, offset=self._offset, limit=self._limit
        )
        data_table.fixed_columns = 1
        i = 0
        for i, (id_, time, path, args) in enumerate(paths):
            # print("paths", time, path, action)
            timestamp = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
            timestamp = timestamp.replace(tzinfo=timezone.utc).astimezone()
            comment = args.get("comment") if args else ""
            path = path.replace(":", "·")
            data_table.add_row(
                timestamp.strftime("%Y-%m-%d %H:%M:%S"), path, comment, key=str(id_)
            )
        # TODO if i < self._history_limit, we reached end of history.
        # Perhaps signal this somehow, e.g. disable the "more" action?
        self._offset += self._limit
        data_table.focus()

    def on_data_table_row_selected(self, message):
        bookmark_id = int(message.row_key.value)
        id_, _, timestamp, path, _ = self.bookmarks.get_item(bookmark_id)
        self.dismiss((id_, path.split(":")))

    def action_close(self):
        self.dismiss()

    def action_load_more(self):
        data_table = self.query_one(DataTable)
        data_table.fixed_columns = 1
        offset = self._offset
        self.load()
        data_table.move_cursor(row=offset)

    def on_input_submitted(self, message):
        if message.control.id == "path-filter":
            value = message.value
            if value:
                self.path_filter = message.value
            else:
                self.path_filter = None
