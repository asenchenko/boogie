from boogie.plugins.tango.utils import get_db


async def search_sardana_elements(pattern):
    db = await get_db()
    if "*" in pattern:
        pattern = pattern.replace("*", "%")
    else:
        pattern = f"%{pattern}%"
    pool_query = """
        SELECT DISTINCT server, name FROM device
        WHERE class = 'Pool'
    """
    pool_results = await db.query(pool_query)
    pool_devices = {
        server: device for server, device in zip(pool_results[::2], pool_results[1::2])
    }
    pools = ",".join(f"'{s}'" for s in pool_devices)

    elements_query = f"""
        SELECT DISTINCT server, class, alias FROM device
        WHERE server IN ({pools})
        AND class NOT IN ('MacroServer','Door')
        AND alias LIKE '{pattern}'
        ORDER BY server, alias
        LIMIT 100
    """
    elements = await db.query(elements_query)

    # TODO also search for macroservers, doors, ...

    def get_path(server, clss, alias):
        if clss == "Pool":
            return ["Sardana", "Pools", pool_devices[server]]
        if clss == "Controller":
            return ["Sardana", "Pools", pool_devices[server], "Controllers", alias]
        return ["Sardana", "Pools", pool_devices[server], "Elements", clss, alias]

    return [
        (
            f"{pool_devices[server]} -> [b]{alias}[/b] ({clss})",
            get_path(server, clss, alias),
        )
        for server, clss, alias in zip(elements[::3], elements[1::3], elements[2::3])
    ]
