from dataclasses import dataclass

from boogie.plugins.tango.entry import TangoEntry
from .pools import SardanaPools
from .macroservers import SardanaMacroServers


@dataclass(frozen=True, repr=False, eq=False)
class SardanaRoot(TangoEntry):
    parent: TangoEntry | None = None
    name: str = "Sardana"

    _children = {
        "macroservers": SardanaMacroServers,
        "pools": SardanaPools,
    }

    async def get_children(self):
        return [child(self) for child in self._children.values()]

    async def get_child(self, name):
        return self._children[name.lower()](self)

    def get_pool(self, name):
        pools = SardanaPools(self)
        pool = pools.get_child(name)
        return pool

    # async def get_filtered_nodes(self, pattern) -> dict:

    #     pattern = pattern.replace("*", "%")
    #     db = await get_db()
    #     pools_query = """
    #     SELECT server, name
    #     FROM device
    #     WHERE class = 'Pool'
    #     """
    #     _, pool_results = await db.command_inout("DbMySQLSelect", pools_query)
    #     server_pools = {
    #         server: device.lower()
    #         for server, device in zip(pool_results[::2], pool_results[1::2])
    #     }
    #     element_query = f"""
    #     SELECT DISTINCT server, class, name, alias
    #     FROM device
    #     WHERE class IN ('Motor','PseudoMotor','Controller','Counter')
    #     AND alias LIKE '{pattern}'
    #     ORDER BY server
    #     """
    #     # TODO order the results by pool device name, not server

    #     _, el_results = await db.command_inout("DbMySQLSelect", element_query)
    #     tree: EntryTree = {}
    #     for el_server, el_clss, el_dev, el_alias in zip(el_results[::4],
    #                                                     el_results[1::4],
    #                                                     el_results[2::4],
    #                                                     el_results[3::4]):
    #         if el_clss == "Controller":
    #             # TODO needs special handling
    #             continue

    #         pools_entry = SardanaPools(self)
    #         pools = tree.setdefault(pools_entry, {})

    #         pool_device = server_pools[el_server]
    #         pool_entry = SardanaPool(pools_entry, pool_device)
    #         pool = pools.setdefault(pool_entry, {})
    #         elements_entry = await pool_entry.get_child("elements")
    #         elements = pool.setdefault(elements_entry, {})

    #         element_type_entry = await elements_entry.get_child(el_clss)
    #         element_type = elements.setdefault(element_type_entry, {})

    #         element_entry = await element_type_entry.get_child(
    #             name=el_alias, full_name=el_dev, pool=pool_device, type=el_clss)
    #         element_type[element_entry] = {}

    #     return tree
