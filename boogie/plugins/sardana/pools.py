"""
Entries for Sardana Pool and elements
"""

from __future__ import annotations
from abc import ABCMeta
from dataclasses import dataclass
from functools import lru_cache
import json
from typing import Sequence, TYPE_CHECKING

import tango
from tango.asyncio import DeviceProxy

from boogie.cache import cached
from boogie.plugins.tango.utils import get_device_proxy, get_db
from boogie.plugins.tango.entry import TangoEntry
from boogie.plugins.tango.device import (
    TangoDeviceAttributes,
    TangoDeviceProperties,
    TangoDeviceCommands,
)

if TYPE_CHECKING:
    from .entry import SardanaRoot


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPools(TangoEntry):

    parent: SardanaRoot
    name: str = "Pools"

    async def get_children(self):
        db = await get_db()
        query = "SELECT name FROM device WHERE class = 'Pool' ORDER BY name"
        _, pools = await db.command_inout("DbMySQLSelect", query)
        return [SardanaPool(self, pool) for pool in pools]

    async def get_child(self, name):
        return SardanaPool(self, name)

    async def get_filtered_nodes(self):
        return {}


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolElement(TangoEntry):

    name: str
    element_type: str | None = None

    # pool: str | None = None
    # full_name: str | None = None
    # manager: str | None = None
    # interfaces: Sequence[str] = ()
    # id: str | None = None

    # @cached
    # async def get_label(self):
    #     info = await self.get_info()
    #     style = self._get_label_style()
    #     if "axis" in info:
    #         return Text(f"{self.name} ({info['axis']})", style=style)
    #     return Text(self.name, style=style)

    @property
    def pool(self) -> SardanaPool:
        for entry in self.path:
            if isinstance(entry, SardanaPool):
                return entry
        raise KeyError("No pool found?!")

    def get_info(self):
        return self.pool.get_element_info(self.name)

    async def get_device(self) -> str:
        info = await self.get_info()
        full_name = info["full_name"]
        return "/".join(full_name.split("/")[-3:])

    async def get_dserver(self) -> str:
        proxy = await self.get_proxy()
        info = proxy.info()
        return info.server_id

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(await self.get_device())

    async def get_title(self) -> str:
        info = await self.get_info()
        if info:
            return f"{info['type']}: [b]{self.name}[/b]"
        return self.name

    async def get_children(self):
        return [
            TangoDeviceAttributes(self),
            TangoDeviceProperties(self),
            TangoDeviceCommands(self),
        ]

    async def get_child(self, name):
        if name.lower() == "attributes":
            return TangoDeviceAttributes(self)
        if name.lower() == "properties":
            return TangoDeviceProperties(self)
        if name.lower() == "commands":
            return TangoDeviceCommands(self)


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolMotor(SardanaPoolElement):
    element_type: str = "Motor"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolPseudoMotor(SardanaPoolElement):
    element_type: str = "PseudoMotor"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolMeasurementGroup(SardanaPoolElement):
    element_type: str = "MeasurementGroup"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolCTExpChannel(SardanaPoolElement):
    element_type: str = "CTExpChannel"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolZeroDExpChannel(SardanaPoolElement):
    element_type: str = "ZeroDExpChannel"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolOneDExpChannel(SardanaPoolElement):
    element_type: str = "OneDExpChannel"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolTwoDExpChannel(SardanaPoolElement):
    element_type: str = "TwoDExpChannel"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolIORegister(SardanaPoolElement):
    element_type: str = "IORegister"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolPseudoCounter(SardanaPoolElement):
    element_type: str = "PseudoCounter"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolTriggerGate(SardanaPoolElement):
    element_type: str = "PoolTriggerGate"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolElementsBase(TangoEntry, metaclass=ABCMeta):

    child_class: type | None = None
    element_type: str | None = None
    controller_name: str | None = None

    _child_classes = {
        "motor": SardanaPoolMotor,
        "pseudomotor": SardanaPoolPseudoMotor,
        "measurementgroup": SardanaPoolMeasurementGroup,
        "ctexpchannel": SardanaPoolCTExpChannel,
        "zerodexpchannel": SardanaPoolCTExpChannel,
        "onedexpchannel": SardanaPoolCTExpChannel,
        "twodexpchannel": SardanaPoolCTExpChannel,
        "ioregister": SardanaPoolIORegister,
        "pseudocounter": SardanaPoolPseudoCounter,
        "triggergate": SardanaPoolTriggerGate,
        # TODO more types
    }

    @property
    def pool(self) -> SardanaPool:
        for entry in self.path:
            if isinstance(entry, SardanaPool):
                return entry
        raise KeyError("No pool found?!")

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(self.pool)

    def get_elements(self) -> list[SardanaPoolElement]:
        pool = self.pool
        return pool.get_elements()

    async def get_children(self):
        try:
            elements = await self.get_elements()
            sorted_elements = sorted(elements.values(), key=lambda e: e["name"].lower())
            children = []
            for el in sorted_elements:
                # The elements may be filtered on type and/or on controller
                if (not self.element_type or el["type"] == self.element_type) and (
                    not self.controller_name or el["parent"] == self.controller_name
                ):
                    name = el["name"]
                    type_ = el["type"].lower()
                    child_class = self._child_classes.get(type_)
                    if child_class:
                        child = child_class(self, name)
                        children.append(child)
            return children
        except tango.DevFailed as e:
            print("Could not get children", e)
            return []

    async def get_child(self, name):
        try:
            elements = await self.parent.parent.get_elements()
        except tango.DevFailed as e:
            raise KeyError(f"Could not get child element {name}: {e.args[0].desc}")
        element = elements[name.lower()]
        return self._child_classes[element["type"].lower()](self, name)

    def __repr__(self):
        return f"{type(self).__name__}({self.name})"


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllers(SardanaPoolElementsBase):
    parent: SardanaPool
    name: str = "Controllers"

    element_type = "Controller"

    async def get_children(self):
        try:
            elements = await self.parent.get_elements()
        except tango.DevFailed:
            return []
        controllers = (
            element["name"]
            for element in elements.values()
            if element.get("type") == "Controller"
        )
        return [
            await self.get_child(ctrl)
            for ctrl in sorted(controllers, key=lambda c: c.lower())
        ]

    async def get_child(self, name):
        return SardanaPoolController(self, name)


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolController(SardanaPoolElement):

    parent: SardanaPoolControllers
    module: str | None = None
    klass: str | None = None
    file_name: str | None = None
    types: Sequence[str] = ()
    main_type: str | None = None
    language: str | None = None

    async def get_children(self):
        children = []
        children.append(
            SardanaPoolControllerElements(
                self,
                "Elements",
                controller_name=self.name,
            )
        )

        children.extend(await super().get_children())
        return children

    async def get_child(self, name):
        if name.lower() == "elements":
            return SardanaPoolControllerElements(
                self,
                "Elements",
                controller_name=self.name,
            )
        return await super().get_child(name)


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllerElements(SardanaPoolElementsBase):
    async def get_children(self):
        try:
            elements = await self.parent.parent.get_elements()
            # Here, we order by axis
            sorted_elements = sorted(
                (e for e in elements.values() if e["parent"] == self.controller_name),
                key=lambda e: e.get("axis"),
            )
            children = []
            for el in sorted_elements:
                # The elements may be filtered on type and/or on controller
                name = el["name"]
                type_ = el["type"].lower()
                child_class = self._child_classes.get(type_)
                if child_class:
                    child = child_class(self, name)
                    children.append(child)
            return children
        except tango.DevFailed:
            return []


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolPseudoMotors(SardanaPoolElementsBase):
    name: str = "PseudoMotor"

    element_type: str = "PseudoMotor"
    child_class: type = SardanaPoolPseudoMotor


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolMeasurementGroups(SardanaPoolElementsBase):
    name: str = "MeasurementGroup"

    element_type: str = "MeasurementGroup"
    child_class: type = SardanaPoolMeasurementGroup


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolCTExpChannels(SardanaPoolElementsBase):
    name: str = "CTExpChannel"

    element_type: str = "CTExpChannel"
    child_class: type = SardanaPoolCTExpChannel


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolZeroDExpChannels(SardanaPoolElementsBase):
    name: str = "ZeroDExpChannel"

    element_type: str = "ZedoDExpChannel"
    child_class: type = SardanaPoolZeroDExpChannel


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolOneDExpChannels(SardanaPoolElementsBase):
    name: str = "OneDExpChannel"

    element_type: str = "ZedoDExpChannel"
    child_class: type = SardanaPoolOneDExpChannel


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolTwoDExpChannels(SardanaPoolElementsBase):
    name: str = "TwoDExpChannel"

    element_type: str = "ZedoDExpChannel"
    child_class: type = SardanaPoolTwoDExpChannel


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolIORegisters(SardanaPoolElementsBase):
    name: str = "IORegister"

    element_type: str = "IORegister"
    child_class: type = SardanaPoolIORegister


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolMotors(SardanaPoolElementsBase):
    name: str = "Motor"

    element_type: str = "Motor"
    child_class: type = SardanaPoolMotor


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolPseudoCounters(SardanaPoolElementsBase):
    name: str = "PseudoCounter"

    element_type: str = "PseudoCounter"
    child_class: type = SardanaPoolPseudoCounter


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolTriggerGates(SardanaPoolElementsBase):
    name: str = "TriggerGate"

    element_type: str = "TriggerGate"
    child_class: type = SardanaPoolTriggerGate


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolElements(TangoEntry):

    parent: SardanaPool
    name: str = "Elements"

    _child_classes = {
        "motor": SardanaPoolMotors,
        "pseudomotor": SardanaPoolPseudoMotors,
        "measurementgroup": SardanaPoolMeasurementGroups,
        "ctexpchannel": SardanaPoolCTExpChannels,
        "zerodexpchannel": SardanaPoolZeroDExpChannels,
        "onedexpchannel": SardanaPoolOneDExpChannels,
        "twodexpchannel": SardanaPoolTwoDExpChannels,
        "ioregister": SardanaPoolIORegisters,
        "pseudocounter": SardanaPoolPseudoCounters,
        "triggergate": SardanaPoolTriggerGates,
        # TODO more types?
    }

    async def get_children(self):
        return [cls(self) for key, cls in self._child_classes.items()]

    async def get_child(self, name):
        return self._child_classes[name.lower()](self)

    @property
    def pool_device(self) -> str:
        return self.parent.name

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(self.pool_device)

    async def get_element(self, name) -> SardanaPoolElement | None:
        "Shortcut to getting an element by name"
        element = await self.parent.get_element_info(name)
        if element:
            eltype = await self.get_child(element["type"])
            return await eltype.get_child(name)
        return None


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllerLib(TangoEntry):
    # parent: Entry
    # name: str = "Unknown"
    # pool: str | None = None
    # full_name: str | None = None
    # manager: str | None = None
    # interfaces: list[str] | None = None
    # id: int | None = None
    # module: str | None = None
    # file_path: str | None = None
    # file_name: str | None = None
    # # path: str | None = None
    # description: str | None = None
    # elements: list[str] | None = None
    # exc_summary: str | None = None
    # exc_info: str | None = None

    async def get_pool(self):
        return self.parent.parent

    async def get_data(self):
        libs = await self.parent.get_controller_libs()
        return libs[self.name]


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllerLibs(TangoEntry):

    name: str = "ControllerLibs"

    leaf_node = True

    @property
    def pool_device(self):
        return self.parent.name

    async def get_pool_proxy(self):
        return await get_device_proxy(self.pool_device)

    async def get_pool(self):
        return self.parent

    @cached
    async def get_controller_libs(self):
        proxy = await self.get_pool_proxy()
        ctrllib_list = await proxy.read_attribute("ControllerLibList")
        ctrllibs = [json.loads(line) for line in ctrllib_list.value]
        return {lib["name"]: lib for lib in ctrllibs}

    async def get_children(self):
        libs = await self.get_controller_libs()
        return [SardanaPoolControllerLib(self, lib["name"]) for lib in libs.values()]

    async def get_child(self, name):
        libs = await self.get_controller_libs()
        if name in libs:
            return SardanaPoolControllerLib(self, name)
        raise KeyError()


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllerClass(TangoEntry):

    json_data: str  # Note: contains raw JSON from sardana
    # TODO a but unclear what's the best way here. The problem is how
    # to handle cases where entries' info is provided by the parent.
    # It's probably fine to have several ways, but it's confusing.
    # Now there are the following ways:
    #
    # 1. Add properties to the entry dataclass and provide at creation
    # 2. Just lump it into a JSON string at creation and have a method to
    #    extract it when needed.
    # 3. Fetch the data async from the parent when needed.
    #
    # 1 has the benefit of making typing nice, but it requires knowing
    #   the existing fields up front. Also breaks if Sardana changes them.
    # 2 seems kind of hacky but simple
    # 3 is neat but makes for more async behavior
    #
    # Going for 2 for now.

    async def get_pool(self):
        return self.parent.parent

    @property
    @lru_cache(1)
    def data(self):
        return json.loads(self.json_data)

    @property
    def description(self):
        return self.data.get("description")


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPoolControllerClasses(TangoEntry):

    name: str = "ControllerClasses"

    leaf_node = True

    async def get_pool_proxy(self):
        return await get_device_proxy(self.parent.name)

    async def get_pool(self):
        return self.parent

    @cached
    async def _get_controller_classes(self):
        proxy = await self.get_pool_proxy()
        ctrlcls_list = await proxy.read_attribute("ControllerClassList")
        return {json.loads(line)["name"]: line for line in ctrlcls_list.value}

    async def get_children(self):
        classes = await self._get_controller_classes()
        return [
            SardanaPoolControllerClass(self, name, json_data=data)
            for name, data in classes.items()
        ]

    async def get_child(self, name):
        classes = await self._get_controller_classes()
        return SardanaPoolControllerClass(self, name, json_data=classes[name])


@dataclass(frozen=True, repr=False, eq=False)
class SardanaPool(TangoEntry):
    _child_classes = {
        "elements": SardanaPoolElements,
        "controllers": SardanaPoolControllers,
        "controllerlibs": SardanaPoolControllerLibs,
        "controllerclasses": SardanaPoolControllerClasses,
    }

    async def get_child(self, name):
        return self._child_classes[name.lower()](self)

    async def get_children(self):
        return [await self.get_child(key) for key in self._child_classes]

    async def get_info(self) -> tango.DeviceInfo:
        db = await get_db()
        device = await self.get_device()
        return await db.get_device_info(device)

    async def get_class(self) -> str:
        info = await self.get_info()
        return info.class_name

    async def get_title(self):
        return f"Pool: [bold]{self.name}[/]"

    async def get_description(self):
        proxy = await get_device_proxy(self.name)
        return proxy.description()

    async def get_device(self):
        return self.name

    async def get_alias(self):
        db = await get_db()
        return await db.get_alias_from_device(self.name)

    async def get_import_info(self) -> None:
        db = await get_db()
        device = await self.get_device()
        _, info = await db.command_inout("DbImportDevice", device)
        return info

    async def get_db_info(self) -> tango.DeviceInfo:
        db = await get_db()
        return await db.get_device_info(self.name)

    async def get_proxy(self):
        return await get_device_proxy(self.name)

    # @cached
    async def get_elements(self) -> dict[str, dict]:
        "Return a dict of all the elements in the pool"
        proxy = await self.get_proxy()
        _, elements = (await proxy.read_attribute("Elements")).value
        return tango.utils.CaselessDict(
            {el["name"]: el for el in json.loads(elements)["new"] if "name" in el}
        )

    # @cached
    async def get_element_info(self, alias):
        elements = await self.get_elements()
        return elements.get(alias)

    async def get_controller_class_info(self, name):
        proxy = await self.get_proxy()
        info = await proxy.command_inout("GetControllerClassInfo", name)
        return json.loads(info)
