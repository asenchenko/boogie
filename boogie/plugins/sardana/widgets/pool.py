from textual.widgets import Static

from boogie.plugins.tango.widgets.device import TangoDeviceDetailsBase
from ..pools import SardanaPool
from boogie.plugins.tango.widgets.device import DeviceState
from boogie.plugins.tango.widgets.details import TangoBanner


class PoolDetails(TangoDeviceDetailsBase[SardanaPool]):
    pass


class PoolBanner(TangoBanner[SardanaPool]):

    DEFAULT_CSS = """
    PoolBanner {
        height: 3;
        layout: horizontal;
    }
    PoolBanner #name {
        width: 1fr;
        padding: 1 2;
        content-align: center middle;
    }
    PoolBanner DeviceState {
        width: 15;
    }
    """

    def compose(self):
        yield Static(id="name")
        yield DeviceState()

    async def _update(self, entry):
        self.query_one(DeviceState).set_entry(entry)
        title = await entry.get_title()
        self.query_one("#name").update(f"{title}")
