from __future__ import annotations
import json

import tango
from textual.widgets import Static, ProgressBar

from boogie.widgets.entry import EntryWidget
from boogie.plugins.tango.listener import get_listener
from boogie.plugins.tango.widgets.device import TangoDeviceDetailsBase
from boogie.plugins.tango.widgets.attribute import TangoAttributeValueNumeric
from .element import ElementState
from ..macroservers import SardanaMacroServerDoor
from ..pools import SardanaPoolElement


class DoorDetails(TangoDeviceDetailsBase[SardanaMacroServerDoor]):

    ACTIONS = [
        ("stop_macro", "Stop macro", "error", "Stop the current macro"),
        (
            "pause_macro",
            "Pause macro",
            "warning",
            "Pause the current macro, temporarily",
        ),
        ("resume_macro", "Resume macro", "primary", "Resume a paused macro"),
    ]

    def compose(self):
        yield from super().compose()
        yield DoorMacroStatus()
        yield DoorReservedElements()

    async def _update(self, entry):
        await super()._update(entry)
        self.query_one(DoorMacroStatus).set_entry(entry)
        self.query_one(DoorReservedElements).set_entry(entry)


class DoorMacroStatus(EntryWidget):

    DEFAULT_CSS = """
    DoorMacroStatus {
        layout: vertical;
        height: auto;
        padding: 0 1;
    }
    """

    def compose(self):
        yield MacroProgress()

    def on_mount(self):
        self.classes = "section"
        self.border_title = "Macro status"

    async def _update(self, entry):
        attr = f"{entry.name}/MacroStatus"
        listener = await get_listener()
        prog = self.query_one(MacroProgress)
        async for event in listener.listen([attr]):
            attr, data = event
            if isinstance(data, tango.DevFailed):
                continue
            _, json_value = data.value
            if json_value:
                statuses = json.loads(json_value)
                with self.app.batch_update():
                    for status in statuses:
                        prog.set_status(status)


class MacroProgress(Static):

    DEFAULT_CSS = """
    MacroProgress {
        height: auto;
        width: 1fr;
        layout: vertical;
        #macro {
            height: 1;
            width: 1fr;
            overflow-x: hidden;
            text-style: bold;
        }
    }
    """

    def compose(self):
        yield Static("<unknown macro>", id="macro")
        yield ProgressBar(total=100, show_eta=False)

    def set_status(self, status: dict) -> None:
        macro_id = status.get("id")
        if macro_id:
            bar = self.query_one(ProgressBar)
            label = self.query_one("#macro", expect_type=Static)
            if macro := status.get("macro_line"):
                bar.progress = 0
                label.update(macro)
            state = status.get("state")
            if state == "step":
                step = status["step"]
                bar.progress = step
            elif state == "pause":
                # TODO set style?
                pass
            elif state in {"finish", "stop"}:
                bar.progress = 100


class ReservedElementInfo(EntryWidget):

    DEFAULT_CSS = """
    ReservedElementInfo {
        layout: horizontal;
        height: auto;
    }
    """

    def compose(self):
        yield ElementState()
        yield TangoAttributeValueNumeric()

    async def _update(self, entry: SardanaPoolElement):
        self.query_one(ElementState).set_entry(entry)
        if entry.element_type in {"Motor", "PseudoMotor"}:
            tv = self.query_one(TangoAttributeValueNumeric)
            await self.mount(tv)
            attributes_entry = await entry.get_child("attributes")
            position_entry = await attributes_entry.get_child("position")
            tv.set_entry(position_entry)


class DoorReservedElements(EntryWidget):
    DEFAULT_CSS = """
    DoorReservedElements {
        layout: vertical;
        height: auto;
        padding: 0 1;
        Button.physical {
            height: 1;
            width: auto;
            border: none;
            background: transparent;
            padding: 0;
            margin: 0 1;
            min-width: 0;
            text-style: underline;
        }
        Button.polled:hover {
            color: $secondary;
        }
    }
    """

    def on_mount(self):
        self.border_title = "Reserved elements"
        self.classes = "section"

    async def _update(self, entry):
        attr = f"{entry.name}/ReservedElements"
        old_value = None
        listener = await get_listener()
        async for event in listener.listen([attr]):
            attr, data = event
            if isinstance(data, tango.DevFailed):
                self.display = False
            else:
                _, json_value = data.value
                try:
                    reserved_elements = json.loads(json_value)
                except ValueError as e:
                    print(f"Could not decode reserved elements as JSON: {e}")
                    continue
                if reserved_elements:
                    if json_value != old_value:
                        # Got a new set of reserved elements, let's update
                        # the state widgets.
                        with self.app.batch_update():
                            self.query(ReservedElementInfo).remove()
                            for item in reserved_elements:
                                for element in item["elements"]:
                                    element_entry = await self.entry.get_element(
                                        element
                                    )
                                    element_state = ReservedElementInfo()
                                    self.mount(element_state)
                                    element_state.set_entry(element_entry)
                else:
                    self.query(ReservedElementInfo).remove()
                old_value = json_value
