from __future__ import annotations
from typing import Generic

from textual.containers import Horizontal
from textual.widgets import Static, Button
import tango

from boogie.messages import NavigateTo
from boogie.entry import EntryType
from boogie.widgets.misc import InfoTable
from boogie.plugins.tango.widgets.error import TangoError
from boogie.plugins.tango.widgets.device import (
    DeviceState,
    DeviceStatus,
)
from boogie.plugins.tango.utils import get_db
from boogie.plugins.tango.listener import get_listener
from boogie.plugins.tango.widgets.details import (
    TangoDetails,
    TangoEntryWidget,
    TangoBanner,
)

from ..pools import SardanaPoolElement


class ElementDetails(TangoDetails, Generic[EntryType]):
    """
    Base class for Sardana element detail widgets
    """

    DEFAULT_CSS = """
    ElementDetails {
        border-top: none;
        layout: grid;
        height: 1fr;
        grid-size: 1;
        grid-rows: auto;
        overflow-y: auto;

        .actions {
            dock: bottom;
        }

        &.error .actions {
            display: none;
        }
    }
    """

    def compose(self):
        yield ElementInfo()
        yield ElementStatus()
        with Horizontal(classes="actions"):
            for method, title, variant, tooltip in self.ACTIONS:
                b = Button(title, variant=variant, id=method, classes="action")
                b.tooltip = tooltip
                yield b
        yield TangoError()

    async def _update(self, entry):
        for w in self.query("* > EntryWidget"):
            w.set_entry(entry)

    # @on(Button.Pressed, ".action")
    # async def button_stop(self, message):
    #     proxy = await self.entry.get_proxy()
    #     await proxy.command_inout(message.button.id)


class ElementStatus(DeviceStatus):

    DEFAULT_CSS = """
    ElementStatus {
        height: 7;
        overflow-y: auto;
        display: none;
    }
    """


class ElementInfoTable(InfoTable):
    FIELDS = [
        ("Name", "name"),
        ("Device", "device"),
        ("Pool", "pool"),
        ("Controller", "controller"),
        ("Axis", "axis"),
        ("Module", "module"),
        ("Class", "klass"),
    ]


class ElementInfo(TangoEntryWidget):
    DEFAULT_CSS = """
    ElementInfo {
        width: 1fr;
        height: auto;
    }
    """

    def compose(self):
        yield ElementInfoTable()

    def on_mount(self):
        self.border_title = "Info"
        self.classes = "section"

    async def _get_device_link(self, entry):
        device_name = await entry.get_device()
        path = f"Device:{device_name.replace('/', ':')}"
        return f"[@click=screen.go_to('{path}')]{device_name}[/]"

    def _get_pool_link(self, entry):
        pool = entry.pool
        path = f"Device:{pool.name.replace('/', ':')}"
        return f"[@click=screen.go_to('{path}')]{pool.name}[/]"

    async def _get_controller_link(self, entry, controller):
        db = await get_db()
        pool = entry.pool
        controller_alias = await db.get_alias_from_device(controller)
        path = f"Sardana:Pools:{pool.name}:Controllers:{controller_alias}"
        return f"[@click=screen.go_to('{path}')]{controller_alias}[/]"

    def _get_controller_class_link(self, entry, info):
        pool = entry.pool
        klass = info["klass"]
        path = "Sardana"
        path = f"Sardana:Pools:{pool.name}:ControllerClasses:{klass}"
        return f"[@click=screen.go_to('{path}')]{klass}[/]"

    async def _update(self, entry):
        raw_info = await entry.get_info()
        info = {
            **raw_info,
            "name": raw_info["name"],
            "device": await self._get_device_link(entry),
            "pool": self._get_pool_link(entry),
        }
        if "controller" in raw_info:
            info["controller"] = await self._get_controller_link(
                entry, raw_info["controller"]
            )
        if "klass" in info:
            info["klass"] = self._get_controller_class_link(entry, raw_info)
        self.query_one(InfoTable).set_config(info)


class ElementState(Static, TangoEntryWidget):
    DEFAULT_CSS = """
    ElementState {
        background: $background-lighten-2;
        height: 1;
        width: auto;
        margin-right: 1;
        padding: 0 1;
    }
    """

    async def _update(self, entry):
        last_value = None
        self.update(entry.name)
        self.classes = ""
        device = await entry.get_device()
        state_attr = f"{device}/state"
        status_attr = f"{device}/status"
        listener = await get_listener()
        async for event in listener.listen([state_attr, status_attr]):
            try:
                attr, data = event
                if attr.endswith("/state"):
                    if isinstance(data, tango.DevFailed):
                        if data.args[0].reason == "API_AttrNotFound":
                            pass
                        else:
                            state = "N/A"
                            self.classes = "dead"
                    else:
                        if data.value != last_value:
                            state = str(data.value)
                            self.classes = f"tango-state state-{state}"
                            last_value = data.value
                elif attr.endswith("/status"):
                    if isinstance(data, tango.DeviceAttribute):
                        self.tooltip = data.value
            except Exception as e:
                print("oops", e)

    def on_click(self):
        self.post_message(NavigateTo(self.entry))


class ElementBanner(TangoBanner[SardanaPoolElement]):

    DEFAULT_CSS = """
    ElementBanner {
        height: 3;
    }
    ElementBanner #name {
        width: 1fr;
        padding: 1 2;
        content-align: center middle;
    }
    ElementBanner DeviceState {
        width: 15;
    }
    """

    def compose(self):
        with Horizontal():
            yield Static(id="name")
            yield DeviceState()

    async def _update(self, entry):
        self.query_one(DeviceState).set_entry(entry)
        title = await entry.get_title()
        self.query_one("#name").update(f"{title}")
