from datetime import datetime
import json

from rich.text import Text
from rich.pretty import Pretty
from textual.containers import VerticalScroll
from textual.widgets import DataTable, Static

from boogie.messages import NavigateTo
from boogie.widgets.details import Details, DetailsList
from boogie.widgets.modal import ConfirmModal

# from boogie.plugins.tango.utils import suspend_app, edit_text
from ..macroservers import (
    SardanaMacroServerEnvironment,
    SardanaMacroServerEnvironmentVariable,
    SardanaMacroServerScanHistory,
    SardanaMacroServerScanHistoryItem,
    SardanaMacroServerGeneralHooks,
)


class EnvironmentListDetails(DetailsList[SardanaMacroServerEnvironment]):

    BINDINGS = [
        ("/", "filter", "Filter"),
        ("n", "create", "New"),
        ("delete", "delete", "Delete"),
    ]

    COLUMNS = (
        "Variable",
        "Type",
    )

    async def get_row(self, entry):
        return (
            Text(entry.name, style="bold"),
            type(entry.value).__name__,
        ), {}

    async def action_delete(self):
        data_table = self.query_one(DataTable)
        entry = self.entries[data_table.cursor_row]

        async def maybe_delete(delete):
            if delete:
                await entry.delete()
                self._load_entries()
                self.post_message(NavigateTo(self.entry))

        question = f"Really want to delete property\n'{str(entry)}'?"
        self.app.push_screen(ConfirmModal(question), maybe_delete)

    # async def action_create(self):
    #     async def maybe_create(name):
    #         if name:
    #             # await entry.delete()
    #             # self._load_entries()
    #             # self.post_message(GoToEntry(self.entry))
    #             print("create", name)
    #             with suspend_app(self.app):
    #                 print("edit")
    #                 value = edit_text()
    #             if value is not None:
    #                 await self.entry.create(name, value)
    #                 self.notify(f"Created property '{name}'")
    #                 self._load_entries()
    #                 new_entry = await self.entry.get_child(name)
    #                 self.post_message(NavigateTo(new_entry))
    #             else:
    #                 self.notify(f"Property '{name}' not created!")

    #     message = f"Creating new property for device\n'{self.entry.device}'"
    #     self.app.push_screen(GetStringModal(message, "Name"), maybe_create)


class EnvironmentVariableDetails(Details[SardanaMacroServerEnvironmentVariable]):

    DEFAULT_CSS = """
    EnvironmentVariableDetails {
        border-top: heavy $background-lighten-3;
        border-title-align: center;
        border-title-color: white;
    }
    """

    def compose(self):
        with VerticalScroll():
            yield Static(id="value")

    async def _update(self, entry):
        self.border_title = entry.name
        value = json.loads(entry.value)
        self.query_one("#value").update(Pretty(value))


class ScanHistoryDetailsList(DetailsList[SardanaMacroServerScanHistory]):

    BINDINGS = [
        ("/", "filter", "Filter"),
    ]

    COLUMNS = "#", "Macro", "Start", "Status", "Duration"

    async def get_row(self, entry):
        data = json.loads(entry.data)
        return (
            Pretty(data["serialno"]),
            Text(data["title"], style="bold"),
            datetime.fromtimestamp(data["startts"]).isoformat().rsplit(".", 1)[0],
            data["endstatus"],
            f'{data["endts"] - data["startts"]:.1f} s',
        ), {}


class ScanHistoryItemDetails(Details[SardanaMacroServerScanHistoryItem]):

    DEFAULT_CSS = """
    ScanHistoryItemDetails {
        border-top: heavy $background-lighten-3;
        border-title-align: center;
        border-title-color: white;
    }
    """

    def compose(self):
        with VerticalScroll():
            yield Static(id="value")

    async def _update(self, entry):
        data = json.loads(entry.data)
        self.border_title = f"#{data[f'serialno']}"
        self.query_one("#value").update(Pretty(data))


class ScanHistoryGeneralHooksListDetails(DetailsList[SardanaMacroServerGeneralHooks]):

    BINDINGS = [
        ("/", "filter", "Filter"),
    ]

    COLUMNS = "Place", "Hook(s)"

    async def get_row(self, entry):
        return (
            Text(entry.name, style="bold"),
            Text("\n".join(entry.hooks)),
        ), {}
