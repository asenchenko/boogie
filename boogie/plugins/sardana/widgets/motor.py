from __future__ import annotations

from textual.widgets import Label
from textual.containers import Horizontal

from boogie.plugins.tango.utils import get_db
from boogie.widgets.entry import EntryWidget
from ..pools import SardanaPoolMotor, SardanaPoolPseudoMotor
from .element import ElementDetails, ElementState


class MotorDetails(ElementDetails[SardanaPoolMotor]):

    ACTIONS = [
        ("stop", "Stop", "warning", "Stop the current motion"),
        ("abort", "Abort", "error", "Abort the current motion"),
    ]


class PseudoMotorDetails(ElementDetails[SardanaPoolPseudoMotor]):
    def compose(self):
        yield from super().compose()
        yield PseudoMotorPhysicalElements()

    async def _update(self, entry):
        await super()._update(entry)
        self.query_one(PseudoMotorPhysicalElements).set_entry(entry)


class PseudoMotorPhysicalElements(EntryWidget):
    DEFAULT_CSS = """
    PseudoMotorPhysicalElements {
        layout: grid;
        width: auto;
        height: auto;
        grid-gutter: 0 1;
        grid-columns: auto;
        display: none;
    }
    """

    def on_mount(self):
        self.border_title = "Physical elements"
        self.classes = "section"

    async def _update(self, entry):
        info = await entry.get_info()
        # physical_elements = info["physical_elements"]
        pool = entry.pool
        elements = await pool.get_child("Elements")
        db = await get_db()
        ctrl_alias = info["parent"]
        ctrl = await db.get_device_from_alias(ctrl_alias)
        physical_roles = (await db.get_device_property(ctrl, "physical_roles"))[
            "physical_roles"
        ]
        # print("*** entry", physical_elements, physical_roles)
        # ctrl_path = f"Sardana:Pools:{pool.name}:Controllers:{ctrl_alias}"
        if physical_roles:
            # print("*** ctrl_path", ctrl_path)
            self.query("*").remove()
            for role, element in zip(physical_roles[::2], physical_roles[1::2]):
                motor_entry = await elements.get_element(element)
                print("role", role, motor_entry)
                # controller_info = await pool.get_element_info(controller_alias)
                motor_state = ElementState()
                motor_state.set_entry(motor_entry)
                info = await motor_entry.get_info()
                h = Horizontal(
                    Label(f"{role}: "),
                    motor_state,
                    Label(info["type"]),
                )
                self.mount(h)
            self.display = True
        else:
            self.display = False


# class MotorBanner(ElementBanner):
#     entry_class = SardanaPoolMotor


# class PseudoMotorBanner(ElementBanner):
#     entry_class = SardanaPoolPseudoMotor
