from rich.text import Text
from rich.pretty import Pretty
from textual.containers import VerticalScroll
from textual.widgets import Static

from boogie.widgets.details import Details
from boogie.plugins.tango.widgets.details_list import TangoDetailsList
from ..pools import SardanaPoolControllerLibs, SardanaPoolControllerLib


class SardanaControllerLibsDetailsList(TangoDetailsList[SardanaPoolControllerLibs]):

    COLUMNS = ["Name", "Description"]

    async def get_row(self, entry: SardanaPoolControllerLib):
        # TODO guess get_row will have to be async after all...
        data = await entry.get_data()
        height = str(data["description"]).count("\n") + 1
        return (
            Text(entry.name, style="bold"),
            data["description"] or "",
        ), dict(height=height)


class SardanaControllerLibDetails(Details[SardanaPoolControllerLib]):

    DEFAULT_CSS = """
    SardanaControllerLibDetails {
        border-top: heavy $background-lighten-3;
        border-title-align: center;
        border-title-color: white;
    }
    """

    def compose(self):
        with VerticalScroll():
            yield Static(id="file_path")

    async def _update(self, entry):
        infos = []
        pool = await entry.get_pool()
        data = await entry.get_data()
        for classname in data["elements"]:
            info = await pool.get_controller_class_info(classname)
            infos.extend(info)
        self.query_one("#file_path").update(Pretty(infos))
        self.border_title = entry.name
        # TODO more info, get code etc
