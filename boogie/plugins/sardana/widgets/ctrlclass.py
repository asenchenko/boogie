from textwrap import dedent

from rich.text import Text

from boogie.widgets.details import Details
from boogie.widgets.misc import InfoTable
from boogie.plugins.tango.widgets.details_list import TangoDetailsList
from ..pools import SardanaPoolControllerClasses, SardanaPoolControllerClass


class SardanaControllerClassesDetailsList(
    TangoDetailsList[SardanaPoolControllerClasses]
):

    COLUMNS = ["Name", "Description"]

    async def get_row(self, entry):
        desc = dedent((entry.description or "")).strip()
        height = str(desc).count("\n") + 1
        return (
            Text(entry.name, style="bold"),
            desc,
        ), dict(height=height)


class ControllerClassInfoTable(InfoTable):

    FIELDS = [
        ("Full name", "full_name"),
        ("Main type", "main_type"),
        ("Module", "module"),
        ("Motor roles", "motor_roles"),
        ("Pseudo motor roles", "pseudo_motor_roles"),
        ("Counter roles", "counter_roles"),
        ("Pseudo counter roles", "pseudo_counter_roles"),
        ("File path", "file_path"),
        ("Description", "description"),
        ("Properties", "properties"),
        ("Properties desc.", "properties_desc"),
    ]


class SardanaControllerClassDetails(Details[SardanaPoolControllerClass]):

    DEFAULT_CSS = """
    SardanaControllerClassDetails {
        border-top: heavy $background-lighten-3;
        border-title-align: center;
        border-title-color: white;
        height: 1fr;
    }
    """

    def compose(self):
        yield ControllerClassInfoTable()

    async def _update(self, entry):
        t = self.query_one(InfoTable)
        t.set_config(entry.data)
        self.border_title = entry.name
