from __future__ import annotations

from textual.widgets import Static

from boogie.widgets.entry import EntryWidget
from .element import ElementDetails, ElementState
from ..pools import SardanaPoolMeasurementGroup


class MeasurementGroupDetails(ElementDetails[SardanaPoolMeasurementGroup]):
    def compose(self):
        yield from super().compose()
        yield MeasurementGroupElements()


class MeasurementGroupElements(EntryWidget):

    DEFAULT_CSS = """
    MeasurementGroupElements {
        layout: grid;
        height: auto;
        grid-gutter: 0 1;
        grid-size: 3;
        grid-columns: auto;
        grid-rows: 1;
        display: none;
    }
    """

    def on_mount(self):
        self.border_title = "Elements"
        self.classes = "section"

    async def _update(self, entry):
        data = await entry.get_info()
        physical_elements = data["elements"]
        if physical_elements:
            self.query("*").remove()
            pool = entry.pool
            elements = await pool.get_child("Elements")
            for element in physical_elements or []:
                if element.startswith("tango://"):
                    pass
                else:
                    state = ElementState()
                    entry = await elements.get_element(element)
                    info = await entry.get_info()
                    self.mount(state)
                    self.mount(Static(info["type"]))
                    self.mount(Static(info["parent"]))
                    state.set_entry(entry)
            self.display = True
        else:
            self.display = False
