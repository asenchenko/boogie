from __future__ import annotations

import tango
from textual import on
from textual.containers import Horizontal
from textual.widgets import Static, Button, Label
from textual.css.query import NoMatches

from boogie.widgets.details import EntryWidget
from boogie.plugins.tango.widgets.error import TangoError
from .element import ElementDetails, ElementState
from ..pools import SardanaPoolController


class ControllerDetails(ElementDetails[SardanaPoolController]):

    ACTIONS = [
        ("stop", "Stop", "warning", "Stop ongoing controller operation"),
        ("abort", "Abort", "error", "Abort ongoing controller operation"),
    ]

    def compose(self):
        yield from super().compose()
        yield ControllerElements()
        # yield ControllerMainTypeInfo()
        with Horizontal(classes="actions"):
            for method, title, variant, tooltip in self.ACTIONS:
                b = Button(title, variant=variant, id=method, classes="action")
                b.tooltip = tooltip
                yield b
        yield TangoError()

    @on(Button.Pressed)
    async def button_stop(self, message):
        proxy = await self.entry.get_proxy()
        await proxy.command_inout(message.button.id)


class ControllerElements(EntryWidget):

    DEFAULT_CSS = """
    ControllerElements {
        width: 1fr;
        layout: grid;
        grid-gutter: 0 1;
        grid-size: 2;
        grid-columns: auto 1fr;
        grid-rows: 1;
    }
    """

    def on_mount(self):
        self.border_title = "Elements"
        self.classes = "section"

    async def _update(self, entry):
        pool = entry.pool
        try:
            elements = await pool.get_elements()
        except tango.DevFailed:
            elements = {}
        child_elements = [
            el for el in elements.values() if el["parent"] == self.entry.name
        ]
        if child_elements:
            self.query("*").remove()
            elements = await pool.get_child("Elements")
            for el in child_elements:
                entry = await elements.get_element(el["name"])
                state = ElementState()
                info = await entry.get_info()
                self.mount(Label(f'Axis {info["axis"]}:'))
                self.mount(state)
                state.set_entry(entry)
            self.display = True
        else:
            self.display = False


class ControllerMainTypeInfo(EntryWidget):

    DEFAULT_CSS = """
    ControllerMainTypeInfo #PseudoMotor {
        layout: horizontal;
    }
    """

    def compose(self):
        v = Static(id="PseudoMotor", classes="section")
        v.border_title = "Motor roles"
        yield v

    async def _update(self, entry):
        with self.app.batch_update():
            for w in self.query(".section"):
                w.display = False
            try:
                w = self.query_one(f"#{entry.main_type}")
                w.query("*").remove()
                pool = entry.pool
                proxy = await entry.get_proxy()
                physical_roles = (await proxy.get_property("physical_roles"))[
                    "physical_roles"
                ]
                for role, name in zip(physical_roles[::2], physical_roles[1::2]):
                    elements = await pool.get_child("Elements")
                    entry = await elements.get_element(name)
                    state = ElementState()
                    await w.mount_all([Label(f"{role}: "), state])
                    state.set_entry(entry)

                w.display = True
            except NoMatches:
                pass
