"""
Entries for Sardana MacroServers
"""

from __future__ import annotations
from dataclasses import dataclass
import json
import pickle
from typing import TYPE_CHECKING

import tango
from tango.asyncio import DeviceProxy

from boogie.plugins.tango.entry import TangoEntry
from boogie.plugins.tango.utils import get_device_proxy, get_db
from boogie.plugins.tango.device import TangoDevice
from .pools import SardanaPoolElement

if TYPE_CHECKING:
    from .entry import SardanaRoot


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServers(TangoEntry):

    parent: SardanaRoot
    name: str = "MacroServers"

    async def get_children(self) -> list[SardanaMacroServer]:
        db = await get_db()
        query = (
            "SELECT name, alias FROM device WHERE class = 'MacroServer' ORDER BY name"
        )
        _, macroservers = await db.command_inout("DbMySQLSelect", query)
        return [
            await self.get_child(ms, alias)
            for ms, alias in zip(macroservers[::2], macroservers[1::2])
        ]

    async def get_child(
        self, name: str, alias: str | None = None
    ) -> SardanaMacroServer:
        if "/" not in name:
            db = await get_db()
            alias = name
            name = await db.get_device_from_alias(alias)
        return SardanaMacroServer(self, name, alias=alias)

    async def get_filtered_nodes(self):
        return {}


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServer(TangoDevice):

    parent: SardanaMacroServers
    name: str
    alias: str | None = None

    async def get_children(self):
        return [
            SardanaMacroServerDoors(self),
            SardanaMacroServerEnvironment(self),
            SardanaMacroServerScanHistory(self),
            SardanaMacroServerGeneralHooks(self),
        ]

    def __str__(self):
        return self.alias or self.name

    async def get_child(self, name):
        lower_name = name.lower()
        if lower_name == "doors":
            return SardanaMacroServerDoors(self)
        if lower_name == "environment":
            return SardanaMacroServerEnvironment(self)
        if lower_name == "scan history":
            return SardanaMacroServerScanHistory(self)
        if lower_name == "general hooks":
            return SardanaMacroServerGeneralHooks(self)

    async def get_info(self) -> tango.DeviceInfo:
        db = await get_db()
        return await db.get_device_info(self.device)

    async def get_element(self, name) -> SardanaPoolElement:
        proxy = await self.get_proxy()
        elements = await proxy.read_attribute("Elements")
        for element in json.loads(elements.value[1])["new"]:
            if element["name"].lower() == name.lower():
                break
        else:
            raise KeyError(f"No such element {name}")
        poolname = element["pool"]
        pool = await self.parent.parent.get_pool(poolname)
        elements = await pool.get_child("elements")
        return await elements.get_element(name)

    @property
    def device(self) -> str:
        return self.name

    async def get_proxy(self) -> DeviceProxy:
        return await get_device_proxy(self.device)


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerDoors(TangoEntry):

    parent: SardanaMacroServer
    name: str = "Doors"

    async def get_children(self) -> list[SardanaMacroServerDoor]:
        db = await get_db()
        info = await self.parent.get_info()
        server_name = info.ds_full_name
        query = f"""
        SELECT name, alias FROM device
        WHERE class = 'Door'
        AND server = '{server_name}'
        ORDER BY name
        """
        _, doors = await db.command_inout("DbMySQLSelect", query)
        return [
            SardanaMacroServerDoor(self, door, alias)
            for door, alias in zip(doors[::2], doors[1::2])
        ]

    async def get_child(self, name, alias=None) -> SardanaMacroServerDoor:
        if "/" not in name:
            db = await get_db()
            alias = name
            name = await db.get_device_from_alias(alias)
        return SardanaMacroServerDoor(self, name, alias)


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerDoor(TangoDevice):

    parent: SardanaMacroServerDoors
    name: str
    alias: str | None = None

    def __str__(self) -> str:
        return self.alias or self.name

    async def get_element(self, alias) -> SardanaPoolElement:
        ms = self.parent.parent
        return await ms.get_element(alias)

    async def stop_macro(self) -> None:
        proxy = await self.get_proxy()
        await proxy.command_inout("StopMacro")

    async def pause_macro(self) -> None:
        proxy = await self.get_proxy()
        await proxy.command_inout("PauseMacro")

    async def resume_macro(self) -> None:
        proxy = await self.get_proxy()
        await proxy.command_inout("ResumeMacro")


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerEnvironment(TangoEntry):

    parent: SardanaMacroServer
    name: str = "Environment"
    leaf_node = True

    async def get_env(self):
        proxy = await self.parent.get_proxy()
        result = await proxy.read_attribute("Environment")
        encoding, value = result.value
        if encoding == "pickle":
            return pickle.loads(value)["new"]
        else:
            raise NotImplementedError(f"Unknown encoding '{encoding}'")

    async def get_children(self) -> list[SardanaMacroServerEnvironmentVariable]:
        env = await self.get_env()
        return [
            SardanaMacroServerEnvironmentVariable(self, key, json.dumps(value))
            for key, value in env.items()
        ]

    async def get_child(self, name) -> SardanaMacroServerEnvironmentVariable:
        env = await self.get_env()
        if name in env:
            value = env[name]
            return SardanaMacroServerEnvironmentVariable(self, name, json.dumps(value))
        raise KeyError()


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerEnvironmentVariable(TangoEntry):

    value: str = ""

    # def __hash__(self):
    #     # TODO why is this needed, it's in the base class...
    #     return hash(":".join(p.name for p in self.path))


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerScanHistory(TangoEntry):

    parent: SardanaMacroServer
    name: str = "Scan history"
    leaf_node = True

    async def get_history(self) -> list:
        proxy = await self.parent.get_proxy()
        result = await proxy.read_attribute("Environment")
        encoding, value = result.value
        if encoding == "pickle":
            env = pickle.loads(value)["new"]
            scan_history = env.get("ScanHistory", [])
            return scan_history
        return []

    async def get_children(self) -> list[SardanaMacroServerScanHistoryItem]:
        history = await self.get_history()
        return list(
            reversed(
                [
                    SardanaMacroServerScanHistoryItem(
                        self, str(item["serialno"]), data=json.dumps(item)
                    )
                    for item in history
                ]
            )
        )

    async def get_child(self, name) -> SardanaMacroServerScanHistoryItem:
        serialno = int(name)
        history = await self.get_history()
        for item in history:
            if item["serialno"] == serialno:
                return SardanaMacroServerScanHistoryItem(
                    self, name, data=json.dumps(item)
                )
        raise KeyError()


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerScanHistoryItem(TangoEntry):

    parent: SardanaMacroServerScanHistory
    name: str
    data: str = "{}"

    # def __hash__(self):
    #     # TODO why is this needed, it's in the base class...
    #     return hash(":".join(p.name for p in self.path))


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerGeneralHooks(TangoEntry):

    parent: SardanaMacroServer
    name: str = "General hooks"
    leaf_node = True

    async def get_children(self) -> list[SardanaMacroServerGeneralHookPlace]:
        proxy = await self.parent.get_proxy()
        result = await proxy.read_attribute("Environment")
        encoding, value = result.value
        if encoding == "pickle":
            env = pickle.loads(value)["new"]
            general_hooks = env.get("_GeneralHooks", [])
            hooks_by_place: dict[str, set] = {}
            # TODO what about order?
            for hook, places in general_hooks:
                for place in places:
                    hooks_by_place.setdefault(place, set()).add(hook)
            return [
                SardanaMacroServerGeneralHookPlace(self, place, tuple(sorted(hooks)))
                for place, hooks in hooks_by_place.items()
            ]
        return []


@dataclass(frozen=True, repr=False, eq=False)
class SardanaMacroServerGeneralHookPlace(TangoEntry):
    hooks: tuple[str, ...] = ()
