from .widgets import *  # noqa

from boogie.plugins.tango import get_title
from .entry import SardanaRoot
from .search import search_sardana_elements


INDEX = 2

BROWSERS: list[dict] = [
    {
        "root": SardanaRoot,
        "search": search_sardana_elements,
        "get_title": get_title,
    }
]
