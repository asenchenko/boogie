from .utils import get_db
from .servers import TangoServers, TangoServerInstance
from .devices import TangoDevices
from .device import TangoDevice
from .aliases import TangoDeviceAliases


async def get_server_instance_entry(servername: str):
    s = TangoServers()
    srv, inst = servername.split("/")
    server = await s.get_child(srv)
    return TangoServerInstance(server, inst)


async def get_server_device_entry(devicename: str):
    db = await get_db()
    info = await db.get_device_info(devicename)
    srv, inst = info.ds_full_name.split("/")
    s = TangoServers()
    server = await s.get_child(srv)
    instance = await server.get_child(inst)
    clss = await instance.get_child(info.class_name)
    return TangoDevice(clss, devicename)


async def get_device_entry(devicename: str):
    ds = TangoDevices()
    domain, family, member = devicename.split("/")
    dom = await ds.get_child(domain)
    fam = await dom.get_child(family)
    mem = await fam.get_child(member)
    return mem


async def get_device_alias_entry(alias: str):
    ds = TangoDeviceAliases()
    return await ds.get_child(alias)


async def get_attribute_entry(attributename: str):
    devicename, attrname = attributename.rsplit("/", 1)
    device = await get_device_entry(devicename)
    attrs = await device.get_child("attributes")
    return await attrs.get_child(attrname)
