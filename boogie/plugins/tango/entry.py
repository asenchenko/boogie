"""
This is an abstraction over a Tango control system, to turn it into a
convenient tree-like structure. It defines a hierarchy of "entries"
that can be traversed easily. Also each entry has various helper methods
and properties that wrap up some of the thornier parts of the Tango API,
and handles things like caching. Also they can provide a Textual widget
for representation in the UI.

The entry API is a bit fluid at the moment.
"""

from __future__ import annotations
from dataclasses import dataclass

from boogie.entry import Entry


@dataclass(frozen=True, repr=False, eq=False)
class TangoEntry(Entry):
    parent: TangoEntry | None
    name: str
