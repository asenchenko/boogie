from textwrap import dedent

from rich.text import Text

from .details_list import TangoDetailsList
from ..device import TangoDeviceCommands


def strip_multiline(s):
    return dedent("\n".join(l.rstrip() for l in str(s).splitlines()))


class TangoDeviceCommandListDetails(TangoDetailsList[TangoDeviceCommands]):

    COLUMNS = ["Command", "In desc", "Out desc"]

    async def get_row(self, entry):
        info = entry.info
        height = max(
            str(info.in_type_desc).count("\n"), str(info.out_type_desc).count("\n"), 1
        )
        return (
            Text(info.cmd_name, style="bold"),
            (
                strip_multiline(info.in_type_desc)
                if info.in_type_desc != "Uninitialised"
                else ""
            ),
            (
                strip_multiline(info.out_type_desc)
                if info.out_type_desc != "Uninitialised"
                else ""
            ),
        ), dict(height=height)
