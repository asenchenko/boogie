from typing import Generic

from boogie.widgets.details import DetailsList
from boogie.entry import EntryType
from .error import TangoError


class TangoDetailsList(DetailsList, Generic[EntryType]):
    error_widget = TangoError
