from __future__ import annotations
from datetime import datetime, timedelta
import re
from typing import Callable

from rich.table import Table
from rich.pretty import Pretty
from rich.columns import Columns
from rich.text import Text
from rich.rule import Rule
from rich.console import RenderableType
import tango
from textual import work, on
from textual.app import ComposeResult
from textual.binding import Binding
from textual.screen import ModalScreen
from textual.containers import Horizontal, Vertical
from textual.events import Key
from textual.widgets import (
    Static,
    Label,
    Input,
    Button,
    RichLog,
    TabbedContent,
    TabPane,
)
from textual.validation import Function
from textual.reactive import reactive

from boogie.messages import NavigateTo
from boogie.widgets.entry import EntryWidget
from .details import TangoDetails, TangoEntryWidget
from ..device import TangoDeviceAttribute
from .error import TangoError


class TangoDeviceAttributeDetails(TangoDetails[TangoDeviceAttribute]):

    DEFAULT_CSS = """
    TangoDeviceAttributeDetails {
        height: 1fr;
        TabPane {
            height: 1fr;
        }
    }
    """

    BINDINGS = [("m", "monitor", "Monitor")]

    def compose(self) -> ComposeResult:
        with TabbedContent(id="tabs"):
            # TODO make these "lazy"
            with TabPane("[u]V[/]alue", id="value"):
                yield TangoAttributeValue()
            with TabPane("[u]I[/]nfo", id="info"):
                yield TangoAttributeInfo()
            with TabPane("[u]C[/]onfig", id="config"):
                yield TangoAttributeConfig()
            with TabPane("[u]A[/]larms", id="alarms"):
                yield TangoAttributeAlarms()
            with TabPane("[u]E[/]vents", id="events"):
                yield TangoAttributeEvents()
        yield TangoError()

    def on_mount(self):
        self.query_one("Tabs").can_focus = False
        self.classes = "header-border"

    async def _update(self, entry):
        with self.app.batch_update():
            self.border_title = str(entry)

            self.query_one(TangoAttributeValue).set_entry(entry)
            self.query_one(TangoAttributeInfo).set_entry(entry)
            self.query_one(TangoAttributeConfig).set_entry(entry)
            self.query_one(TangoAttributeAlarms).set_entry(entry)
            self.query_one(TangoAttributeEvents).set_entry(entry)
            if len(self.subpath) > 1:
                tab_id = self.subpath[1].name
                tabbed = self.query_one(TabbedContent)
                tabbed.active = tab_id

    @on(TabbedContent.TabActivated)
    async def tab_changed(self, message):
        message.stop()  # Don't leak these up
        entry = self.subpath[0]
        tab_id = message.pane.id
        child = await entry.get_child(tab_id)
        self.post_message(NavigateTo(child))
        message.pane.children[0].focus()

    async def action_monitor(self):
        # self.post_message(AddToMonitor(self.entry))
        await self.app.add_to_monitor(self.entry)

    @on(Button.Pressed)
    async def on_edit(self, message):
        pass

    async def on_key(self, event: Key) -> None:
        tabs = self.query(TabPane)
        for tab in tabs:
            assert tab.id is not None
            if tab.id.startswith(event.name):
                tabbed: TabbedContent = self.query_one(
                    "#tabs", expect_type=TabbedContent
                )
                tabbed.active = tab.id


WRITABLE = {
    tango.AttrWriteType.WRITE,
    tango.AttrWriteType.READ_WRITE,
    tango.AttrWriteType.READ_WITH_WRITE,
}


class PersistentLog(EntryWidget[TangoDeviceAttribute]):
    """Log widget that keeps track of logs for its entries"""

    _log_cache: dict[TangoDeviceAttribute, list[RenderableType]] = {}

    def compose(self):
        yield RichLog(markup=True)

    async def _update(self, entry):
        old_logs = self._log_cache.get(entry, [])
        log = self.query_one(RichLog)
        with self.app.batch_update():
            log.clear()
            for line in old_logs:
                log.write(line)

    def write(self, message: RenderableType):
        self.query_one(RichLog).write(message)
        assert self.entry
        self._log_cache.setdefault(self.entry, []).append(message)

    def clear(self):
        self.query_one(RichLog).clear()
        assert self.entry
        self._log_cache.pop(self.entry, None)


class TangoAttributeValue(EntryWidget):

    DEFAULT_CSS = """
    TangoAttributeValue {
        height: 1fr;
        background: $primary-darken-1;
        layout: grid;
        grid-rows: auto 1fr auto;
        VerticalScroll {
            height: 1fr;
        }
        Vertical {
            display: none;
            height: auto;
            dock: bottom;
            background: $background;
        }
        #invalid {
            height: 1;
            color: $error;
            display: none;
        }
        Horizontal {
            height: auto;
        }
        Input {
            border: solid $primary;
            padding: 0;
            width: 1fr;
            &:focus {
                border: solid white;
            }
        }
        Input.-invalid {
            border: solid red;
        }
        Input.-invalid:focus {
            border: solid red;
        }
        RichLog {
            background: black;
            height: 1fr;
        }
    }
    """

    last_read_value: reactive[str | None] = reactive(None)

    BINDINGS = [Binding("c", "copy", "Copy", show=False)]

    def compose(self):
        with Horizontal():
            yield Button("Read", id="read", variant="primary")
            yield Button("Clear", id="clear", variant="default")
        yield PersistentLog()
        with Vertical(id="write-value"):
            yield Label(id="invalid")
            with Horizontal():
                yield Input(
                    validators=[Function(self.is_valid_value)],
                    placeholder="Value to write",
                )
                yield Button("Write", id="write", disabled=True, variant="primary")

    def focus(self, **kwargs):
        w = self.query_one(PersistentLog)
        w.focus()
        super().focus(**kwargs)

    async def is_valid_value(self, value: str) -> bool:
        if isinstance(self.entry, TangoDeviceAttribute):
            button = self.query_one("#write")
            invalid = self.query_one("#invalid", expect_type=Static)
            if value == "":
                button.disabled = True
                invalid.display = False
                return True
            try:
                await self.entry.parse_string(value)
            except ValueError as e:
                button.disabled = True
                invalid.update(str(e))
                invalid.display = True
                return False
            else:
                button.disabled = False
                invalid.display = False
                return True
        return False

    async def _update(self, entry: TangoDeviceAttribute):
        self.query_one(PersistentLog).set_entry(entry)
        container = self.query_one("#write-value")
        info = entry.info
        if info.writable in WRITABLE:
            if info.data_format == tango.AttrDataFormat.SCALAR:
                value_input = self.query_one(Input)
                value_input.placeholder = f"Value to write to '{entry.name}'"
                container.display = True
                container.can_focus_children = True
            else:
                # TODO support writing arrays and images
                pass
        else:
            container.display = False
            container.can_focus_children = False

    def _log_error(self, exc: Exception, duration: timedelta) -> None:
        log = self.query_one(PersistentLog)
        dt = duration.total_seconds()
        log.write(f"[dim red]--- {datetime.now().isoformat()} (took {dt} s) ---")
        if isinstance(exc, tango.DevFailed):
            log.write(f"[red]DevFailed: {exc.args[0].desc}")
        else:
            log.write(f"[red]Unexpected error: {exc}")

    def _render_quality(self, quality):
        match quality:
            case tango.AttrQuality.ATTR_VALID:
                return Text("VALID", style="black on rgb(0,255,0)", justify="right")
            case tango.AttrQuality.ATTR_VALID:
                return Text("INVALID", style="white on rgb(255,0,0)", justify="right")
            case tango.AttrQuality.ATTR_CHANGING:
                return Text(
                    "CHANGING", style="black on rgb(255,255,0)", justify="right"
                )
            case tango.AttrQuality.ATTR_ALARM:
                return Text("ALARM", style="black on rgb(255,127,0)", justify="right")
            case _:
                return str(quality)

    def _log(self, data: tango.DeviceAttribute, duration: timedelta) -> None:
        log = self.query_one(PersistentLog)
        dt = duration.total_seconds()
        # log.write(f"[dim white]--- Read {data.time.isoformat()} (took {dt} s) ---")
        log.write(
            f"[#777]--- Read {data.time.isoformat()} ({dt} s) ---"
        )
        if data.has_failed:
            log.write(Pretty(data.get_err_stack()))
        elif data.is_empty:
            log.write(
                Columns(
                    ["[gray]<Empty>[/]", self._render_quality(data.quality)],
                    expand=False,
                )
            )
        else:
            log.write(
                Columns(
                    [Pretty(data.value), self._render_quality(data.quality)],
                    expand=False,
                )
            )

    @on(Button.Pressed, "#read")
    async def on_read(self):
        try:
            t0 = datetime.now()
            data = await self.entry.read()
            self._log(data, datetime.now() - t0)
            self.last_read_value = str(data.value)
        except tango.DevFailed as e:
            self._log_error(e, datetime.now() - t0)

    @on(Button.Pressed, "#clear")
    async def on_clear(self):
        self.query_one(PersistentLog).clear()

    @on(Button.Pressed, "#write")
    def on_write(self):
        self._write()

    def on_input_submitted(self):
        # TODO probably not really a good idea but, for now...
        self._write()

    @work
    async def _write(self):
        # TODO improve log output
        value_input = self.query_one(Input)
        try:
            value = await self.entry.parse_string(value_input.value)
        except ValueError as e:
            # Shouldn't happen but let's make sure
            print("Error parsing value", e)
            return
        t0 = datetime.now()
        log = self.query_one(PersistentLog)
        try:
            print("write", value)
            await self.app.run_entry_method(self.entry, self.entry.write, value=value)
            # TODO write_read?
        except tango.DevFailed as e:
            print("error", e)
            log.write("[yellow]Failed![/]")
            log.write(Pretty(e))
        except Exception as e:
            print("error", e)
            log.write("[red]Error![/]")
            log.write(Pretty(e))
        duration = datetime.now() - t0
        dt = duration.total_seconds()
        log.write(
            Columns([Rule(f"[yellow]Write {t0.isoformat()} ({dt} s)")], expand=True)
        )

        # log.write(f"[dim blue]--- Write {t0.isoformat()} (took {dt} s) ---")
        log.write(str(value))

    def action_copy(self):
        if self.last_read_value is not None:
            self.app.copy_to_clipboard(
                self.last_read_value, "Copied last read value to clipboard!"
            )


class TangoAttributePolling(EntryWidget, can_focus=False):
    DEFAULT_CSS = """
    TangoAttributePolling {
        height: auto;
    }
    """

    def compose(self):
        yield Static()

    async def _update(self, entry: TangoDeviceAttribute):
        status = await entry.polling_status()
        if status:
            table = Table.grid(expand=True, padding=(0, 1))
            table.add_column(justify="right", max_width=20, style="bold")
            table.add_column()

            name = status[0].split("=")[1].strip()
            attrs: list[str] = []
            last_update_time = None
            for line in status[1:]:
                if line.startswith("Data not updated since"):
                    continue
                if line.startswith("Time needed for the last attributes"):
                    m = re.match(r"[^(]+\(([^)]+)\).*", line)
                    if m:
                        m.group(1).split("+")
                        last_update_time = float(line.rsplit("=", 1)[1])
                    continue
                try:
                    title, value = line.split("=")
                    # TODO better parsing
                    table.add_row(title.strip(), value.strip())
                except ValueError:
                    table.add_row("?", line)
            # if attrs:
            #     table.add_row("Attributes", ", ".join(a.strip() for a in attrs))
            if last_update_time:
                table.add_row("Last update took (ms)", Pretty(last_update_time))
            self.query_one(Static).update(table)


class AttributeConfigFormEdit(Static, can_focus=True):

    DEFAULT_CSS = """
    AttributeConfigFormEdit {
        layout: grid;
        grid-size: 2;
        grid-gutter: 0 1;
        grid-columns: auto 1fr;
        height: auto;
        Label {
            text-align: right;
            padding: 1;
            color: $text-muted;
        }
        Input {
            border: solid gray;
            padding: 0;
        }
        Input.changed {
            border: solid orange;
            color: $secondary;
        }
        Input:focus {
            border: solid white;
            background: $background-lighten-2;
        }
        Input.-invalid {
            padding: 0;
            border: solid red;
            background: $error-darken-2;
        }
        Input.-invalid:focus {
            padding: 0;
            background: $error;
        }
    }
    """

    def __init__(self, fields, config, *args, **kwargs):
        self._fields = fields
        self._config = config
        self._paths = {}
        super().__init__(*args, **kwargs)

    INPUT_TYPES = {
        int: "integer",
        float: "number",
    }

    def compose(self):
        width = 0
        for title, path, typ in self._fields:
            yield Label(f"{title}:")
            steps = path.split(".")
            value = self._config
            for step in steps:
                value = getattr(value, step)
            name = steps[-1]
            self._paths[name] = steps
            input_type = self.INPUT_TYPES.get(typ, "text")
            if value == CONFIG_PLACEHOLDERS.get(name):
                value = ""
            inp = Input(id=name, type=input_type, value=value, valid_empty=True)
            yield inp
            width = max(width, len(title) + 1)

    def on_mount(self):
        self.focus()

    def focus(self, *args):
        self.query(Input).first().focus()

    def on_input_changed(self, message):
        path = self._paths[message.control.id]
        obj = self._config
        for step in path[:-1]:
            obj = getattr(obj, step)
        str_value = str(message.control.value)
        if getattr(obj, path[-1]) != str_value:
            setattr(obj, path[-1], str_value)
            message.control.add_class("changed")


NOT_SPECIFIED = "Not specified"

CONFIG_PLACEHOLDERS = {
    "format": NOT_SPECIFIED,
    "standard_unit": "No standard unit",
    "display_unit": "No display unit",
    "description": "No description",
    "min_alarm": NOT_SPECIFIED,
    "max_alarm": NOT_SPECIFIED,
    "min_warning": NOT_SPECIFIED,
    "max_warning": NOT_SPECIFIED,
    "abs_change": NOT_SPECIFIED,
    "rel_change": NOT_SPECIFIED,
    "period": NOT_SPECIFIED,
    "archive_rel_change": NOT_SPECIFIED,
    "archive_abs_change": NOT_SPECIFIED,
    "archive_period": NOT_SPECIFIED,
}


class ConfigEditModal(ModalScreen[tango.AttributeConfig]):

    DEFAULT_CSS = """
    ConfigEditModal {
        align: center middle;
        height: auto;
        .frame {
            width: 75%;
            height: auto;
            border: panel $secondary;
            padding: 0 1;
        }
        .body {
            height: auto;
        }
        Horizontal {
            height: auto;
        }
        Button {
             width: 1fr;
        }
        AttributeConfigFormEdit {
            height: auto;
        }
    }
    """

    BINDINGS = [
        ("ctrl+s", "save", "Save"),
        ("escape", "cancel", "Cancel"),
    ]

    def __init__(self, title, fields, config, *args, **kwargs):
        self._title = title
        self._fields = fields
        self._config = config
        super().__init__(*args, **kwargs)

    def compose(self):
        with Vertical(classes="frame") as v:
            v.border_title = f"[b]{self._title}[/] attribute configuration"
            with Vertical(classes="body") as v:
                yield AttributeConfigFormEdit(self._fields, self._config)
                with Horizontal():
                    yield Button("Save", id="save", variant="primary")
                    yield Button("Cancel", id="cancel", variant="warning")

    @on(Button.Pressed, "#save")
    def save(self):
        self.dismiss(self._config)

    @on(Button.Pressed, "#cancel")
    def cancel(self):
        self.dismiss(None)

    # def on_key(self, event: Key) -> None:
    #     if event.key == "escape":
    #         event.prevent_default()
    #         self.dismiss(False)

    def action_save(self):
        self.dismiss(self._config)

    def action_cancel(self):
        self.dismiss(None)


class ConfigTable(EntryWidget):

    DEFAULT_CSS = """
    ConfigTable {
        height: 1fr;
        width: 1fr;
        layout: grid;
        grid-size: 2;
        grid-columns: 1fr 2fr;
        grid-gutter: 0 1;
        grid-rows: auto;
        padding: 1 1;
        background: $primary-darken-1;
        Label {
            text-align: right;
            color: $text-muted;
        }
    }
    """

    FIELDS: list[tuple[str, str, Callable]]

    def compose(self):
        for title, name, _ in self.FIELDS:
            yield Label(f"{title}:", expand=True)
            yield Static(id=name.split(".")[-1])
        self.can_focus = True

    async def _update(self, entry: TangoDeviceAttribute):
        info = await entry.get_config()
        with self.app.batch_update():
            for _, name, convert in self.FIELDS:
                value = info
                for key in name.split("."):
                    value = getattr(value, key)
                if value == CONFIG_PLACEHOLDERS.get(key):
                    value = ""
                if convert:
                    value = convert(value)
                widget = self.query_one(f"#{key}", expect_type=Static)
                widget.update(str(value))


class EditableConfigTable(ConfigTable):

    DEFAULT_CSS = """
    EditableConfigTable {
    }
    """

    BINDINGS = [
        ("ctrl+e", "edit", "Edit"),
    ]

    def compose(self):
        yield from super().compose()
        yield Button("Edit", id="edit-config")

    async def action_edit(self):
        await self.edit()

    async def on_button_pressed(self):
        await self.edit()

    async def edit(self):
        config = await self.entry.get_config()

        async def maybe_save(config):
            if config:
                try:
                    await self.app.run_entry_method(
                        self.entry, self.entry.set_config, config=config
                    )
                    self.notify("Config changes saved")
                    self.reload()
                except tango.DevFailed as e:
                    self.notify(f"Failed to set config: {e}", severity="warning")
            else:
                self.notify("Cancelled edit")

        self.app.push_screen(
            ConfigEditModal(self.entry.name, self.FIELDS, config), maybe_save
        )


class TangoAttributeInfo(ConfigTable):
    """Shows non configurable info about an attribute"""

    FIELDS: list[tuple[str, str, Callable]] = [
        ("Name", "name", str),
        ("Data format", "data_format", str),
        ("Data type", "data_type", lambda v: str(tango.CmdArgType.values[v])),
        ("Writable", "writable", str),
        ("Display level", "disp_level", str),
        ("Enum labels", "enum_labels", lambda ls: ", ".join(ls) if ls else ""),
    ]


class TangoAttributeConfig(EditableConfigTable):
    """Shows basic configuration about an attribute"""

    FIELDS = [
        ("Label", "label", str),
        ("Description", "description", str),
        ("Unit", "unit", str),
        ("Standard unit", "standard_unit", str),
        ("Display unit", "display_unit", str),
        ("Format", "format", str),
    ]


class TangoAttributeAlarms(EditableConfigTable):

    FIELDS = [
        ("Min alarm", "alarms.min_alarm", str),
        ("Max alarm", "alarms.max_alarm", str),
        ("Min warning", "alarms.min_warning", str),
        ("Max warning", "alarms.max_warning", str),
    ]


class TangoAttributeEvents(EditableConfigTable):

    FIELDS = [
        ("Absolute change", "events.ch_event.abs_change", str),
        ("Relative change", "events.ch_event.rel_change", str),
        ("Period", "events.per_event.period", str),
        ("Archive abs. change", "events.arch_event.archive_abs_change", str),
        ("Archive rel. change", "events.arch_event.archive_rel_change", str),
        ("Archive period", "events.arch_event.archive_period", str),
    ]


# if __name__ == "__main__":

#     """
#     Test app for widget.
#     $ textual run -c python -m boogie.plugins.tango.widgets.attribute sys/tg_test/1 double_scalar
#     """

#     import asyncio
#     import sys

#     from textual.app import App
#     from textual.widgets import Footer

#     from boogie.plugins.tango.entry import get_server_device_entry

#     class AttributeApp(App):

#         CSS_PATH = ["../../../app2.css"]

#         def compose(self):
#             yield TangoDeviceAttributeDetails()
#             yield Footer()

#         def on_ready(self):
#             self.run_worker(self._start)

#         async def _start(self):
#             await asyncio.sleep(1)
#             device, attr = sys.argv[1:]
#             dev_entry = await get_server_device_entry(device)
#             attributes_entry = await dev_entry.get_child("attributes")
#             entry = await attributes_entry.get_child(attr)
#             self.query_one(TangoDeviceAttributeDetails).set_entry(entry)

#     AttributeApp().run()


class TangoAttributeValueNumeric(TangoEntryWidget):

    DEFAULT_CSS = """
    TangoAttributeValueNumeric {
        height: auto;
    }
    """

    QUALITY = {
        tango.AttrQuality.ATTR_VALID: "[#00ff00]■[/]",
        tango.AttrQuality.ATTR_INVALID: "[red]■[/]",
        tango.AttrQuality.ATTR_CHANGING: "[#ffff00]■[/]",
        tango.AttrQuality.ATTR_ALARM: "[#ff8800]■[/]",
    }

    def compose(self):
        yield Static()

    def get_quality(self, quality: tango.AttrQuality):
        return self.QUALITY[quality]

    async def _update(self, entry: TangoDeviceAttribute):
        w = self.query_one(Static)
        config = await entry.get_config()
        fmt = config.format or "%.3f"
        unit = config.unit or ""
        async for attr, data in await entry.listen():
            if isinstance(data, tango.DevFailed):
                w.update("[red]<error>")
            else:
                w.update(f"{fmt % data.value} {unit} {self.QUALITY[data.quality]}")
