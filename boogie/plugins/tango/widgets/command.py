from datetime import datetime

from rich.text import Text
from rich.pretty import Pretty
import tango
from textual import on, work
from textual.containers import Vertical, Horizontal, VerticalScroll
from textual.validation import Function
from textual.widgets import (
    Static,
    Input,
    RichLog,
    TabbedContent,
    TabPane,
    TextArea,
    Button,
)

from ..device import TangoDeviceCommand
from .details import TangoDetails
from .error import TangoError


class TangoDeviceCommandDetails(TangoDetails[TangoDeviceCommand], can_focus=False):
    # TODO we can be more helpful with arguments, some don't make much sense
    # as string inputs, e.g boolean, state...

    DEFAULT_CSS = """
    TangoDeviceCommandDetails {
        #run TabPane {
            height: 1fr;
            layout: grid;
            grid-size: 1 3;
        }
        #info {
            height: auto;
            width: 100%;
            layout: grid;
            grid-size: 2;
            grid-columns: 1fr 6;
            background: $primary-darken-1;
        }
        #arguments {
            border: solid $primary-lighten-2;
            display: none;
            height: auto;
        }
        #arguments:focus-within {
            border: solid $foreground;
        }
        #arguments Input {
            border: none;
            padding: 0;
            height: 1;
        }
        #arguments Input:focus {
            border: none;
            padding: 0;
        }
        #arguments TextArea {
            height: 3;
        }
        RichLog {
            background: $background;
            height: 1fr;
            &:focus {
                background: $background-lighten-1;
                height: 1fr;
            }
        }
        Input.-invalid {
            background: $error-darken-3 60%;
        }
        Input.-invalid:focus {
            background: $error-darken-3;
        }
    }
    """

    BINDINGS = [
        ("ctrl+r", "run", "Run"),  # TODO does not work!
    ]

    def compose(self):
        # TODO convert to using LazyEntryTabs
        with TabbedContent(name="Something"):
            with TabPane("Run", id="run") as pane:
                pane.can_focus = False
                with Horizontal(id="info"):
                    yield Vertical(id="arguments")
                    run_button = Button("Run", id="run", variant="warning")
                    run_button.can_focus = False
                    yield run_button
                yield RichLog()
            with TabPane("Polling", id="polling"):
                pass
        yield TangoError()

    def on_mount(self):
        self.query_one("Tabs").can_focus = False
        self.classes = "header-border"

    def focus(self, **kwargs):
        w = self.query_one("#arguments > *")
        w.focus()

    @on(TabbedContent.TabActivated)
    async def on_tab_activated(self, message):
        message.stop()  # Don't leak these up
        # TODO proper tab handling, when there are more than one...

    async def _update(self, entry: TangoDeviceCommand):
        self.border_title = str(entry)
        with self.app.batch_update():
            # self.query_one("#types", expect_type=Static).update(
            #     f"{entry.info.in_type} -> {entry.info.out_type}"
            # )
            if entry.takes_arguments:
                args = self.query_one("#arguments", expect_type=Vertical)
                args.display = True
                args.query("*").remove()
                if entry.info.in_type.name.endswith("Array"):
                    # Multi-line editor, one line per argument
                    widget1 = TextArea(show_line_numbers=True, soft_wrap=False)
                    args.mount(widget1)
                    args.border_title = f"Arguments ({entry.info.in_type})"
                else:
                    # Single argument editor
                    widget2 = Input(validators=[Function(self._is_valid_argument)])
                    args.mount(widget2)
                    args.border_title = f"Argument ({entry.info.in_type})"
            else:
                args2 = self.query_one("#arguments")
                args2.display = False
                args2.query("*").remove()
            log = self.query_one(RichLog)
            log.border_title = "Results"
            log.clear()  # Keep log per device/command?

    def _is_valid_argument(self, value: str) -> bool:
        if self.entry:
            try:
                self.entry.validate_argument(value)
            except ValueError:
                return False
            else:
                return True
        return False

    def _get_arguments(self):
        if self.entry.takes_arguments:
            widget = self.query_one("#arguments > *")
            if isinstance(widget, Input):
                value = widget.value
            else:
                value = widget.text.splitlines()
            return self.entry.convert_arguments(value)
            # except Exception as e:
            #     print("Error converting arguments", e)
            #     self.error = e

    def action_run(self):
        self.run()

    @on(Button.Pressed, "#run")
    def run_button(self):
        self.run()

    @work
    async def run(self):
        log = self.query_one(RichLog)
        start = datetime.now()
        try:
            arguments = self._get_arguments()
        except Exception as e:
            # Problem parsing arguments
            # TODO parse args "on the fly" for more immediate feedback
            # TODO argument "preview" to show how we parse the input?
            # self.error = e
            log.write(Text("Error parsing arguments:", style="red"))
            log.write(Pretty(e))
            return
        try:
            result = await self.app.run_entry_method(
                self.entry, self.entry.run, args=arguments
            )
            duration = (datetime.now() - start).total_seconds()

            log.write(
                Text(f"--- {start.isoformat()} ({duration:.3f} s) ---", style="blue")
            )
            if self.entry.takes_arguments:
                log.write(f"In:  {arguments}")
            if self.entry.returns_values:
                log.write(f"Out: {result}")
        except tango.DevFailed as e:
            log.write(Text(f"--- {start.isoformat()} ---", style="red"))
            log.write(Pretty(e))


class CommandArgumentMultiline(Static):
    """
    Widget for commands that take an array argument
    Poor man's implementation of a multiline editor.
    """

    # TODO Replace it as soon as there is a proper one in Textual!

    DEFAULT_CSS = """
    CommandArgumentMultiline {
        width: 100%;
        height: 5;
        layout: horizontal;
    }
    CommandArgumentMultiline VerticalScroll {
        width: 1fr;
    }
    CommandArgumentMultiline Vertical {
        width: auto;
    }
    CommandArgumentMultiline Button {
        min-width: 5;
        padding: 0;
    }
    CommandArgumentMultiline Input:focus {
        background: $background-lighten-1;
    }
    """

    def compose(self):
        with VerticalScroll(id="lines"):
            pass
        with Vertical():
            b = Button("+", id="add")
            b.can_focus = False
            yield b
            b = Button("-", id="remove")
            b.can_focus = False
            yield b

    @on(Button.Pressed, "#add")
    def add_line(self):
        self.query_one("#lines").mount(
            Input(validators=[Function(self._is_valid_argument)])
        )

    @on(Button.Pressed, "#remove")
    def remove_line(self):
        try:
            self.query(Input)[-1].remove()
        except IndexError:
            return

    def _is_valid_argument(self, value: str) -> bool:
        return True
        try:
            self.entry.validate_argument(value)
        except ValueError:
            return False
        else:
            return True

    @property
    def value(self):
        lines = []
        for inp in self.query(Input):
            line = inp.value
            lines.append(line)
        return lines
