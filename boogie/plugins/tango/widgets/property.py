from datetime import datetime
import difflib

from textual import on
from textual.containers import Horizontal, Vertical
from textual.reactive import reactive
from textual.events import Key
from textual.widgets import (
    Static,
    Button,
    DataTable,
    TextArea,
    TabbedContent,
    TabPane,
)
from textual.screen import ModalScreen
from textual.binding import Binding

from boogie.widgets.entry import EntryWidget
from boogie.messages import Reload
from ..device import TangoDeviceProperty
from .details import TangoDetails

# from ..utils import edit_text, suspend_app


class TangoDevicePropertyDetails(TangoDetails[TangoDeviceProperty], can_focus=False):

    DEFAULT_CSS = """
    TangoDevicePropertyDetails {
        height: 1fr;
        .header {
            dock: top;
        }
        TabbedContent {
            height: 1fr;
        }
        TabPane {
            padding: 0 0;
            height: 1fr;
        }
    }
    """

    def compose(self):
        with TabbedContent():
            with TabPane("Value", id="value"):
                yield TangoPropertyValue()
            with TabPane("History", id="history"):
                yield TangoPropertyHistory()

    def on_mount(self) -> None:
        self.query_one("Tabs").can_focus = False
        self.classes = "header-border"

    async def _update(self, entry: TangoDeviceProperty) -> None:
        self.border_title = str(entry)
        with self.app.batch_update():
            self.query_one(TangoPropertyValue).set_entry(entry)
            self.query_one(TangoPropertyHistory).set_entry(entry)
            if len(self.subpath) > 1:
                tab_id = self.subpath[1].name
                tabbed = self.query_one(TabbedContent)
                tabbed.active = tab_id

    @on(TabbedContent.TabActivated)
    async def on_tab_activated(self, message: TabbedContent.TabActivated) -> None:
        message.stop()  # Don't leak these up


class TangoPropertyValue(EntryWidget, can_focus=False):

    DEFAULT_CSS = """
    TangoPropertyValue {
        height: 1fr;
        background: $primary-darken-1;

        Horizontal {
            height: 1;
            width: 100%;
            align-horizontal: center;
        }
        ContentSwitcher {
            background: $primary-darken-3;
        }
    }
    """

    BINDINGS = [
        Binding("e", "edit", "Edit", show=False),
        Binding("ctrl+s", "save", "Save", show=False),
        Binding("c", "copy", "Copy", show=False),
        # ("E", "edit_externally", "Edit externally"),
    ]

    editing: reactive[bool] = reactive(False)

    def compose(self):
        with Horizontal():
            yield Static()
        yield TextArea(show_line_numbers=True, read_only=True, soft_wrap=False)
        self._value = ""

    async def _update(self, entry: TangoDeviceProperty) -> None:
        raw_value = await entry.get_value()
        self._value = self._value = "\n".join(raw_value)

        self.query_one(TextArea).load_text(self._value)

    def watch_editing(self, _, editing: bool) -> None:
        self.query_one(TextArea).read_only = not editing
        self._show_help()

    def _show_help(self) -> None:
        s = self.query_one(Static)
        if self.editing:
            s.update("Press [b]Ctrl+S[/] to save, [b]Escape[/] to cancel")
        else:
            s.update("Press [b]E[/] to edit, [b]C[/] to copy to clipboard")

    # @work
    # async def edit_external(self) -> None:
    #     "Edit the property in an external editor, determined by EDITOR env var"
    #     # Experimental! Probably doesn't work
    #     text_area = self.query_one(TextArea)
    #     current_row, _ = text_area.cursor_location
    #     with suspend_app(self.app):
    #         result = edit_text(
    #             await self.entry.get_value(), self.entry.name, current_row + 1
    #         )
    #     if result is not None:
    #         await self.app.run_entry_method(self.entry.set_value, value=result)
    #         self.notify(f"Updated property '{str(self.entry)}'", severity="information")
    #     else:
    #         self.notify(
    #             f"No change made to property '{str(self.entry)}'", severity="warning"
    #         )
    #     await self._update(self.entry)

    # def on_tango_property_value_editor_edit_externally(self, message) -> None:
    #     self.edit_external()

    def action_edit(self) -> None:
        if not self.editing:
            self.editing = True

    async def action_save(self) -> None:
        if self.editing:
            text_area = self.query_one(TextArea)
            value = text_area.text.splitlines()
            assert self.entry
            await self.app.run_entry_method(
                self.entry, self.entry.set_value, value=value
            )
            await self._update(self.entry)
            self.notify(f"Wrote property {self.entry.name}", severity="warning")
            self.editing = False

    def action_copy(self) -> None:
        area = self.query_one(TextArea)
        text = area.selected_text or area.text
        if text:
            self.app.copy_to_clipboard(text, "Copied to clipboard")

    def reset_editor(self) -> None:
        self.query_one(TextArea).load_text(self._value)

    async def on_key(self, event: Key) -> None:
        if event.key == "escape":
            if self.editing:
                event.stop()
                self.editing = False
                self.reset_editor()
                assert self.entry
                self.notify(f"No changes were made to property {self.entry.name}")


class TangoPropertyHistory(EntryWidget):

    DEFAULT_CSS = """
    TangoPropertyHistory {
        height: 1fr;
    }

    TangoPropertyHistory DataTable {
        height: 1fr;
    }
    """

    def compose(self):
        data_table = DataTable()
        data_table.add_columns("Timestamp", "#lines", "Value (first 10 lines)")
        data_table.cursor_type = "row"
        data_table.zebra_stripes = True
        yield data_table
        self.can_focus = False

    def focus(self, **kwargs):
        # Make sure to focus the table
        data_table = self.query_one(DataTable)
        data_table.focus()

    def parse_property_history_time(self, time: str) -> datetime | None:
        try:
            return datetime.strptime(time, "%d/%m/%Y %H:%M:%S")
        except Exception as e:
            print("Failed to parse %r: %r", time, e)
        return None

    async def _update(self, entry: TangoDeviceProperty) -> None:
        with self.app.batch_update():
            data_table = self.query_one(DataTable)
            data_table.clear()
            history = await entry.get_history()
            n_items = len(history)
            for index, item in enumerate(reversed(history)):
                value = item.get_value()
                deleted = item.is_deleted()
                data_table.add_row(
                    self.parse_property_history_time(item.get_date()),
                    "DELETED" if deleted else len(value),
                    "\n".join(value[:10]) if value else "",
                    height=None,
                    key=str(n_items - index - 1),
                )

    @on(DataTable.RowSelected)
    def on_selected(self, message: DataTable.RowSelected) -> None:
        async def maybe_restore(restore):
            if restore:
                history = await self.entry.get_history()
                item = history[index]
                await app.run_entry_method(
                    self.entry, self.entry.set_value, value=item.get_value()
                )
                self.post_message(Reload())

        assert message.row_key.value
        index = int(message.row_key.value)
        assert self.entry
        self.app.push_screen(
            PropertyHistoryDiffModal(self.entry, index),
            maybe_restore,
        )


class PropertyHistoryDiffModal(ModalScreen):
    """
    Screen with a yes/no dialog.
    """

    DEFAULT_CSS = """
    PropertyHistoryDiffModal {
        Vertical {
            height: 100%;
            width: 100%;
            margin: 2 4;
            border: solid white;
            background: $background;
            border-title-align: center;
            DataTable {
                height: 1fr;
            }
            Horizontal {
                height: auto;
                width: 1fr;
                Button {
                    width: 1fr;
                }
            }
        }
    }
    """

    def __init__(self, entry: TangoDeviceProperty, index: int, *args, **kwargs) -> None:
        self.entry = entry
        self.index = index
        super().__init__(*args, **kwargs)

    def compose(self):
        with Vertical() as v:
            v.border_title = f"Diff for property '{self.entry.name}'"
            dt = DataTable()
            dt.add_columns("Current", "Historical")
            dt.show_cursor = False
            dt.zebra_stripes = True
            yield dt
            with Horizontal():
                yield Button("Cancel", variant="error", id="no")
                b = Button("Restore", variant="primary", id="yes")
                b.tooltip = (
                    "This will overwrite the current property value with "
                    + " the historical value."
                )
                yield b

    async def on_mount(self) -> None:
        history = await self.entry.get_history()
        current_value = list(history[-1].get_value())
        historical_value = list(history[self.index].get_value())
        # Show a simple line based diff (experimental!)
        diff = difflib.ndiff(current_value, historical_value)
        current = []
        historical = []
        for line in diff:
            stripped_line = line[2:]
            if line.startswith("  "):
                # Line in both
                current.append(stripped_line)
                historical.append(stripped_line)
            elif line.startswith("+ "):
                # Line added
                current.append("[#505050]---[/#505050]")
                historical.append(f"[#00FF00]{stripped_line}[/#00FF00]")
            elif line.startswith("- "):
                # Line removed
                current.append(f"[#FF0000]{stripped_line}[/#FF0000]")
                historical.append("[#505050]---[/#505050]")
            elif line.startswith("? "):
                # Subline info
                pass
        dt = self.query_one(DataTable)
        dt.add_rows(zip(current, historical))

    def on_button_pressed(self, message: Button.Pressed) -> None:
        if message.button.id == "yes":
            self.dismiss(True)
        else:
            self.dismiss(False)

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss(False)


if __name__ == "__main__":

    import sys

    from textual.app import App

    from boogie.plugins.tango.getters import get_device_entry

    class PropertyApp(App):

        CSS_PATH = ["../../../app2.css"]

        def compose(self):
            """Create child widgets for the app."""
            yield TangoDevicePropertyDetails()

        async def on_ready(self):
            device, prop = sys.argv[1:]
            dev_entry = await get_device_entry(device)
            properties_entry = await dev_entry.get_child("properties")
            entry = await properties_entry.get_child(prop)
            self.query_one(TangoDevicePropertyDetails).set_entry(entry)

    app = PropertyApp()

    app.run()
