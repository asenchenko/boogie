import asyncio
from collections import deque
from typing import Generic

import tango
from textual.widgets import DataTable, TabbedContent, TabPane

from .details import TangoDetails, TangoEntryWidget
from ..device import (
    TangoDevicePolling,
    TangoDevicePolledAttributes,
    TangoDevicePolledCommands,
)
from boogie.entry import EntryType
from boogie.widgets.entry import EntryWidget


# from textual_plotext import PlotextPlot
PlotextPlot = None


class DevicePolling(TangoDetails[TangoDevicePolling]):

    DEFAULT_CSS = """
    DevicePolling {
    }
    """

    def compose(self):
        self.border_title = "Polling"
        with TabbedContent():
            with TabPane("Attributes"):
                yield DeviceAttributePolling(self)
            # with TabPane("Commands"):
            #     yield DeviceCommandPolling(self)
        self.classes = "header-border"

    async def _update(self, entry):
        # self.query_one("#attributes").set_entry(await entry.get_child("attributes"))
        # self.query_one("#commands").set_entry(await entry.get_child("commands"))
        pass


class DevicePollingBase(TangoEntryWidget, Generic[EntryType]):

    DEFAULT_CSS = """
    DeviceAttributePolling {
        border-top: none;
    }
    """

    COLUMNS = "Name", r"Period \[ms]"

    POLLING_TYPE: str | None = None

    def compose(self):
        data_table = DataTable()
        data_table.add_columns(*self.COLUMNS)
        data_table.cursor_type = "row"
        data_table.zebra_stripes = True
        yield data_table
        yield self.error_widget()
        self.border_title = "Polling"

    async def _update(self, entry):
        data_table = self.query_one(DataTable)
        data_table.clear()
        polling_statuses = await entry.parent.get_polling_status()
        polled_attrs = polling_statuses.get("attribute", {})
        for attr in polled_attrs:
            name = attr["name"]
            period = attr["period"]
            data_table.add_row(name, period)

    def on_data_table_row_selected(self, message: DataTable.RowSelected) -> None:
        print(message)


class DeviceAttributePolling(DevicePollingBase[TangoDevicePolledAttributes]):

    POLLING_TYPE = "command"


class DeviceCommandPolling(TangoDevicePolledCommands):

    POLLING_TYPE = "command"


class _DevicePolling(TangoDetails):

    DEFAULT_CSS = """
    DevicePolling {
    }
    DevicePolling #plot {
        height: 1fr;
    }
    """

    def compose(self):
        self.border_title = "Polling"
        # yield PlotextPlot()
        yield DevicePollingSubDetails()
        yield self.error_widget()

    async def get_poll_deltas(self, entry, attribute, last_time=0, history_size=10):
        "Return any new polling deltas received"
        hist = await entry.get_attribute_history(attribute, history_size)
        times = [h.time.totime() for h in hist if h.time.totime() >= last_time]
        if not times:
            return [], last_time
        deltas = [round((t2 - t1) * 1000) for t1, t2 in zip(times, times[1:])]
        return deltas, max(times)
        # return [1000 + gauss(0, 10) for _ in range(3)], max(times)

    async def _update(self, entry):
        self.query_one(DevicePollingSubDetails).set_entry(entry)
        p = self.query_one(PlotextPlot)
        period_deltas = {}
        p.plt.title("Polling deltas [ms]")
        p.plt.yticks([])
        p.tooltip = (
            "This histogram shows the distribution of polling deltas, "
            + " i.e. the time between consecutive pollings. Ideally,"
            + " the deltas should be very close to their respective"
            + " polling periods. If that is not the case, it indicates"
            + " slow operations, or overload on polling threads."
        )
        t0s = {}
        while True:
            try:
                polling_status = await entry.get_polling_status()
                period_attrs = polling_status.get("attribute", {})
                for period, attrs in period_attrs.items():
                    name = attrs[0]["name"]
                    deltas, t0s[name] = await self.get_poll_deltas(
                        entry,
                        name,
                        t0s.get(name, 0),
                        history_size=attrs[0]["buffer_depth"],
                    )
                    period_deltas.setdefault(period, deque(maxlen=1000)).extend(deltas)
                    # for attr in attrs:
                    #     period_deltas.setdefault(period, []).append(attr["reading_time"])
                p.plt.clear_data()
                for period, deltas in period_deltas.items():
                    if deltas:
                        bins = 20  # min(len(deltas), self.size.width - 2)
                        p.plt.hist(deltas, bins, label=f"{period}")
                p.refresh()
                self._error = None
            except tango.DevFailed as e:
                self._error = e
                await asyncio.sleep(10)
                polling_status = None

            await asyncio.sleep(1)


class DevicePollingSubDetails(TangoEntryWidget):

    DEFAULT_CSS = """
    DevicePollingSubDetails {
        height: 1fr;
    }
    DevicePollingSubDetails TabbedContent {
        height: 1fr;
    }

    DevicePollingSubDetails ContentSwitcher {
        height: 1fr;
    }

    DevicePollingSubDetails TabPane {
        height: 1fr;
        width: 1fr;
        overflow: auto;
    }
    """

    def compose(self):
        with TabbedContent():
            with TabPane("Attributes"):
                yield DeviceAttributePolling()
            with TabPane("Commands"):
                yield DeviceCommandPolling()

    # @on(TabbedContent.TabActivated)
    # async def on_tab_activated(self, message):
    #     entry = self.subpath[0]
    #     child = await entry.get_child(message.tab_id)
    #     self.post_message(NavigateTo(child))

    async def _update(self, entry):
        for w in self.query(EntryWidget):
            w.subpath = self.subpath
