from collections import deque

from textual.widgets import Static, Digits
from textual.containers import Container
from textual.reactive import reactive
import numpy as np
import tango
from rich.pretty import Pretty
from rich.text import Text

from boogie.widgets.entry import MonitorWidget, MonitorWidgetTrend
from boogie.plot import plot, time_plot, category_time_plot
from ..device import TangoDeviceAttribute
from ..listener import get_listener


NUMERIC_TYPES = {
    value
    for value in tango.CmdArgType.values.values()
    if tango.utils.is_numerical(value)
}


class TangoAttributeMonitorWidget(Static):
    "Base class"


class TangoAttributeMonitorSimple(TangoAttributeMonitorWidget):

    DEFAULT_CSS = """
    TangoAttributeMonitorSimple {
        height: auto;
        width: 1fr;
        layout: grid;
        grid-size: 4 1;
        grid-columns: auto 1fr auto 2;
        grid-gutter: 0 1;
        #name {
            text-style: bold;
        }
        #quality {
        }
        #value {
            height: auto;
            text-align: right;
            text-style: bold;
        }
        &.error, &.failed {
            background: rgba(255,0,0,0.1);
        }
    }
    """

    attr_name: reactive[str] = reactive("", init=False)
    attr_config: reactive[tango.AttributeInfoEx | None] = reactive(None, init=False)
    attr_data: reactive[tango.DeviceAttribute | tango.DevFailed | None] = reactive(
        None, init=False
    )

    def watch_attr_name(self, _, name):
        name_widget = self.query_one("#name", expect_type=Static | Digits)
        name_widget.update(self.format_name(name))

    def format_name(self, name):
        dev, attr = name.rsplit("/", 1)
        return Text.from_markup(f"[#888888]{dev}/[/]{attr}")

    def format_value(self, value):
        if isinstance(value, tango.DevState):
            return Text(str(value), style=STATE_COLORS[value])
        return Pretty(value)

    def watch_attr_data(self, _, data):
        value_widget = self.query_one("#value", expect_type=Static | Digits)
        unit_widget = self.query_one("#unit", expect_type=Static)
        quality_widget = self.query_one("#quality", expect_type=Static)
        if isinstance(data, tango.DevFailed):
            if data.args[0].reason == "API_AttrNotFound":
                value_widget.update("N/A")
            else:
                value_widget.update(Pretty(data))
            self.classes = "error"
            quality_widget.classes = ""
        else:
            if data.has_failed:
                value_widget.update(Pretty(data.get_err_stack()))
                quality_widget.classes = ""
                self.add_class("failed")
            elif data.is_empty:
                value_widget.update("<Empty>")
                quality_widget.classes = f"quality-{str(data.quality)}"
                self.remove_class("failed")
            else:
                if self.attr_config:
                    if (
                        self.attr_config.format
                        and self.attr_config.format != "Not specified"
                    ):
                        try:
                            value_widget.update(self.attr_config.format % data.value)
                        except TypeError:
                            value_widget.update(Pretty(data.value))
                    else:
                        value_widget.update(self.format_value(data.value))
                    if self.attr_config.unit:
                        unit_widget.update(self.attr_config.unit)
                else:
                    value_widget.update(Pretty(data.value))
                quality_widget.classes = f"quality-{str(data.quality)}"
                quality_widget.tooltip = str(data.quality)
                self.remove_class("failed")


class TangoAttributeMonitorSmall(TangoAttributeMonitorSimple):

    # Set of attribute formats that this widget claims to handle
    DATA_FORMATS = {tango.AttrDataFormat.SCALAR, tango.AttrDataFormat.SPECTRUM}
    # DATA_TYPES if defined does the same for data types.

    def compose(self):
        np.set_printoptions(threshold=5)
        self.can_focus = False
        yield Static(id="name")
        yield Static(id="value")
        yield Static(id="unit")
        yield Static("⬤", id="quality", expand=True)

    def on_mount(self):
        value_widget = self.query_one("#value", expect_type=Static)
        value_widget.update("")
        value_widget.expand = True
        unit_widget = self.query_one("#unit", expect_type=Static)
        unit_widget.update("")
        quality_widget = self.query_one("#quality", expect_type=Static)
        quality_widget.classes = ""


STATE_COLORS = {
    tango.DevState.ON: "#00FF00",
    tango.DevState.OFF: "#666666",
    tango.DevState.CLOSE: "#666666",
    tango.DevState.OPEN: "#00FF00",
    tango.DevState.INSERT: "#666666",
    tango.DevState.EXTRACT: "#00FF00",
    tango.DevState.MOVING: "rgb(128,160,255)",
    tango.DevState.STANDBY: "rgb(255,255,0)",
    tango.DevState.FAULT: "red",
    tango.DevState.INIT: "rgb(204,204,122)",
    tango.DevState.RUNNING: "rgb(128,160,255)",
    tango.DevState.ALARM: "rgb(255,140,0)",
    tango.DevState.DISABLE: "rgb(255,0,255)",
    tango.DevState.UNKNOWN: "rgb(128,128,128)"
}


class TangoAttributeMonitorLarge(TangoAttributeMonitorSimple):

    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = NUMERIC_TYPES

    DEFAULT_CSS = """
    TangoAttributeMonitorLarge {
        #value-container {
            height: auto;
            width: 1fr;
            align-horizontal: right;
        }
        #value {
            width: auto;
        }
    }
    """

    def compose(self):
        self.can_focus = False
        yield Static(id="name")
        with Container(id="value-container"):
            yield Digits(id="value")
        yield Static(id="unit")
        yield Static("⬤", id="quality", expand=True)

    def on_mount(self):
        value_widget = self.query_one("#value", expect_type=Digits)
        value_widget.update("")
        value_widget.expand = True
        unit_widget = self.query_one("#unit", expect_type=Static)
        unit_widget.update("")
        quality_widget = self.query_one("#quality", expect_type=Static)
        quality_widget.classes = ""


class AttributeMonitorTrend(MonitorWidgetTrend):

    DEFAULT_CSS = """
    AttributeMonitorTrend {
        layout: grid;
        grid-size: 1 2;
        grid-rows: 1 1fr;
        width: 1fr;
        #plot {
            width: 1fr;
            height: 100%;
        }
    }
    """

    attr_name: reactive[str] = reactive("", init=False)
    attr_config: reactive[tango.AttributeInfoEx | None] = reactive(None, init=False)
    attr_data: reactive[tango.DeviceAttribute | tango.DevFailed | None] = reactive(
        None, init=False
    )

    def compose(self):
        yield TangoAttributeMonitorSmall(id="small")
        yield Static(id="plot")

    def on_mount(self):
        self._timestamps = deque(maxlen=10000)
        self._values = deque(maxlen=10000)
        # self._run()

    def watch_attr_name(self, _, name):
        self.get_widget_by_id("small").attr_name = name

    def watch_attr_config(self, _, config):
        self.get_widget_by_id("small").attr_config = config

    def watch_attr_data(self, _, data):
        self.get_widget_by_id("small").attr_data = data
        if isinstance(data, tango.DeviceAttribute):
            self._values.append(data.value)
            self._timestamps.append(data.time.todatetime().astimezone(tz=None))


class AttributeMonitorTrendNumeric(AttributeMonitorTrend):

    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = NUMERIC_TYPES | {tango.DevBoolean}

    DEFAULT_CSS = """
    AttributeMonitorTrendNumeric {
        height: 15vh;
        min-height: 7;
    }
    """

    def draw(self, time_window, time_ticks):
        plot_widget = self.get_widget_by_id("plot")
        p = time_plot(
            data=[(self._timestamps, self._values, self.attr_name, "rgb(255, 150, 0)")],
            x_limits=time_window,
            height=plot_widget.size.height,
            width=plot_widget.size.width,
            time_ticks=time_ticks,
            show_x_axis=False,
            show_legend=False,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))


class AttributeMonitorTrendBoolean(AttributeMonitorTrend):

    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = {tango.DevBoolean}

    DEFAULT_CSS = """
    AttributeMonitorTrendBoolean {
        height: 2;
    }
    """

    attr_name: reactive[str] = reactive("", init=False)
    attr_config: reactive[tango.AttributeInfoEx | None] = reactive(None, init=False)
    attr_data: reactive[tango.DeviceAttribute | tango.DevFailed | None] = reactive(
        None, init=False
    )

    PLOT_COLORS = {
        False: "red",
        True: "green",
    }

    def draw(self, time_window, time_ticks):
        plot_widget = self.get_widget_by_id("plot")
        p = category_time_plot(
            data=[(self._timestamps, self._values)],
            colors=self.PLOT_COLORS,
            t_limits=time_window,
            height=plot_widget.size.height,
            width=plot_widget.size.width,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))


class AttributeMonitorTrendState(AttributeMonitorTrend):

    DATA_FORMATS = {tango.AttrDataFormat.SCALAR}
    DATA_TYPES = {19}  # TODO DevState is somehow different

    DEFAULT_CSS = """
    AttributeMonitorTrendState {
        height: 2;
    }
    """

    attr_name: reactive[str] = reactive("", init=False)
    attr_config: reactive[tango.AttributeInfoEx | None] = reactive(None, init=False)
    attr_data: reactive[tango.DeviceAttribute | tango.DevFailed | None] = reactive(
        None, init=False
    )

    PLOT_COLORS = STATE_COLORS

    def draw(self, time_window, time_ticks):
        plot_widget = self.get_widget_by_id("plot")
        p = category_time_plot(
            data=[(self._timestamps, self._values)],
            colors=self.PLOT_COLORS,
            t_limits=time_window,
            height=plot_widget.size.height,
            width=plot_widget.size.width,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))


class TangoAttributeMonitorSpectrum(TangoAttributeMonitorWidget):

    DATA_FORMATS = {tango.AttrDataFormat.SPECTRUM}
    DATA_TYPES = NUMERIC_TYPES

    DEFAULT_CSS = """
    TangoAttributeMonitorSpectrum {
        height: 10;
        width: 1fr;
        Container {
            #name {
                text-style: bold;
            }
            #size {
            }
            height: 1;
            layout: grid;
            grid-size: 4 1;
            grid-columns: auto 1fr auto 2;
            grid-gutter: 0 1;
        }
        #plot {
            width: 100%;
            height: 1fr;
        }
    }
    """

    attr_name: reactive[str] = reactive("", init=False)
    attr_config: reactive[tango.AttributeInfoEx | None] = reactive(None, init=False)
    attr_data: reactive[tango.DeviceAttribute | tango.DevFailed | None] = reactive(
        None, init=False
    )

    def compose(self):
        with Container():
            yield Static(id="name")
            yield Static(id="size")
            yield Static(id="unit")
            yield Static("⬤", id="quality", expand=True)
        yield Static(id="plot")

    def watch_attr_name(self, _, name: str) -> None:
        self.get_widget_by_id("name").update(name)

    def watch_attr_config(self, _, config: tango.AttributeInfoEx) -> None:
        self.get_widget_by_id("unit").update(config.unit or "")

    def watch_attr_data(self, _, data: tango.DeviceAttribute | tango.DevFailed) -> None:
        if isinstance(data, tango.DeviceAttribute):
            self.draw(data)

    def draw(self, data: tango.DeviceAttribute) -> None:
        css_vars = self.app.get_css_variables()
        self.query_one("#size").update(f"[{len(data.value)}]")
        self.query_one("#quality").classes = f"quality-{str(data.quality)}"
        plot_widget = self.get_widget_by_id("plot")
        p = plot(
            data=data.value,
            color=css_vars["primary-lighten-3"],
            height=plot_widget.size.height,
            width=plot_widget.size.width,
            show_x_axis=False,
            show_legend=False,
        )
        self.get_widget_by_id("plot").update(Text.from_ansi(p))


class TangoAttributeMonitor(MonitorWidget[TangoDeviceAttribute]):
    """
    Widget for monitoring a single Tango attribute.
    Uses appropriate child widgets depending on attribute format and type.
    The "swap" action allows choosing between several alternatives.
    """

    DEFAULT_CSS = """
    TangoAttributeMonitor {
        height: auto;
    }
    """

    BINDINGS = [(".", "swap", "Swap")]

    # Widget classes available for monitoring
    WIDGETS: list[type(TangoAttributeMonitorWidget)] = [
        TangoAttributeMonitorSmall,
        TangoAttributeMonitorLarge,
        TangoAttributeMonitorSpectrum,
        AttributeMonitorTrendBoolean,
        AttributeMonitorTrendState,
        AttributeMonitorTrendNumeric,
    ]

    current = None

    async def _update(self, entry: TangoDeviceAttribute) -> None:
        config = await entry.get_config()
        self.tooltip = "\n".join([
            f"Attribute: {await entry.get_name()}",
            f"Data type: {tango.CmdArgType.values[config.data_type]}",
            f"Data format: {config.data_format}",
            f"Description: {config.description}",
        ])

        self.can_focus = True
        try:
            self.info = await entry.get_config()
            self.classes = ""
        except tango.DevFailed:
            return
        with self.app.batch_update():
            self.query("*").remove()
            data_type = tango.CmdArgType.values[self.info.data_type]
            print("data_type", data_type)
            widgets = [
                widget_class()
                for widget_class in self.WIDGETS
                if (
                    self.info.data_format in widget_class.DATA_FORMATS
                    and (not hasattr(widget_class, "DATA_TYPES")
                         or data_type in widget_class.DATA_TYPES)
                )
            ]
            print("monitor widgets", entry, widgets)
            for w in widgets:
                await self.mount(w)
                w.display = False
            self.current = widgets[0]
            await self._update_current_widget()

        attr = await entry.get_full_name()
        listener = await get_listener()
        async for attr, data in listener.listen([attr]):
            self.current.attr_data = data  # type: ignore

    async def _update_current_widget(self) -> None:
        self.current.display = True
        name = await self.entry.get_name()
        self.current.attr_name = name
        attr_config = await self.entry.get_config()
        self.current.attr_config = attr_config

    async def action_swap(self) -> None:
        "Toggle through the list of supported monitor widgets"
        try:
            widgets = list(self.children)
            index = widgets.index(self.current)
            next_index = (index + 1) % len(widgets)
            self.current.display = False
            self.current = widgets[next_index]
        except Exception as e:
            self.log("error", exc=e)
        await self._update_current_widget()
        self.screen._run_time_axis()
