from typing import Generic

from rich.text import Text
import tango
from textual.widgets import DataTable, Label, Input, Button, TextArea
from textual.screen import ModalScreen
from textual.containers import Horizontal
from textual.events import Key

from boogie.messages import NavigateTo
from boogie.widgets.modal import ConfirmModal, GetStringModal
from boogie.widgets.entry import EntryType
from ..device import TangoDeviceProperty, TangoDeviceProperties
from ..classes import TangoDeviceClassProperties
from ..properties import TangoObjectProperties
from .details_list import TangoDetailsList


INTERNAL_PROPERTY_DESCRIPTIONS = tango.utils.CaselessDict(
    {
        "description": "Description string for the device",
        "polled_attr": "Attribute polling config (name, period, ...)",
        "polled_cmd": "Command polling config (name, period, ...)",
        "logging_level": "Persistent logging level",
        "logging_target": "Persistent logging target",
        # ...?
    }
)


class CreatePropertyModal(ModalScreen[tuple[str, list[str]]]):

    DEFAULT_CSS = """
    CreatePropertyModal {
        align: center middle;
        padding: 0 1;
        background: $surface;
        border: thick $primary;
    }
    CreatePropertyModal Label {
        width: 1fr;
        height: 2;
        content-align: center middle;
    }
    CreatePropertyModal Input {
        column-span: 2;
        width: 1fr;
        height: auto
    }
    CreatePropertyModal TextArea {
        column-span: 2;
        height: 1fr;
        width: 1fr;
        margin: 1 1;
    }
    CreatePropertyModal Horizontal {
        height: auto;
    }
    CreatePropertyModal Button {
        width: 1fr;
    }
    """

    def __init__(self, devicename, *args, **kwargs):
        self.devicename = devicename
        super().__init__(*args, **kwargs)

    def compose(self):
        yield Label(f"Creating new property for device {self.devicename}")
        yield Input(placeholder="Name")
        yield TextArea(show_line_numbers=True, soft_wrap=False)
        with Horizontal():
            yield Button("Cancel", variant="error", id="no")
            yield Button("Proceed", variant="primary", id="yes")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "yes":
            name = self.query_one(Input).value
            value = self.query_one(TextArea).text.splitlines()
            self.dismiss((name, value))
        else:
            self.dismiss()

    def on_key(self, event: Key) -> None:
        if event.key == "escape":
            event.prevent_default()
            self.dismiss()


class TangoPropertyDetailsList(TangoDetailsList, Generic[EntryType]):

    BINDINGS = [
        ("n", "create", "New"),
        ("r", "rename", "Rename"),
        ("delete", "delete", "Delete"),
    ]

    COLUMNS = "Property", "Description"

    async def _update_children(
        self,
        entries: list[TangoDeviceProperty],
        pattern: str,
    ):
        assert self.entry
        try:
            self.property_descriptions, self.property_defaults = (
                await self.entry.get_info()
            )
        except tango.DevFailed:
            self.property_descriptions = {}
            self.property_defaults = {}
        await super()._update_children(entries, pattern)

    def get_description(self, name):
        if name := INTERNAL_PROPERTY_DESCRIPTIONS.get(name):
            return name
        return self.property_descriptions.get(name, "")

    def get_style(self, entry: TangoDeviceProperty):
        if entry.name in INTERNAL_PROPERTY_DESCRIPTIONS:
            return ""
        if entry.deleted:
            return "bold red"
        return "bold"

    async def get_row(self, entry: TangoDeviceProperty):
        return (
            Text(entry.name, style=self.get_style(entry)),
            self.get_description(entry.name),
            # self.property_defaults.get(entry.name, ""),
        ), {}

    async def action_rename(self) -> None:
        data_table = self.query_one(DataTable)
        entry = self.entries[data_table.cursor_row]
        assert entry

        async def maybe_rename(new_name: str) -> None:
            if new_name:
                await entry.rename(new_name)
                await self._load_children(self.entry)
                assert self.entry
                new_entry = await self.entry.get_child(new_name)
                self.post_message(NavigateTo(new_entry))
                self.notify(
                    f"Property '{entry.name}' renamed to '{new_name}'",
                    severity="warning",
                )

        question = f"Rename property '{str(entry)}'?"
        self.app.push_screen(GetStringModal(question, initial=str(entry)), maybe_rename)

    async def action_delete(self) -> None:
        data_table = self.query_one(DataTable)
        entry = self.entries[data_table.cursor_row]

        async def maybe_delete(delete) -> None:
            if delete:
                assert isinstance(entry, TangoDeviceProperty)
                try:
                    await self.app.run_entry_method(entry, entry.delete)
                    # await self._load_children(self.entry)
                    assert self.entry
                    self.post_message(NavigateTo(self.entry))
                    # self.post_message(Reload())
                    self.notify(f"Property '{entry.name}' removed", severity="warning")
                    await self._load_children(self.entry)
                except tango.DevFailed as e:
                    self.notify(f"Error: {e.args[-1].desc}")

        question = f"Really want to delete property\n'{str(entry)}'?"
        self.app.push_screen(ConfirmModal(question), maybe_delete)

    async def action_create(self) -> None:

        async def maybe_create(result) -> None:
            if result:
                name, value = result
                if name:
                    if value:
                        try:
                            assert self.entry
                            await self.app.run_entry_method(
                                self.entry, self.entry.put, name=name, value=value
                            )
                            self.notify(f"Created new property '{name}'")
                            await self._load_children(self.entry)
                            new_entry = await self.entry.get_child(name)
                            self.post_message(NavigateTo(new_entry))
                        except tango.DevFailed as e:
                            self.notify(f"Failed to create property: {e}")
                    else:
                        self.notify("Can't create empty property.")
            else:
                self.notify("Property not created!")

        # assert isinstance(self.entry, TangoDeviceProperties)
        assert self.entry
        self.app.push_screen(CreatePropertyModal(self.entry.parent.name), maybe_create)


class TangoDevicePropertyDetailsList(TangoPropertyDetailsList[TangoDeviceProperties]):
    pass


class TangoDeviceClassPropertyList(
    TangoPropertyDetailsList[TangoDeviceClassProperties]
):
    pass


class TangoObjectPropertyList(TangoPropertyDetailsList[TangoObjectProperties]):
    pass
