from rich.text import Text
from rich.table import Table

import tango
from textual.widgets import Static

from boogie.widgets.entry import Error


class TangoError(Error):
    def watch_error(self, old_error, error):
        if isinstance(error, tango.DevFailed):
            self.border_title = "Tango error"
            table = Table.grid(padding=(0, 1))
            table.add_column(max_width=12, justify="right")
            table.add_column()
            for err in reversed(error.args):
                table.add_row(Text("Desc:", style="bold"), str(err.desc))
                table.add_row(Text("Reason:", style="bold"), str(err.reason))
                table.add_row(Text("Severity:", style="bold"), str(err.severity))
                table.add_row(Text("Origin:", style="bold"), str(err.origin))
                table.add_row("", "")
            self.query_one(Static).update(table)
        else:
            super().watch_error(old_error, error)
