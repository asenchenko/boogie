from __future__ import annotations
import asyncio
from inspect import ismethod
import logging
from time import time
from typing import Any, Awaitable

import tango
from tango.asyncio import DeviceProxy, AttributeProxy

from boogie.cache import cached


logger = logging.getLogger(__name__)


@cached
async def get_device_proxy(devname: str) -> DeviceProxy:
    t0 = time()
    proxy = await DeviceProxy(devname)
    print(f"Got proxy for {devname} in {time() - t0}s.")
    return proxy


@cached
async def get_attribute_proxy(devname: str, attrname: str) -> AttributeProxy:
    return await AttributeProxy(f"{devname}/{attrname}")


@cached
async def get_short_name(maybe_full_name: str) -> str:
    prefix = get_attr_prefix()
    if maybe_full_name.startswith(prefix):
        return maybe_full_name[len(prefix):]
    return maybe_full_name


def get_attr_prefix() -> str:
    # db = await get_db()
    # return f"tango://{await db.get_db_host()}:{await db.get_db_port()}/"
    return f"tango://{tango.ApiUtil.get_env_var('TANGO_HOST')}/"


@cached
async def get_full_name(maybe_full_name: str) -> str:
    "Get the full Tango 'URL' for the attribute"
    # TODO is this too naive?

    if maybe_full_name.startswith("tango://"):
        return maybe_full_name.lower()
    attr_prefix = get_attr_prefix()
    return f"{attr_prefix}{maybe_full_name}".lower()


class AsyncTangoDatabase:
    def __init__(self, db=None):
        self.db = None
        self.done = asyncio.Event()

    async def init(self):
        tango_host = tango.ApiUtil.get_env_var("TANGO_HOST")
        print(f"Creating AsyncTangoDatabase {tango_host}")
        t0 = time()
        loop = asyncio.get_event_loop()
        self.db = await loop.run_in_executor(None, tango.Database)
        self.done.set()
        print(f"Created AsyncTangoDatabase in {time() - t0:.3f} s.")
        return self

    async def get(self) -> AsyncTangoDatabase:
        if not self.done.is_set():
            logger.debug("Waiting for Tango db connection")
            await self.done.wait()
            logger.debug("Returning AsyncTangoDatabase")
        return self

    @cached
    async def get_uri(self) -> str:
        host = await self.get_db_host()
        port = await self.get_db_port()
        return f"tango://{host}:{port}/"

    def __getattr__(self, attr) -> Any:
        cmd = getattr(self.db, attr)

        if ismethod(cmd) and not asyncio.iscoroutine(cmd):
            loop = asyncio.get_event_loop()

            def wrapper(*args):
                t0 = time()
                result = loop.run_in_executor(None, cmd, *args)
                logger.debug(f"Async DB call: {attr} {args} took {time() - t0:.3e} s")
                return result

            return wrapper

    async def query(self, query: str) -> list[str]:
        "Make a SQL select query to the Tango database and return the rows"
        _, results = await self.db.command_inout(
            "DbMySQLSelect", query, green_mode=tango.GreenMode.Asyncio
        )
        return results


_db: AsyncTangoDatabase | None = None


def get_db() -> Awaitable[AsyncTangoDatabase]:
    """
    Get an async DB object.
    Make sure we only create the DB once, and return the same object
    on any subsequent calls. Even if the DB is not done when the next
    call comes in. It can take ~1s to connect to the Tango DB.
    Note that this returns a coroutine!
    """
    global _db
    if _db is None:
        _db = AsyncTangoDatabase()
        return _db.init()
    elif _db:
        return _db.get()
    else:
        raise RuntimeError("Should not happen!")


# def edit_text(lines=[], title="", position=None) -> None:
#     """Takes a list of strings and opens $EDITOR on it. Returns the result
#     if changes were made.
#     """
#     # we need a temporary file to open
#     prefix = "boogie-"
#     if title:
#         prefix += title + "-"
#     f = tempfile.NamedTemporaryFile(prefix=prefix, suffix=".txt", delete=False)
#     f.writelines(bytes((line + "\n").encode("utf-8")) for line in lines)
#     tmp_name = f.name
#     t1 = os.path.getmtime(tmp_name)
#     f.close()
#     # launching the default editor (does this work everywhere?)
#     if position:
#         os.system("%s +%d %s" % (os.getenv("EDITOR"), position, tmp_name))
#     else:
#         os.system("%s %s" % (os.getenv("EDITOR"), tmp_name))
#     with open(tmp_name) as f:
#         data = f.read()
#     t2 = os.path.getmtime(tmp_name)
#     os.remove(tmp_name)
#     if t1 != t2:  # crude way of checking if changes were made
#         return data.strip().split("\n")


# @contextmanager
# def suspend_app(app) -> Iterator[None]:
#     """
#     Simple way to suspend the application while some other console progran
#     runs, useful for temporarily starting an editor or something.
#     """
#     driver = app._driver
#     if driver is not None:
#         driver.stop_application_mode()
#         with redirect_stdout(sys.__stdout__), redirect_stderr(sys.__stderr__):
#             yield
#         driver.start_application_mode()
