import asyncio
from datetime import datetime, timedelta
from time import time
from math import floor
from typing import Callable, Awaitable

from rich.text import Text
from textual import work
from textual.screen import Screen
from textual.containers import Container
from textual.widgets import Header, Footer, Static
from textual.css.query import NoMatches
from textual.binding import Binding
from textual.reactive import reactive

from .messages import NavigateTo
from .widgets.entry import MonitorWidget, MonitorWidgetTrend
from .plot import BrailleCanvas, draw_time_axis, get_time_ticks, get_screen_position


class MonitorScreen(Screen):

    DEFAULT_CSS = """
    MonitorScreen {
        height: 100%;
        width: 100%;
        background: $background;
        overflow-y: auto;
        #monitors {
            border-top: solid $primary-background;
            display: none;
        }
        #time-axis {
            height: 3;
            width: 100%;
            dock: bottom;
            margin-bottom: 1;
            padding: 0 1;
            background: $primary-background-darken-1;
        }
        #placeholder {
            width: 1fr;
            height: 1fr;
            align: center middle;
            text-align: center;
            background: $background;
            color: $secondary;
        }
        #placeholder Static {
            width: auto;
        }
    }
    """

    zoom: reactive[int] = reactive(-1)

    BINDINGS = [
        Binding("escape", "show_main", "Close"),
        ("plus", "zoom_in", "Zoom in"),
        ("minus", "zoom_out", "Zoom out"),
    ]

    def compose(self):
        yield Header()
        self.title = "Monitor"
        with Container(id="placeholder"):
            yield Static(
                "Add things (e.g. attributes) to be monitored."
                + "\nPress 'm' when visiting e.g. an attribute."
            )
        yield Static(id="monitors")
        yield Static("Bottom", id="time-axis")
        yield Footer()


    # async def get_listener(self, entry):
    #     listener = self._listeners.get(entry)
    #     if not listener:
    #         listener = await get_listener()

    # def store_data(self, entry, timestamp, value):
    #     data = self.store.get(entry)
    #     if data is None:
    #         data = (deque(maxlen=10000), deque(maxlen=10000))
    #         self._store[entry] = data
    #     timestamps, values = data
    #     timestamps.append(timestamp)
    #     values.append(value)

    # def get_data(self, entry):
    #     data = self.store.get(entry)
    #     if data is None:
    #         data = (deque(maxlen=10000), deque(maxlen=10000))
    #         self._store[entry] = data
    #     return data

    def action_show_main(self):
        self.app.switch_screen("main")

    def action_zoom_in(self):
        self.zoom -= 1
        self._run_time_axis()

    def action_zoom_out(self):
        self.zoom += 1
        self._run_time_axis()

    def time_window(self, width: int) -> tuple[datetime, datetime]:
        scale = 2**self.zoom
        now = datetime.fromtimestamp(int(time() / scale) * scale).astimezone(tz=None)
        return (
            now - timedelta(seconds=2 * width * scale - 1),
            now + timedelta(seconds=1),
        )

    async def add(self, widget, path=None):
        try:
            self.query_one("#placeholder").display = False
        except NoMatches:
            pass
        monitors = self.get_widget_by_id("monitors")
        monitors.display = True
        wrapper = MonitorWidgetWrapper(widget)
        await monitors.mount(wrapper)
        wrapper.focus()
        self._run_time_axis()

    def watch_zoom(self, _, zoom):
        self._run_time_axis()

    @work(exclusive=True, group="run")
    async def _run_time_axis(self):
        """
        If there are trends, update them periodically, together with the
        time axis at the bottom.
        Update period depends on the zoom level.
        """
        widgets = [w for w in self.query(MonitorWidgetTrend) if w.parent.current == w]
        time_axis = self.query_one("#time-axis")
        if len(widgets) == 0:
            # No trend widgets visible; we're done here
            time_axis.display = False
            return
        time_axis.display = True
        time_window = self.time_window(self.size.width-2)
        time_ticks = get_time_ticks(*time_window)
        for w in widgets:
            w.draw(time_window, time_ticks)

        time_axis = self.get_widget_by_id("time-axis")
        axis = draw_time_axis(time_window, time_ticks, time_axis.content_size.width)
        time_axis.update(Text.from_ansi(axis))

        t = time()
        await asyncio.sleep(t - int(t))
        timestep = max(0.25, 2**self.screen.zoom)
        while True:
            current = self.app.screen == self
            # Only draw if the monitor screen is actually visible
            if current:
                widgets = [
                    w for w in self.query(MonitorWidgetTrend) if w.parent.current == w
                ]
                if len(widgets) == 0:
                    # No trend widgets visible; we're done here
                    time_axis.display = False
                    break
                time_axis.display = True
                time_window = self.time_window(self.size.width - 2)
                time_ticks = get_time_ticks(*time_window)
                with self.app.batch_update():
                    for w in widgets:
                        w.draw(time_window, time_ticks)
                    axis = draw_time_axis(time_window, time_ticks, time_axis.content_size.width)
                    time_axis.update(Text.from_ansi(axis))
                    # self.draw_time_axis(time_axis, time_window, time_ticks)
            await asyncio.sleep(timestep)

    TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S"

    # def draw_time_axis(self, time_axis, time_window, time_ticks):
    #     "Draw the time axis at the bottom (only visible if trends are shown)"
    #     canvas = BrailleCanvas(time_axis.content_size.width, 2)
    #     x_ticks, x_desc = time_ticks
    #     xmin, xmax = time_window
    #     dx = xmax - xmin
    #     for x, tick in x_ticks:
    #         cx = get_screen_position(dx.total_seconds(),
    #                                  canvas.w,
    #                                  (x - xmin).total_seconds())
    #         canvas.draw_text((cx - len(tick) // 2) // 2, 0, tick)

    #     xmin_repr = xmin.astimezone(tz=None).strftime(self.TIMESTAMP_FORMAT)
    #     xmax_repr = xmax.astimezone(tz=None).strftime(self.TIMESTAMP_FORMAT)
    #     canvas.draw_text(0, 1, f"◀ {xmin_repr}")
    #     canvas.draw_text(canvas.width - len(xmax_repr) - 3, 1, f"{xmax_repr} ▶")
    #     canvas.draw_text(canvas.width // 2 - len(x_desc) // 2, 1, x_desc)
    #     time_axis = self.get_widget_by_id("time-axis")
    #     time_axis.update(Text.from_ansi("\n".join(canvas.render())))


class MonitorWidgetWrapper(Static):
    """
    General wrapper widget for a Monitor widget
    TODO Container?
    """

    DEFAULT_CSS = """
    MonitorWidgetWrapper {
        height: auto;
        &> MonitorWidget {
            margin-right: 1;
            padding-left: 1;
        }
        #separator {
            height: 1;
            border-top: solid $primary-background;
        }
    }
    MonitorWidgetWrapper >MonitorWidget:focus-within {
        padding-left: 0;
        border-left: outer white;
        background: $background-lighten-1;
    }

    """

    BINDINGS = [
        ("delete", "remove", "Remove"),
        ("enter", "jump", "Jump"),
    ]

    def __init__(self, widget: MonitorWidget, *args, **kwargs):
        self.widget = widget
        # self.can_focus = False
        super().__init__(*args, **kwargs)

    def compose(self):
        yield self.widget
        yield Static(id="separator")

    def action_remove(self):
        self.remove()

    def action_jump(self):
        self.post_message(NavigateTo(self.widget.entry))
