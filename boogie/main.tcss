/*
This file mainly contains visual styling, CSS rules that are important
for functionality is usually specified in DEFAULT_CSS on each widget.
*/

/* Variables */
$focus-border: outer white;


/* /\* Global *\/ */
/* * { */
/*     link-color: $primary-lighten-3; */
/* } */

Button.small {
    margin: 0;
    padding: 0;
    border: none;
    border-left: outer $panel-lighten-2;
    border-right: outer $panel-darken-3;
    width: auto;
    &.-active {
        border-left: outer $panel-darken-2;
        border-right: outer $panel-lighten-2;
    }

    &.-primary {
        border-right: outer $primary-darken-2;
        border-left: outer $primary-lighten-2;
    }

    &.-primary.-active {
        border-left: outer $primary-darken-2;
        border-right: outer $primary-lighten-2;
    }

    &.-warning {
        border-left: outer $warning-lighten-3;
        border-right: outer $warning-darken-2;
    }

    &.-warning.-active {
        border-right: outer $warning-darken-3;
        border-left: outer $warning-lighten-2;
    }

    &.-error {
        border-left: outer $error-lighten-3;
        border-right: outer $error-darken-2;
    }

    &.-error.-active {
        border-right: outer $error-darken-3;
        border-left: outer $error-lighten-2;
    }

}

/* Make tab bars more compact */
Tabs {
    height: 1;
    background: $background;
}

Tab {
    padding: 0 1;
    opacity: 70%;
    color: white;
}

Tab.-active {
    opacity: 100%;
    color: white;
    background: $primary-darken-1;
}

TabPane {
    padding: 0;
    height: 1fr;
}


Header {
    background: $background;
}

TextArea {
    border: none;
    padding: 0 0;
    background: $background;
}

Browser {

    Path {
        background: $primary-darken-1;
        .separator {
            color: #ffffff60
        }
    }

    /* Panels, the main parts of the browser UI */
    .panel {
        padding-left: 1;
    }

    .panel:focus-within {
        padding-left: 0;
        border-left: $focus-border;
    }

    #right {

        background: $background;

        Banner {
            background: $background-lighten-1;
        }

        /* Details */
        DetailsList {
            background: $background;
            Input.pattern {
                border: solid $background-lighten-3;
                &:focus {
                    border: solid white;
                }
            }
        }
        Details {

            background: $background;
            border-title-align: center;
            border-title-color: $text;
            border-title-style: bold;

            &.header-border {
                border-top: heavy $background-lighten-3;
            }

            Input {
                border: none;
            }

            .section {
                padding: 0 1;
                border: round $surface-lighten-1;
                border-title-style: bold;
                border-title-color: $primary-lighten-3;
                &:focus-within {
                    border: round $primary;
                }
            }
            .actions {
                background: $background-lighten-1;
            }
        }
    }

    Error {
        color: $error-lighten-2;
        border: round $error-darken-2;
        border-title-color: $error;
        &:focus-within {
            border: round $error;
            border-title-color: $error-lighten-3;
            border-title-style: bold;
        }
        height: auto;
        max-height: 25%;
        min-height: 6;
    }
}
